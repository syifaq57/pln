/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import 'react-native-gesture-handler';
import codePush from 'react-native-code-push';
import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import DrawerLayout from './src/DrawerLayout';

let codePushOptions = {checkFrequency: codePush.CheckFrequency.MANUAL};

export default class App extends Component {
  componentDidMount() {
    codePush.sync({
      updateDialog: {
        mandatoryUpdateMessage: 'There is a new Update',
        title: 'please press button to start updating',
        mandatoryContinueButtonLabel: 'update',
      },
      installMode: codePush.InstallMode.IMMEDIATE,
    });
  }
  render() {
    return <DrawerLayout />;
  }
}

App = codePush(codePushOptions)(App);
