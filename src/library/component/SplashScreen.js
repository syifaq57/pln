import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    TouchableOpacity,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Icon,
    CheckBox,
    Card
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
import Icon2 from "react-native-vector-icons/FontAwesome";
import styles from "../../res/styles/Login";
import GlobalConfig from "../network/GlobalConfig";


export default class SplashScreen extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            tokenFB:"",
            
        };
    }

    static navigationOptions = {
        header: null
    };

    exitApp() {
        Alert.alert(
            'Confirmation',
            'Exit WARM Mobile?',
            [
                { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'Yes', onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false }
        );
    }

    handleBackPress = () => {
        this.exitApp(); // works best when the goBack is async
        return true;
    }

    setTimePassed() {
        this.props.navigation.navigate("Login")
     }
     

    componentDidMount() {
        this._onFocusListener = this.props.navigation.addListener(
            "didFocus",
            playload => {
                AsyncStorage.getItem('fcmToken').then((value)=>{
                    this.setState({
                        tokenFB:value
                    }, function(){
                        // console.log('cmnn dd mon splsh scree', this.state.tokenFB)
                        AsyncStorage.getItem("idlogin").then((value)=>(
                            this.setState({
                                idlogin:value
                            },function(){
                                if(this.state.idlogin == 1){
                                    AsyncStorage.getItem("username").then((UserValue)=>(
                                        this.setState({
                                            username:UserValue
                                        },function(){
                                            AsyncStorage.getItem("password").then((PassValue)=>(
                                                this.setState({
                                                    password:PassValue
                                                },function(){
                                                    console.log('userpass', this.state.username)
                                                    console.log('pass', this.state.password)
                                                    if(this.state.username!=''||this.state.password!=''){
                                                        // this.onLoginmyACTS()
                                                        this.onLoginPress()
                                                      }
                                                })
                                            ))
                                        })
                                    ))
                                }else{
                                    console.log('fkkk')
                                    setTimeout( () => {
                                        this.setTimePassed();
                                     },2500);
                                }
                            })
                        ))
                    })
                })
                
        
            }
        )
        // console.log('ope splasscreen');
        


        
        this._onFocusListener = this.props.navigation.addListener(
            "didFocus",
            payload => {
                this.backHandler =BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
                this.setState({ registerToken: "" });
            }
        );

        const didBlurSubscription = this.props.navigation.addListener(
            'didBlur',
            payload => {
              console.log("masuk blur")
              this.backHandler.remove();
            }
          );
    }

    
    onLoginPress = () => {
        // this.props.navigation.navigate("TabNav")
        this.setState({
            visibleDialogSubmit: true
        })
        var url = GlobalConfig.SERVERHOST + 'login/apiLogin';
        var formData = new FormData();
        formData.append("user", this.state.username)
        formData.append("pass", this.state.password)
        console.log('login splash',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                // console.log(response)
                if (response.success == true ) {
                    this.saveUerPass()
                    AsyncStorage.setItem('data_user', (JSON.stringify(response.data))).then(() => {
                        this.setState({
                             password: '',
                            visibleDialogSubmit: false,
                            // visibleWelcome: true
                        },function(){
                            this.onNextLogin()
                            
                        })
                        this.props.navigation.navigate("Dashboard")

                    })
                }else{
                    this.setState({
                        visibleDialogSubmit: false
                    })

                    Alert.alert('Cannot Log in', response.msg, [{
                        text: 'Okay'
                    }])
                    this.props.navigation.navigate("Login")
                }
            })
            .catch((error) => {
                this.setState({
                    visibleDialogSubmit: false
                })
                Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                this.props.navigation.navigate("Login")
                console.log(error)
            })
         
    }
    
    saveUerPass(){
        AsyncStorage.setItem('username', this.state.username)
        AsyncStorage.setItem('password', this.state.password)
    }
    onNextLogin(){
        // AsyncStorage.setItem("idlogin", "1")
        var url = GlobalConfig.SERVERHOST + 'api/api/sendToken';
        var formData = new FormData();
        formData.append("username", this.state.username)
        formData.append("device_id", this.state.tokenFB)
        console.log('send token splash',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                console.log(response)
                if (response.success == true ) {
                   console.log('splash usrnm',response.username)
                }else{
                    
                }
            })
            .catch((error) => {
                this.setState({
                    visibleDialogSubmit: false
                })
                Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
    }

    render() {
        return (
            <Container style={{justifyContent:'center',backgroundColor:'white', alignItems:'center'}}>
                {}
                    <Image
                        style={{width: 160, height: 70}}
                        source={require('../../res/images/warmlogo.jpeg')}        
                   />    
            </Container>
        );
    }
}
