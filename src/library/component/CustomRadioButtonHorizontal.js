/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Platform, TouchableOpacity} from 'react-native';
import {Text, View} from 'native-base';
import colors from '../../res/colors';
import {ScrollView} from 'react-native-gesture-handler';
// import console = require("console");

class CustomRadioButtonHorizontal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      statRadio: [],
      selected: [],
      lengthOption: this.props.option.length,
      option: this.props.option,
    };
  }

  componentDidMount() {
    var selected = [];
    if (this.props.selected !== undefined) {
      for (var i = 0; i < this.state.lengthOption; i++) {
        if (this.state.option[i].NAMA_LIST === this.props.selected) {
          selected[i] = true;
        } else {
          selected[i] = false;
        }
      }
    } else {
      for (var i = 0; i < this.state.lengthOption; i++) {
        selected[i] = false;
      }
    }
    // eslint-disable-next-line react/no-did-mount-set-state
    this.setState({
      selected: selected,
    });
    console.log(this.state.selected);
  }

  methodPress(data, index) {
    var selected = [];
    for (var i = 0; i < this.state.lengthOption; i++) {
      if (i === index) {
        selected[i] = true;
      } else {
        selected[i] = false;
      }
    }
    this.setState({
      selected: selected,
    });
    this.props.onPressMethod(data);
  }

  render() {
    // var statRadio=[]
    // for (let i=0;i<this.props.option.length;i++){
    //     statRadio[i]=false
    // }
    // this.setState({
    //     statRadio:statRadio
    // })
    return (
      <ScrollView
        horizontal={true}
        style={{
          paddingLeft: 10,
          paddingVertical: 5,
          flexDirection: 'row',
          borderWidth: 1,
          borderColor: Platform.OS === 'ios' ? colors.lightGray : colors.gray05,
          borderRadius: 5,
        }}>
        {this.state.option.map((data, index) => (
          <TouchableOpacity onPress={() => this.methodPress(data, index)}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',

                paddingBottom: 5,
                paddingTop: 5,
              }}>
              <View
                style={{
                  height: 20,
                  width: 20,
                  borderRadius: 10,
                  borderWidth: 2,
                  marginRight: 5,
                  borderColor: colors.gray02,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {this.state.selected[index] ? (
                  <View
                    style={{
                      height: 12,
                      width: 12,
                      borderRadius: 6,
                      backgroundColor: colors.greenpln,
                    }}
                  />
                ) : null}
              </View>
              <Text style={{fontSize: 10, marginRight: 10, marginLeft: 5}}>
                {' '}
                {data[this.props.displayData]}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    );
  }
}

export default CustomRadioButtonHorizontal;
