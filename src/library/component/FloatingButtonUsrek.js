import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import {Card, Left, Right, Button} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import colors from '../../res/colors';
import ActionButton from 'react-native-action-button';

export default class FloatingButtonDashboard extends Component {
  navigateToScreen(route) {
    this.props.navigation.navigate(route);
  }

  render() {
    return (
      <ActionButton
        style={{marginBottom: -20, position: 'absolute'}}
        size={50}
        autoInactive={false}
        buttonColor="rgba(21, 103, 123,1)">
        <ActionButton.Item
          buttonColor="#E9482F"
          title="Input Usulan Program Kerja"
          onPress={() => this.navigateToScreen('InputUsrek')}>
          <Icon name="user-alt" style={styles.actionButtonIcon} />
        </ActionButton.Item>
        {/* <ActionButton.Item
          buttonColor="#3498db"
          title="Rencana Tindak Lanjut"
          onPress={() => this.navigateToScreen('InputRencana')}>
          <Icon name="user-tie" style={styles.actionButtonIcon} />
        </ActionButton.Item>
        <ActionButton.Item
          buttonColor="#DB1BBB"
          title="Relisasi Tindak Lanjut"
          onPress={() => this.navigateToScreen('InputRealisasi')}>
          <Icon name="user-check" style={styles.actionButtonIcon} />
        </ActionButton.Item> */}
      </ActionButton>
    );
  }
}
const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});
