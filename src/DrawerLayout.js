import React from 'react';
import {createAppContainer} from 'react-navigation';
import {Platform} from 'react-native';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';

import colors from './res/colors/index';
import Icon from 'react-native-vector-icons/FontAwesome';

import Login from './screen/Login';
import Dashboard from './screen/Dashboard/HomeDashboard';
import KamisLemonChart from './screen/Dashboard/KamisLemonChart/KamisLemonChart';
import Kegiatan from './screen/Dashboard/ButtonDashboardItem/Kegiatan';

import LKS from './screen/LKS/HomeLKS';
import InputAnomali from './screen/LKS/ItemInputLKS/InputAnomali';
import InputRencana from './screen/LKS/ItemInputLKS/InputRencana';
import InputRealisasi from './screen/LKS/ItemInputLKS/InputRealisasi';
import DetailLKS from './screen/LKS/DetailLKS/DetailLKS';

import BA from './screen/BA/HomeBA';
import InputBA from './screen/BA/ItemInputBA/InputBA';
import InputApprovel from './screen/BA/ItemInputBA/InputApprovel';
import DetailBA from './screen/BA/DetailBA/DetailBA';

import Aset from './screen/PergantianAset/HomeAset';
import InputAset from './screen/PergantianAset/ItemInputAset/InputPergantianAset';
import DetailAset from './screen/PergantianAset/DetailAset/DetailAset';

import BankUsrek from './screen/BankUsrek/HomeBankUsrek';
import InputUsrek from './screen/BankUsrek/ItemInputUsrek/InputUsrek';
import DetailUsrek from './screen/BankUsrek/DetailUsrek/DetailUsrek';

import KamisLemon from './screen/KamisLemon/HomeKamisLemon';
import InputKamisLemon from './screen/KamisLemon/ItemInputKamis/InputKamisLemon';
import DetailKamisLemon from './screen/KamisLemon/DetailKamisLemon/DetailKamis';

import TambahKegiatan from './screen/Dashboard/ButtonDashboardItem/TambahKegiatan';

const TabNav = createBottomTabNavigator(
  {
    Dashboard: {
      screen: Dashboard,
      navigationOptions: {
        title: 'Home',
      },
    },
    LKS: {
      screen: LKS,
      navigationOptions: {
        title: 'LKS',
      },
    },
    BA: {
      screen: BA,
      navigationOptions: {
        title: 'BA',
      },
    },
    Aset: {
      screen: Aset,
      navigationOptions: {
        title: 'BA Aset',
      },
    },
    BankUsrek: {
      screen: BankUsrek,
      navigationOptions: {
        title: 'Usrek',
      },
    },
    KamisLemon: {
      screen: KamisLemon,
      navigationOptions: {
        title: 'KamisLemon',
      },
    },
    // Account: {
    //   screen: Account,
    //   navigationOptions: {
    //     title: 'Account',
    //   },
    // },
  },
  {
    initialRouteName: 'Dashboard',
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({tintColor}) => {
        const {routeName} = navigation.state;
        let iconName;
        if (routeName === 'Dashboard') {
          iconName = 'home';
        } else if (routeName === 'LKS') {
          iconName = 'file-text';
        } else if (routeName === 'BA') {
          iconName = 'bullhorn';
        } else if (routeName === 'Aset') {
          iconName = 'bullhorn';
        } else if (routeName === 'BankUsrek') {
          iconName = 'archive';
        } else if (routeName === 'KamisLemon') {
          iconName = 'thermometer';
        }

        return <Icon name={iconName} size={20} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: colors.greenpln,
    },
    swipeEnabled: false,
    navigationOptions: {
      gesturesEnabled: Platform.OS == 'ios' ? false : false,
    },
  },
);

const NavStack = createStackNavigator(
  {
    Login: {
      screen: Login,
    },
    TabNav: {
      screen: TabNav,
    },
    InputAnomali: {
      screen: InputAnomali,
    },
    InputRencana: {
      screen: InputRencana,
    },
    InputRealisasi: {
      screen: InputRealisasi,
    },
    DetailLKS: {
      screen: DetailLKS,
    },
    InputBA: {
      screen: InputBA,
    },
    DetailBA: {
      screen: DetailBA,
    },
    InputApprovel: {
      screen: InputApprovel,
    },
    InputUsrek: {
      screen: InputUsrek,
    },
    DetailUsrek: {
      screen: DetailUsrek,
    },
    InputKamisLemon: {
      screen: InputKamisLemon,
    },
    DetailKamisLemon: {
      screen: DetailKamisLemon,
    },
    KamisLemonChart: {
      screen: KamisLemonChart,
    },
    Kegiatan: {
      screen: Kegiatan,
    },
    TambahKegiatan: {
      screen: TambahKegiatan,
    },
    InputAset: {
      screen: InputAset,
    },
    DetailAset: {
      screen: DetailAset,
    },
  },
  {
    defaultNavigationOptions: {
      headerShown: false,
    },
  },
);

const DrawerLayout = createAppContainer(NavStack);
export default DrawerLayout;
