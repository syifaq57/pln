import {StyleSheet} from 'react-native';
import colors from '../colors';

let labelTextSize = 12;
let headingTextSize = 15;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: 'flex',
  },
  logo: {
    width: 60,
    height: 70,
    marginTop: 10,
    marginBottom: 40,
  },
  scrollView: {
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
    flex: 1,
    flexDirection: 'column',
    // borderWidth:1,
    // borderColor:'black'
  },
  scrollViewWrapper: {
    flex: 1,
    padding: 0,
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  LoginText: {
    // fontFamily: "Montserrat-Medium",
    fontSize: headingTextSize,
    color: colors.black,
    fontWeight: '400',
    textAlign: 'center',
    marginBottom: 100,
  },
  facebookButtonIcon: {
    color: colors.white,
    position: 'relative',
  },
  label: {
    // fontFamily: "Montserrat-Medium",
    fontSize: labelTextSize,
    color: colors.white,
    fontWeight: '300',
  },
  input: {
    height: 40,
    color: colors.white,
    fontSize: 16,
    paddingLeft: 10,
  },
  inputItem: {
    height: 40,
    marginLeft: 30,
    marginRight: 30,
    paddingLeft: -15,
    // backgroundColor: 'white',
    borderBottomColor: colors.gray092,
    borderBottomWidth: 1,
    // borderBottomLeftRadius: 10,
    // borderBottomRightRadius: 10,
  },
  header: {
    backgroundColor: colors.green03,
  },
  footer: {
    flexDirection: 'row',
    backgroundColor: colors.whiteBlue,
  },
  footerLogoSI: {
    width: 60,
    height: 40,
    marginTop: 5,
  },
  footerLogo: {
    width: 40,
    height: 40,
    marginTop: 5,
  },
  loginButton: {
    marginBottom: 0,
    backgroundColor: colors.greenpln,
    borderColor: '#535A6C',
    marginRight: 40,
    borderRadius: 5,
    width: '100%',
  },
  formCard: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    borderColor: 'white',
    padding: 10,
    marginRight: 20,
    marginLeft: 20,
  },
  logoutButton: {
    marginBottom: 0,
    backgroundColor: '#B22222',
    borderColor: '#B22222',
    marginRight: 40,
    borderRadius: 4,
    width: '100%',
  },
  textButton: {
    fontWeight: 'bold',
    color: '#fff',
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain',
  },
});

export default styles;
