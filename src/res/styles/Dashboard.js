import {StyleSheet} from 'react-native';
import colors from '../colors';

const dashstyles = StyleSheet.create({
  sidebar: {
    flex: 1,
    height: '100%',
    width: '80%',
    backgroundColor: colors.gray09,
    alignSelf: 'flex-start',
    borderColor: colors.bluegray,
    borderRightWidth: 1,
  },
  modal: {
    marginTop: 0,
    marginBottom: 0,
    marginLeft: 0,
  },
  profil: {
    width: 80,
    height: 80,
    borderRadius: 40,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: 'white',
    backgroundColor: colors.gray10,
  },
});

export default dashstyles;
