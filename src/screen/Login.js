/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Image,
  StatusBar,
  KeyboardAvoidingView,
  ScrollView,
  View,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Alert,
  BackHandler,
  TouchableOpacity,
  Platform,
  DeviceEventEmitter,
  PushNotificationIOS,
  ImageBackground,
} from 'react-native';
import {
  Container,
  Header,
  Form,
  Item,
  Label,
  Input,
  Footer,
  Left,
  Right,
  Button,
  Body,
  Title,
  Icon,
  CheckBox,
  Card,
  Text,
} from 'native-base';
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton,
} from 'react-native-popup-dialog';
import Icon2 from 'react-native-vector-icons/FontAwesome';
// import LinearGradient from "react-native-linear-gradient";

import GlobalConfig from '../library/network/GlobalConfig';
import styles from '../res/styles/Login';
import colors from '../res/colors';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // username: '6484718K3',
      // password: '12345',
      username: '',
      password: '',
      visibleDialogSubmit: false,
      secureText: true,
      secureIcon: 'eye-slash',
    };
  }

  bilanganterbesar() {
    var array = [1, 12, 31, 20, 200, 5, 120, 150, 4, 5, 22];

    var hasilTerbesar = array[0];
    var TerbesarKedua = array[0];

    for (var i = 0; i < array.length; i++) {
      if (array[i] > hasilTerbesar) {
        TerbesarKedua = hasilTerbesar;
        hasilTerbesar = array[i];
      } else if (array[i] > TerbesarKedua && array[i] != hasilTerbesar) {
        TerbesarKedua = array[i];
      }
    }
    console.log(TerbesarKedua);
  }

  recurringarray() {
    var array = ['A', 'B', 'C', 'D', 'E', 'F', 'B'];
    var object = [];
    var result = [];
    var hasil = true;

    array.forEach(function(item) {
      if (!object[item]) {
        object[item] = 0;
      }
      object[item] += 1;
      hasil = object;
    });

    for (var huruf in object) {
      if (object[huruf] >= 2) {
        hasil = huruf;
      }
    }

    console.log(hasil);
  }

  async cocokilogiarray() {
    var array = [1, 2, 3, 4, 4, 5, 6, 6];
    var banding = 10;
    var arraySama = [];
    var object = [];
    var hasil;

    await array.forEach(function(item) {
      if (!object[item]) {
        object[item] = 0;
      }
      object[item] += 1;
      hasil = object;
    });

    for (var huruf in object) {
      if (object[huruf] >= 2) {
        await arraySama.push(huruf);
      }
    }

    await console.log(arraySama);
    var total = 0;
    for (var i = 0; i < arraySama.length; i++) {
      total += parseInt(arraySama[i]);
    }
    
    if (total === banding) {
      hasil = true;
    } else {
      hasil = false;
    }

    console.log(hasil);
  }

  componentDidMount() {
    // this.bilanganterbesar();
    this.cocokilogiarray();
    AsyncStorage.getItem('username').then(value =>
      this.setState(
        {
          username: value,
        },
        function() {
          AsyncStorage.getItem('password').then(value =>
            this.setState({
              password: value,
            }),
          );
        },
      ),
    );
  }

  onLoginPress() {
    this.setState({
      visibleDialogSubmit: true,
    });
    var url = GlobalConfig.SERVERHOST + 'login/action_login';
    var formData = new FormData();
    formData.append('username', this.state.username);
    formData.append('password', this.state.password);
    console.log('userpass', formData);
    fetch(url, {
      method: 'POST',
      body: formData,
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.status == 'success') {
          AsyncStorage.setItem('username', this.state.username);
          AsyncStorage.setItem('password', this.state.password);
          AsyncStorage.setItem('DataUser', JSON.stringify(response)).then(() =>
            this.setState(
              {
                visibleDialogSubmit: false,
                username: '',
                password: '',
              },
              function() {
                this.props.navigation.navigate('TabNav');
              },
            ),
          );
        } else {
          this.setState({
            visibleDialogSubmit: false,
          });

          Alert.alert('Cannot Login', response.message, [
            {
              text: 'Okay',
            },
          ]);
        }
      })
      .catch(error => {
        this.setState({
          visibleDialogSubmit: false,
        });
        Alert.alert('Cannot Log in', 'Check Your Internet Connection', [
          {
            text: 'Okay',
          },
        ]);
        console.log(error);
      });
  }

  render() {
    return (
      <Container style={styles.wrapper}>
        {}
        <ImageBackground
          source={require('../res/images/plnBG.png')}
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: colors.white,
            paddingTop: 0,
          }}>
          <KeyboardAvoidingView style={styles.wrapper} behavior="margin">
            <View style={styles.scrollViewWrapper}>
              <View style={styles.scrollView}>
                <View style={{flex: 3}}>
                  <View
                    style={{flex: 1, justifyContent: 'center', marginTop: 0}}>
                    <Image
                      source={require('../res/images/pln.png')}
                      style={{width: 150, height: 150, alignSelf: 'center'}}
                      resizeMode={'center'}
                    />
                    <Text
                      style={{
                        textAlign: 'center',
                        marginTop: 10,
                        fontWeight: 'bold',
                        fontSize: 40,
                        color: colors.greenpln,
                      }}>
                      UPT ON HAND
                    </Text>
                  </View>
                </View>
                <View style={{flex: 3}}>
                  <Form style={{marginLeft: 10, marginRight: 0}}>
                    <Item
                      style={[
                        styles.inputItem,
                        {
                          marginTop: 0,
                          marginBottom: 10,
                          paddingVertical: 25,
                          borderRadius: 5,
                        },
                      ]}>
                      {}
                      <Input
                        returnKeyType="next"
                        style={styles.input}
                        value={this.state.username}
                        placeholder="Username"
                        placeholderTextColor={colors.white}
                        onChangeText={text => this.setState({username: text})}
                      />
                    </Item>
                    <Item
                      last
                      style={[
                        styles.inputItem,
                        {
                          marginBottom: 25,
                          paddingVertical: 25,
                          borderRadius: 5,
                        },
                      ]}>
                      {}
                      <View
                        style={{
                          flexDirection: 'row',
                          flex: 1,
                          height: 40,
                          borderRadius: 8,
                          marginRight: 10,
                        }}>
                        <Input
                          returnKeyType="go"
                          style={[styles.input]}
                          secureTextEntry={this.state.secureText}
                          value={this.state.password}
                          placeholder="Password"
                          placeholderTextColor={colors.white}
                          onChangeText={text => this.setState({password: text})}
                        />
                        <View style={{alignSelf: 'center'}}>
                          <Icon2
                            onPress={() =>
                              this.setState({
                                secureText: this.state.secureText
                                  ? false
                                  : true,
                                secureIcon:
                                  this.state.secureIcon == 'eye'
                                    ? 'eye-slash'
                                    : 'eye',
                              })
                            }
                            name={this.state.secureIcon}
                            color={colors.greenpln}
                            style={{
                              fontSize: 20,
                              paddingLeft: 0,
                              alignSelf: 'center',
                              marginLeft: 5,
                              marginRight: 10,
                            }}
                          />
                        </View>
                      </View>
                    </Item>
                    <View
                      style={{
                        paddingRight: 40,
                        paddingLeft: 40,
                        marginBottom: 30,
                        marginTop: 10,
                      }}>
                      <Button
                        block
                        // onPress={() => this.onLoginPress()}
                        onPress={() => this.onLoginPress()}
                        style={styles.loginButton}>
                        <Text style={styles.textButton}>LOGIN</Text>
                      </Button>
                    </View>
                  </Form>
                </View>
              </View>

              <View style={{alignItems: 'center', marginBottom: 20}}>
                <Text style={{fontWeight: 'bold'}}>Verison 1.3</Text>
              </View>
              <View style={{width: 270, position: 'absolute'}}>
                <Dialog
                  visible={this.state.visibleDialogSubmit}
                  dialogTitle={<DialogTitle title="Authenticating .." />}>
                  <DialogContent>
                    {
                      <ActivityIndicator
                        size="large"
                        color="#330066"
                        animating
                      />
                    }
                  </DialogContent>
                </Dialog>
              </View>
            </View>
          </KeyboardAvoidingView>
        </ImageBackground>
      </Container>
    );
  }
}
