/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, AsyncStorage, Keyboard, ActivityIndicator} from 'react-native';
import {Input, Content} from 'native-base';
import colors from '../../../res/colors/index';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  UltimateListView,
  UltimateRefreshView,
} from 'react-native-ultimate-listview';
import ItemListLKS from './itemListLKS';
import GlobalConfig from '../../../library/network/GlobalConfig';

export default class ListLKS extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      searchText: '',
      limit: 10,
      isLoading: false,
    };
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      'didFocus',
      payload => {
        this.setState({isLoading: true}, function() {
          this.setState({isLoading: false});
        });
        // AsyncStorage.getItem('SavedAnomali').then(value => {
        //   if (value === '1') {
        //     this.setState({isLoading: true}, function() {
        //       this.setState({isLoading: false});
        //     });
        //     console.log('reload');
        //     AsyncStorage.setItem('SavedAnomali', '0');
        //   }
        // });
        // this.keyboardDidShowListener = Keyboard.addListener(
        //   'keyboardDidShow',
        //   this._keyboardDidShow,
        // );
        // this.keyboardDidHideListener = Keyboard.addListener(
        //   'keyboardDidHide',
        //   this._keyboardDidHide,
        // );
      },
    );
  }

  onFetch = async (page = 0, startFetch, abortFetch) => {
    try {
      setTimeout(() => {
        console.log((page - 1) * 10);
        var url = GlobalConfig.SERVERHOST + 'lkso/get_all_data_lkso';
        var formData = new FormData();
        formData.append('start', (page - 1) * 10);
        formData.append('limit', 10);
        formData.append('keyword', this.state.searchText);
        formData.append('id_ultg', this.props.id_ultg);
        formData.append('id_gardu_induk', this.props.id_gardu_induk);
        formData.append('status_pemantauan', this.props.status_pemantauan);
        formData.append('status_penyelesaian', this.props.status_penyelesaian);
        console.log('user tes', formData);
        fetch(url, {
          method: 'POST',
          body: formData,
        })
          .then(response => response.json())
          .then(responseData => {
            console.log('print', responseData);
            startFetch(responseData.data, 10);
            console.log('mlaku');
          })
          .catch(error => {
            console.log('eror 1', error);
            alert('Error Connection ' + error);
          })
          .done(() => {});
      }, 1000);
    } catch (err) {
      abortFetch();
      console.log('eror', err);
    }
  };

  _renderRowView = (item, index, separator) => {
    return (
      <View style={{width: '100%'}}>
        <ItemListLKS
          navigation={this.props.navigation}
          tgl_anomali={item.tgl_anomali}
          no_lkso={item.no_lkso}
          nm_indikator_anomali={item.nm_indikator_anomali}
          subarea={item.subarea}
          id_lkso={item.id_lkso}
          status_lkso={item.status_lkso}
          id_indikator_anomali={item.id_indikator_anomali}
        />
      </View>
    );
  };

  onFilter() {
    this.setState({isLoading: true}, function() {
      this.setState({isLoading: false, visibleSort: false});
    });
  }
  loadSearch() {
    this.setState({isLoading: true}, function() {
      this.setState({isLoading: false});
    });
  }

  render() {
    return (
      <View style={{height: '100%'}}>
        <View
          style={{
            paddingHorizontal: 15,
            paddingVertical: 10,
            flexDirection: 'row',
            marginTop: 5,
          }}>
          <View
            style={{
              flexDirection: 'row',
              flex: 1,
              height: 40,
              backgroundColor: colors.white,
              borderRadius: 8,
              marginRight: 10,
            }}>
            <Input
              style={{fontSize: 11, paddingLeft: 15, height: 40}}
              placeholder="Type something here"
              value={this.state.searchText}
              onSubmitEditing={() => this.loadSearch()}
              onEndEditing={() => console.log('canceled')}
              onBlur={() => console.log('canceled')}
              returnKeyType={'search'}
              onChangeText={text => this.setState({searchText: text})}
            />
            {this.state.keyboardShow && (
              <View style={{alignSelf: 'center'}}>
                <Icon
                  onPress={() => this.setState({searchText: ''})}
                  name="close"
                  style={{
                    fontSize: 15,
                    paddingLeft: 0,
                    alignSelf: 'center',
                    marginLeft: 5,
                    marginRight: 10,
                  }}
                />
              </View>
            )}
          </View>
          <View style={{alignSelf: 'center'}}>
            <Icon
              onPress={() => this.loadSearch()}
              name="search"
              color={colors.greenpln}
              style={{
                fontSize: 30,
                paddingLeft: 0,
                alignSelf: 'center',
                marginLeft: 5,
                marginRight: 5,
              }}
            />
          </View>
        </View>
        {this.state.isLoading ? (
          <View>
            <ActivityIndicator />
          </View>
        ) : (
          <View style={{flex: 1}}>
            <UltimateListView
              ref={ref => (this._listView = ref)}
              onFetch={this.onFetch}
              headerView={this.renderHeaderView}
              item={this._renderRowView}
              refreshableTitlePull="Pull To Refresh"
              refreshableMode="basic" //basic | advanced
            />
          </View>
        )}
      </View>
    );
  }
}
