/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import {Card, Text, Left, Right, Button} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';

export default class ItemListLKS extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // DataSource: [
      //   {
      //     tanggalAnomali: '30 September 2019',
      //     garduInduk: 'GI 150KV SAMBIKEREP',
      //     indikatorAnomali: '3 (Need Attention / Pemantauan)',
      //     ultg: 'ULTG GRESIK',
      //     nomorLKS: '027/LKSO/ALTAP/III/2020',
      //   },
      //   {
      //     tanggalAnomali: '7 Oktober 2019',
      //     garduInduk: 'GI 150KV GILITIMUR',
      //     indikatorAnomali: '2 (Repair During Next PM)',
      //     ultg: 'ULTG SAMPANG',
      //     nomorLKS: '026/LKSO/ALTAP/III/2020',
      //   },
      //   {
      //     tanggalAnomali: '8 Oktober 2019',
      //     garduInduk: 'GI 150KV ALTAPRIMA',
      //     indikatorAnomali: '2 (Repair During Next PM)',
      //     ultg: 'ULTG GRESIK',
      //     nomorLKS: '017/LKSO/ALTAP/III/2020',
      //   },
      //   {
      //     tanggalAnomali: '30 September 2019',
      //     garduInduk: 'GI 150KV SAMBIKEREP',
      //     indikatorAnomali: '3 (Need Attention / Pemantauan)',
      //     ultg: 'ULTG GRESIK',
      //     nomorLKS: '017/LKSO/ALTAP/III/2020',
      //   },
      //   {
      //     tanggalAnomali: '7 Oktober 2019',
      //     garduInduk: 'GI 150KV GILITIMUR',
      //     indikatorAnomali: '2 (Repair During Next PM)',
      //     ultg: 'ULTG SAMPANG',
      //     nomorLKS: '019/LKSO/ALTAP/III/2020',
      //   },
      //   {
      //     tanggalAnomali: '8 Oktober 2019',
      //     garduInduk: 'GI 150KV ALTAPRIMA',
      //     indikatorAnomali: '2 (Repair During Next PM)',
      //     ultg: 'ULTG GRESIK',
      //     nomorLKS: '019/LKSO/ALTAP/III/2020',
      //   },
      // ],
    };
  }
  navigateToScreen(route) {
    AsyncStorage.setItem('no_lkso', this.props.no_lkso);
    AsyncStorage.setItem('id_lkso', this.props.id_lkso).then(() => {
      this.props.navigation.navigate(route, {id_lkso: this.props.id_lkso});
    });
  }

  componentDidMount() {
    if (this.props.id_indikator_anomali === '1') {
      this.setState({
        colorindikator: 'rgba(207,82,77,1)',
      });
    } else if(this.props.id_indikator_anomali === '2') {
      this.setState({
        colorindikator: 'rgba(50,198,165,1)',
      });
    } else {
      this.setState({
        colorindikator: 'rgba(234,165,95,1)',
      });
    }
  }

  render() {
    return (
      <View>
        <TouchableOpacity onPress={() => this.navigateToScreen('DetailLKS')}>
          <View style={{marginLeft: 15, marginRight: 15}}>
            <Card
              style={{
                borderRadius: 8,
                flex: 1,
                padding: 15,
                // borderColor: 'white',
                backgroundColor: this.state.colorindikator,
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginLeft: 0,
                  marginRight: 0,
                }}>
                <View style={{flex: 1}}>
                  <View style={{flex: 1, flexDirection: 'row', marginTop: 5}}>
                    <View style={{flex: 2}}>
                      <Text style={{fontSize: 10, color: 'white'}}>
                        Tanggal Anomali
                      </Text>
                      <Text style={{marginBottom: 10, color: 'white'}}>
                        {this.props.tgl_anomali}
                      </Text>
                    </View>
                    <View style={{flex: 2}}>
                      <Text
                        style={{
                          color: 'white',
                          textAlign: 'right',
                          fontWeight: 'bold',
                        }}>
                        {this.props.status_lkso}
                      </Text>
                    </View>
                  </View>
                  <View style={{flex: 1, flexDirection: 'column'}}>
                    <Text style={{fontSize: 10, color: 'white'}}>
                      Nomor LKS
                    </Text>
                    <Text
                      numberOfLines={2}
                      style={{marginBottom: 10, color: 'white'}}>
                      {this.props.no_lkso}
                    </Text>
                    <Text style={{fontSize: 10, color: 'white'}}>
                      Indikator Anomali
                    </Text>
                    <Text
                      style={{
                        marginBottom: 10,
                        color: 'white',
                      }}>
                      {this.props.nm_indikator_anomali}
                    </Text>
                  </View>
                </View>
                {/* <View style={{flex: 3, justifyContent: 'center'}}>
                  <View style={{alignItems: 'center'}}>
                    <Image
                      source={require('../../../res/images/tower.png')}
                      style={{
                        height: 40,
                        width: 40,
                        resizeMode: 'contain',
                      }}
                    />
                    <Text
                      uppercase={false}
                      style={{
                        fontSize: 12,
                        fontWeight: 'bold',
                        color: colors.darkOrange,
                        marginTop: 5,
                      }}>
                      {this.props.subarea}
                    </Text>
                  </View>
                </View> */}
                {/* <Text
                    style={{
                      alignSelf: 'center',
                      color: colors.greenDefault,
                      fontWeight: 'normal',
                    }}>
                    {data.ultg}
                  </Text> */}
              </View>
            </Card>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
