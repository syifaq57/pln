import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import {Card, Left, Right, Button} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import colors from '../../../res/colors/index';
import ActionButton from 'react-native-action-button';

export default class FloatingButtonDashboard extends Component {
  navigateToScreen(route) {
    this.props.navigation.navigate(route);
  }

  render() {
    return (
      <ActionButton
        style={{marginBottom: 0, position: 'absolute'}}
        size={50}
        autoInactive={false}
        buttonColor="rgba(21, 103, 123,1)">
        {/* {this.props.visibleRAB === true ? ( */}
        <ActionButton.Item
          buttonColor="#3498db"
          title="Rencana Tindak Lanjut"
          onPress={() =>
            this.navigateToScreen('InputRencana', {
              id_lkso: this.props.id_lkso,
            })
          }>
          <Icon name="user-tie" style={styles.actionButtonIcon} />
        </ActionButton.Item>
        {/* ) : (
          <View />
        )} */}
        <ActionButton.Item
          buttonColor="#DB1BBB"
          title="Berita Acara"
          onPress={() => this.navigateToScreen('InputBA')}>
          <Icon name="user-check" style={styles.actionButtonIcon} />
        </ActionButton.Item>
      </ActionButton>
    );
  }
}
const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});
