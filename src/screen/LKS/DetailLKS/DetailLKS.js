/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Platform,
  Dimensions,
  Alert,
  ActivityIndicator,
  Linking,
  Image,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Button,
  Content,
  Text,
  Card,
} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';
// import ItemDetail from './itemDetail';
import {TabBar, TabView, SceneMap} from 'react-native-tab-view';
import FloatingButton from './FloatingDetailLKS';
import GlobalConfig from '../../../library/network/GlobalConfig';
import Modal from 'react-native-modal';

export default class DetailLKS extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      dataCoba: 'tes',
      index: 0,
      routes: [
        {key: 'DetailAnomali', title: 'Detail Anomali '},
        {key: 'RencanaRealisasi', title: 'Rencana Realisasi'},
      ],
      images: [],
      isModalVisible: false,
      isModalVisible2: false,
      DataUser: [],
      visibleRAB: false,
    };
  }

  loadData() {
    this.setState({
      isLoading: true,
    });
    const {id_lkso} = this.props.navigation.state.params;
    this.setState({isLoading: true});
    var url = GlobalConfig.SERVERHOST + 'lkso/get_all_data_lkso';
    var formData = new FormData();
    formData.append('id_lkso', id_lkso);
    console.log('frmdta', formData);
    fetch(url, {
      method: 'POST',
      body: formData,
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.status === 'success') {
          this.setState(
            {
              data: response.data,
            },
            function() {
              // const DataUser = this.state.DataUser;
              // const jabatan = this.state.DataUser.detail_jabatan[0];
              // if (
              //   jabatan.nm_jenis_pegawai === 'MULTG' ||
              //   DataUser.user.is_admin === 'Y'
              // ) {
              //   this.setState(
              //     {
              //       visibleRAB: true,
              //     },
              //     function() {
              this.setState({
                isLoading: false,
              });
            },
          );
        } else {
          // this.setState(
          //   {
          //     visibleRAB: false,
          //   },
          //   function() {
          this.setState({
            isLoading: false,
          });
          //           },
          //         );
          //       }
          //     },
          //   );
          // } else {
          //   this.setState({
          //     isLoading: false,
          //   });
          Alert('Gagal Load Data', [
            {
              text: 'Okay',
            },
          ]);
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        Alert.alert('Error', 'Check Your Internet Connection', [
          {
            text: 'Okay',
          },
        ]);
        console.log(error);
      });
  }

  onOpenForm() {
    const {id_lkso} = this.props.navigation.state.params;
    var url = GlobalConfig.SERVERHOST + 'lkso/get_dokumen_lkso';
    var formData = new FormData();
    formData.append('id_lkso', id_lkso);
    console.log('frmdta', formData);
    fetch(url, {
      method: 'POST',
      body: formData,
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.status === 'success') {
          this.setState(
            {
              url: response.data.dokumen,
            },
            function() {
              // console.log('Data all', jabatan);
              Linking.canOpenURL(this.state.url).then(supported => {
                if (supported) {
                  Linking.openURL(this.state.url);
                } else {
                  alert('Browser tidak ditemukan!');
                  console.log("Don't know how to open URI: " + this.state.url);
                }
              });
            },
          );
        } else {
          this.setState({
            isLoading: false,
          });
          Alert('Gagal Load Data', [
            {
              text: 'Okay',
            },
          ]);
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        Alert.alert('Error', 'Check Your Internet Connection', [
          {
            text: 'Okay',
          },
        ]);
        console.log(error);
      });
  }

  componentDidMount() {
    // this.loadData();

    this._onFocusListener = this.props.navigation.addListener(
      'didFocus',
      payload => {
        // AsyncStorage.getItem('DataUser').then(value =>
        //   this.setState(
        //     {
        //       DataUser: JSON.parse(value),
        //     },
        //     function() {
        //       console.log('idpgw', this.state.DataUser.user.nip);
        //       this.loadData();
        //     },
        //   ),
        // );
        // this.loadData();
        // AsyncStorage.getItem('SavedRencana').then(value => {
        //   if (value === '1') {
        //     this.loadData();
        //     console.log('reload');
        //     AsyncStorage.setItem('SavedRencana', '0');
        //   }
        // });
      },
    );

    const {id_lkso} = this.props.navigation.state.params;
    this.setState(
      {
        id_lkso: id_lkso,
      },
      function() {
        // console.log('lks0', this.state.id_lkso);
      },
    );
  }

  // onClickImage(data) {
  //   console.log(data);
  //   this.setState(
  //     {
  //       images: [
  //         {
  //           uri:
  //             GlobalConfig.base_url +
  //             'assets/foto-anomali/' +
  //             this.state.data.dokumen_anomali_satu,
  //         },
  //       ],
  //       isModalVisible: !this.state.isModalVisible,
  //     },
  //     function() {
  //       console.log('tes', this.state.images);
  //     },
  //   );
  // }

  toggleModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
      // images:[{uri:key}]
    });
  };

  toggleModal2 = () => {
    this.setState({
      isModalVisible2: !this.state.isModalVisible2,
      // images:[{uri:key}]
    });
  };

  render() {
    const DetailAnomali = () => (
      <Content>
        <View style={{margin: 15}}>
          <View style={{marginLeft: 10}}>
            <Text style={{fontWeight: 'bold'}}>Detail Anomali</Text>
          </View>
          <Card
            style={{
              borderRadius: 8,
              flex: 1,
              padding: 15,
            }}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 2}}>
                  <Text style={{fontSize: 10}}>Tanggal Anomali</Text>
                  <Text style={{marginBottom: 10}}>
                    {this.state.data.tgl_anomali}
                  </Text>
                </View>
                <View style={{flex: 1}}>
                  <Button
                    onPress={() => this.onOpenForm()}
                    style={{
                      backgroundColor: colors.greenpln,
                      borderRadius: 5,
                    }}>
                    <View style={{flex: 1}}>
                      <Text style={{textAlign: 'center'}}>Form LKS</Text>
                    </View>
                  </Button>
                </View>
              </View>

              <Text style={{fontSize: 10}}>Nomor LKS</Text>
              <Text style={{marginBottom: 10}}>{this.state.data.no_lkso}</Text>
              <Text style={{fontSize: 10}}>ULTG</Text>
              <Text style={{marginBottom: 10}}>{this.state.data.nm_ultg}</Text>

              <Text style={{fontSize: 10}}>Gardu Induk</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.nm_gardu_induk}
              </Text>
              <Text style={{fontSize: 10}}>BAY</Text>
              <Text style={{marginBottom: 10}}>{this.state.data.nm_bay}</Text>
              <Text style={{fontSize: 10}}>Peralatan</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.nm_peralatan}
              </Text>
              <Text style={{fontSize: 10}}>Indikator Anomali</Text>
              <Text style={{marginBottom: 10}}>
                <Text style={{marginBottom: 10}}>
                  {this.state.data.nm_indikator_anomali}
                </Text>
              </Text>
              <Text style={{fontSize: 10}}>Uraian Anomali</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.uraian_anomali}
              </Text>
            </View>
          </Card>
          <View style={{marginLeft: 10}}>
            <Text style={{fontWeight: 'bold'}}>Dokumentasi Anomali</Text>
          </View>
          <Card
            style={{
              borderRadius: 8,
              flex: 1,
              padding: 10,
              paddingVertical: 20,
            }}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 2, justifyContent: 'center'}}>
                <TouchableOpacity
                  onPress={() => this.toggleModal()}
                  style={{flex: 1}}>
                  <Image
                    style={{
                      alignSelf: 'center',
                      width: 100,
                      height: 60,
                      borderRadius: 3,
                      borderColor: 'black',
                      resizeMode: 'contain',
                    }}
                    source={{
                      uri:
                        GlobalConfig.base_url +
                        'assets/foto-anomali/' +
                        this.state.data.dokumen_anomali_satu,
                    }}
                  />
                </TouchableOpacity>
              </View>
              <View style={{flex: 2, justifyContent: 'center'}}>
                <TouchableOpacity
                  onPress={() => this.toggleModal2()}
                  style={{flex: 1}}>
                  <Image
                    style={{
                      alignSelf: 'center',
                      width: 100,
                      height: 60,
                      borderRadius: 3,
                      borderColor: 'black',
                      resizeMode: 'contain',
                    }}
                    source={{
                      uri:
                        GlobalConfig.base_url +
                        'assets/foto-anomali/' +
                        this.state.data.dokumen_anomali_dua,
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </Card>
        </View>
        <View style={{height: 50}} />
        <Modal
          style={{
            justifyContent: 'center',
            padding: 10,
            paddingHorizontal: 0,
            marginHorizontal: 0,
          }}
          isVisible={this.state.isModalVisible}
          backdropOpacity={1}
          onBackdropPress={this.toggleModal}
          onBackButtonPress={this.toggleModal}
          animationIn={'slideInUp'}
          animationOut={'slideOutDown'}
          hideModalContentWhileAnimating={true}>
          <View>
            <Button
              style={{
                width: 40,
                height: 40,
                margin: 20,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'red',
              }}
              onPress={() => this.toggleModal()}>
              <Icon2
                onPress={() => this.toggleModal()}
                name={'close'}
                size={20}
              />
            </Button>
          </View>
          <View style={{flex: 1}}>
            <Image
              style={{
                alignSelf: 'center',
                width: '100%',
                height: '100%',
                resizeMode: 'contain',
                borderRadius: 3,
              }}
              source={{
                uri:
                  GlobalConfig.base_url +
                  'assets/foto-anomali/' +
                  this.state.data.dokumen_anomali_satu,
              }}
            />
          </View>
        </Modal>
        <Modal
          style={{
            justifyContent: 'center',
            padding: 10,
            paddingHorizontal: 0,
            marginHorizontal: 0,
          }}
          isVisible={this.state.isModalVisible2}
          backdropOpacity={1}
          onBackdropPress={this.toggleModal2}
          onBackButtonPress={this.toggleModal2}
          animationIn={'slideInUp'}
          animationOut={'slideOutDown'}
          hideModalContentWhileAnimating={true}>
          <View>
            <Button
              style={{
                width: 40,
                height: 40,
                margin: 20,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'red',
              }}
              onPress={() => this.toggleModal2()}>
              <Icon2
                onPress={() => this.toggleModal2()}
                name={'close'}
                size={20}
              />
            </Button>
          </View>
          <View style={{flex: 1}}>
            <Image
              style={{
                alignSelf: 'center',
                width: '100%',
                height: '100%',
                resizeMode: 'contain',
                borderRadius: 3,
              }}
              source={{
                uri:
                  GlobalConfig.base_url +
                  'assets/foto-anomali/' +
                  this.state.data.dokumen_anomali_dua,
              }}
            />
          </View>
        </Modal>
      </Content>
    );

    const RencanaRealisasi = () => (
      <Content>
        <View style={{margin: 15}}>
          <View style={{marginLeft: 10, marginTop: 10}}>
            <Text style={{fontWeight: 'bold'}}>Rencana dan Realisasi</Text>
          </View>
          <Card
            style={{
              borderRadius: 8,
              flex: 1,
              padding: 15,
            }}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text style={{fontSize: 10}}>Uraian Rencana Tindak Lanjut</Text>
              {this.state.data.uraian_rencana === null ? (
                <Text style={{marginBottom: 10}}> - </Text>
              ) : (
                <Text style={{marginBottom: 10}}>
                  {this.state.data.uraian_rencana}
                </Text>
              )}
              <Text style={{fontSize: 10}}>Tanggal Rencana</Text>
              {this.state.data.tgl_rencana === null ? (
                <Text style={{marginBottom: 10}}> - </Text>
              ) : (
                <Text style={{marginBottom: 10}}>
                  {this.state.data.tgl_rencana}
                </Text>
              )}
              <Text style={{fontSize: 10}}>PIC Pelaksana</Text>
              {this.state.data.pic_pelaksana === null ? (
                <Text style={{marginBottom: 10}}> - </Text>
              ) : (
                <Text style={{marginBottom: 10}}>
                  {this.state.data.pic_pelaksana}
                </Text>
              )}
            </View>
          </Card>

          {/* <View style={{marginLeft: 10, marginTop: 10}}>
            <Text style={{fontWeight: 'bold'}}>Berita Acara</Text>
          </View>
          <Card
            style={{
              borderRadius: 8,
              flex: 1,
              padding: 15,
            }}>
            <Card
              style={{
                borderRadius: 8,
                flex: 1,
                padding: 10,
              }}>
              <View style={{flex: 1, flexDirection: 'column'}}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <View
                    style={{flex: 1, justifyContent: 'center', marginLeft: 10}}>
                    <Text style={{fontWeight: 'bold'}}>
                      BA Tindak Lanjut Anomali
                    </Text>
                  </View>

                  <Button
                    style={{
                      marginLeft: 5,
                      marginRight: 10,
                      backgroundColor: colors.greenpln,
                      borderRadius: 5,
                    }}>
                    <View style={{paddingHorizontal: 15, paddingVertical: 0}}>
                      <Text style={{color: 'white'}}>Lihat</Text>
                    </View>
                  </Button>
                </View>
              </View>
            </Card>
            <Card
              style={{
                borderRadius: 8,
                flex: 1,
                padding: 10,
              }}>
              <View style={{flex: 1, flexDirection: 'column'}}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <View
                    style={{flex: 1, justifyContent: 'center', marginLeft: 10}}>
                    <Text style={{fontWeight: 'bold'}}>BA Tindak Lanjut 2</Text>
                  </View>

                  <Button
                    style={{
                      marginLeft: 5,
                      marginRight: 10,
                      backgroundColor: colors.greenpln,
                      borderRadius: 5,
                    }}>
                    <View style={{paddingHorizontal: 15, paddingVertical: 0}}>
                      <Text style={{color: 'white'}}>Lihat</Text>
                    </View>
                  </Button>
                </View>
              </View>
            </Card>
            <Card
              style={{
                borderRadius: 8,
                flex: 1,
                padding: 10,
              }}>
              <View style={{flex: 1, flexDirection: 'column', margin: 10}}>
                <Text style={{fontSize: 10}}>Keterangan Tambahan</Text>
                <Text style={{marginBottom: 10}}>
                  Telah dilakukan perbaikan pondasi CT 150kV phasa T pada T/L
                  Bay Waru#2 di GI Sambikerep dengan melakukan penyemenan ulang
                  pada pondasi yang retak.
                </Text>
              </View>
            </Card>
          </Card> */}
        </View>
        <View style={{height: 50}} />
      </Content>
    );

    return (
      <Container>
        <Header
          transparent
          style={{
            marginTop: Platform.OS === 'ios' ? 0 : 0,
            borderBottomWidth: 0,
            backgroundColor: colors.greenpln,
          }}>
          <View
            style={{
              flex: 2,
              justifyContent: 'center',
            }}>
            <Button
              onPress={() => this.props.navigation.goBack()}
              transparent
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <Icon
                name="md-arrow-round-back"
                size={23}
                style={{color: 'white'}}
              />
              <Text
                uppercase={false}
                style={{
                  flex: 1,
                  textAlignVertical: 'center',
                  fontWeight: 'bold',
                  fontSize: 20,
                  color: 'white',
                }}>
                Detail LKS
              </Text>
            </Button>
          </View>
          <View style={{flex: 1}} />
        </Header>
        {this.state.isLoading === true ? (
          <View style={{alignItems: 'center'}}>
            <ActivityIndicator />
          </View>
        ) : (
          <TabView
            navigationState={this.state}
            renderScene={SceneMap({
              DetailAnomali: DetailAnomali,
              RencanaRealisasi: RencanaRealisasi,
            })}
            onIndexChange={index => this.setState({index})}
            initialLayout={{width: Dimensions.get('window').width}}
            renderTabBar={props => (
              <TabBar
                {...props}
                indicatorStyle={{backgroundColor: 'white'}}
                style={{width: '100%', backgroundColor: colors.greenpln}}
                labelStyle={{fontWeight: 'bold', fontSize: 12}}
              />
            )}
          />
        )}
        {/* {this.state.isLoading === true ? (
          <View style={{alignItems: 'center'}}>
            <ActivityIndicator />
          </View>
        ) : ( */}
        <FloatingButton
          id_lkso={this.state.id_lkso}
          // visibleRAB={this.state.visibleRAB}
          navigation={this.props.navigation}
        />
        {/* )} */}
      </Container>
    );
  }
}
