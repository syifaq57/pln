import React, {Component, Fragment} from 'react';
import {
  Image,
  StatusBar,
  KeyboardAvoidingView,
  ScrollView,
  View,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Alert,
  BackHandler,
  Platform,
  DeviceEventEmitter,
  PushNotificationIOS,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  Container,
  Header,
  Form,
  Item,
  Label,
  Input,
  Footer,
  Left,
  Right,
  Button,
  Body,
  Title,
  CheckBox,
  Content,
  Card,
  CardItem,
  Text,
  Textarea,
} from 'native-base';
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton,
} from 'react-native-popup-dialog';
import DatePicker from 'react-native-datepicker';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import SearchableDropdown from '../../../library/component/SearchableDropdown';
import styles from '../../../res/styles/Form';
import colors from '../../../res/colors/index';
import StepIndicator from 'react-native-step-indicator';
import RadioButton from '../../../library/component/CustomRadioButton';
import Modal from 'react-native-modal';
import dashstyles from '../../../res/styles/Dashboard';
import ImagePicker from 'react-native-image-crop-picker';

const ListStatusPenyelesaian = [
  {NAMA_LIST: 'Pending', ID_OPTIONS_LIST: '1'},
  {NAMA_LIST: 'On Progress', ID_OPTIONS_LIST: '2'},
  {NAMA_LIST: 'Done', ID_OPTIONS_LIST: '3'},
  {NAMA_LIST: 'Cancel', ID_OPTIONS_LIST: '4'},
];

export default class InputRealisasi extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
  }

  render() {
    return (
      <Container>
        <Header
           style={{
            backgroundColor: colors.greenpln,
            marginTop: Platform.OS === 'ios' ? 0 : 20,
          }}
          androidStatusBarColor={'#3b9bb3'}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              marginBottom: 10,
            }}>
            <Left style={{marginLeft: 10}}>
              <Button
                onPress={() => this.props.navigation.goBack()}
                transparent>
                <Icon
                  name="md-arrow-round-back"
                  size={23}
                  style={{color: 'white', marginTop: 5}}
                />

                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 20,
                    color: 'white',
                    marginLeft: 10,
                  }}>
                  Input Realisasi Tindak Lanjut
                </Text>
              </Button>
            </Left>
          </View>
        </Header>
        <Content>
          <CardItem>
            <View style={{flex: 1}}>
              <View>
                <Text style={[styles.fontLabel]}>Tanggal Realisasi</Text>
              </View>
              <View>
                <DatePicker
                  style={{width: '100%', height: 50}}
                  date={this.state.REPORT_DATE}
                  mode="date"
                  placeholder="Select Date"
                  format="YYYY-MM-DD"
                  minDate={this.state.minDate}
                  maxDate={this.state.maxDate}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={styles.datePicker}
                  onDateChange={date => {
                    this.setState({REPORT_DATE: date});
                  }}
                />
              </View>
              <View>
                <Text style={[{marginTop: 10}, styles.fontLabel]}>
                  Status Penyelesaian
                </Text>
              </View>
              <View>
                <RadioButton
                  option={ListStatusPenyelesaian}
                  selected={this.state.sortDir_NAME}
                  displayData="NAMA_LIST"
                  onPressMethod={data =>
                    this.setState({
                      sortDir: data.ID_OPTIONS_LIST,
                      sortDir_NAME: data.NAMA_LIST,
                    })
                  }
                />
              </View>
              <View style={{flex: 1, marginLeft: 5}}>
                <View>
                  <Text style={[{marginTop: 10}, styles.fontLabel]}>
                    BA Tindak Lanjut
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    borderColor: colors.greenpln,
                    borderWidth: 1,
                    borderRadius: 5,
                  }}>
                  <View style={{backgroundColor: colors.greenpln}}>
                    <View style={{marginLeft: 10, padding: 5}}>
                      <Text style={{fontWeight: 'bold', color: 'white'}}>
                        Tambah File
                      </Text>
                    </View>
                  </View>
                  <View>
                    <View style={{height: '100%', paddingVertical: 25}}>
                      <Button style={{justifyContent: 'center'}} transparent>
                        <Image
                          source={require('../../../res/images/folder.png')}
                          style={{
                            height: 90,
                            width: 130,
                            resizeMode: 'contain',
                          }}
                        />
                      </Button>
                    </View>
                  </View>
                </View>
              </View>
              <View>
                <Text style={[{marginTop: 10}, styles.fontLabel]}>
                  Keterangan Tambahan
                </Text>
              </View>
              <View>
                <Textarea
                  style={styles.textArea}
                  rowSpan={3}
                  bordered
                  placeholderTextColor={colors.gray02}
                  value={this.state.CATATAN}
                  placeholder="Tulis Keterangan Tambahan"
                  onChangeText={text => this.setState({CATATAN: text})}
                />
              </View>
              <View style={{marginTop: 10}}>
                <Button
                  onPress={() => this.movePage()}
                  style={{
                    flex: 1,
                    backgroundColor: colors.green01,
                  }}>
                  <View style={{flex: 1}}>
                    <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
                      Kirim
                    </Text>
                  </View>
                </Button>
              </View>
            </View>
          </CardItem>
        </Content>
      </Container>
    );
  }
}
