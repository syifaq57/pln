import React, {Component, Fragment} from 'react';
import {
  Image,
  StatusBar,
  KeyboardAvoidingView,
  ScrollView,
  View,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Alert,
  BackHandler,
  Platform,
  DeviceEventEmitter,
  PushNotificationIOS,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  Container,
  Header,
  Form,
  Item,
  Label,
  Input,
  Footer,
  Left,
  Right,
  Button,
  Body,
  Title,
  CheckBox,
  Content,
  Card,
  CardItem,
  Text,
  Textarea,
} from 'native-base';
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton,
} from 'react-native-popup-dialog';
import DatePicker from 'react-native-datepicker';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import SearchableDropdown from '../../../library/component/SearchableDropdown';
import styles from '../../../res/styles/Form';
import colors from '../../../res/colors/index';
import StepIndicator from 'react-native-step-indicator';
import RadioButton from '../../../library/component/CustomRadioButton';
import Modal from 'react-native-modal';
import dashstyles from '../../../res/styles/Dashboard';
import ImagePicker from 'react-native-image-crop-picker';
import GlobalConfig from '../../../library/network/GlobalConfig';

const PIC = [
  {NAMA_LIST: 'MULTG', ID_OPTIONS_LIST: '1'},
  {NAMA_LIST: 'SPV HARGI', ID_OPTIONS_LIST: '2'},
  {NAMA_LIST: 'SPV HARJAR', ID_OPTIONS_LIST: '3'},
  {NAMA_LIST: 'SPV HARJAR', ID_OPTIONS_LIST: '4'},
];

export default class InputRencana extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('id_lkso').then(value =>
      this.setState(
        {
          id_lkso: value,
        },
        function() {
          console.log('idlkso 2', this.state.id_lkso);
        },
      ),
    );
  }

  onKirimData() {
    this.setState({
      isLoadingSave: true,
    });
    if (
      this.state.ID_PIC === '' ||
      this.state.id_rab === '' ||
      this.state.uraian_rencana === '' ||
      this.state.tgl_rencana === ''
    ) {
      alert('Form belum lengkap');
      this.setState({
        isLoadingSave: false,
      });
    } else {
      var url = GlobalConfig.SERVERHOST + 'lkso/add_rencana_lkso';
      var formData = new FormData();
      formData.append('id_lkso', this.state.id_lkso);
      formData.append('uraian_rencana', this.state.uraian_rencana);
      formData.append('tgl_rencana', this.state.tgl_rencana);
      formData.append('pic_pelaksana', this.state.pic_pelaksana);
      formData.append('id_rab', this.state.id_rab);
      
      console.log('all FD', formData);
      fetch(url, {
        method: 'POST',

        body: formData,
        redirect: 'follow',
      })
        .then(response => response.json())
        .then(response => {
          console.log('', response);
          if (response.status === 'success') {
            this.setState(
              {
                isLoadingSave: false,
              },
              function() {
                Alert.alert('Data Saved', response.message, [
                  {
                    text: 'Okay',
                  },
                ]);
                AsyncStorage.setItem('SavedRencana', '1');
                this.props.navigation.goBack();
              },
            );
          } else {
            this.setState({
              isLoadingSave: false,
            });
            Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [
              {
                text: 'Okay',
              },
            ]);
          }
        })
        .catch(error => {
          this.setState({
            isLoadingSave: false,
          });
          Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [
            {
              text: 'Okay',
            },
          ]);
          console.log(error);
        });
    }
  }

  render() {
    return (
      <Container>
        <Header
          transparent
          style={{
            marginTop: Platform.OS === 'ios' ? 0 : 0,
            borderBottomWidth: 0,
            backgroundColor: colors.greenpln,
          }}>
          <View
            style={{
              flex: 2,
              justifyContent: 'center',
            }}>
            <Button
              onPress={() => this.props.navigation.goBack()}
              transparent
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <Icon
                name="md-arrow-round-back"
                size={23}
                style={{color: 'white'}}
              />
              <Text
                uppercase={false}
                style={{
                  flex: 1,
                  textAlignVertical: 'center',
                  fontWeight: 'bold',
                  fontSize: 20,
                  color: 'white',
                }}>
                Input Rencana
              </Text>
            </Button>
          </View>
          <View style={{flex: 1}}/>
        </Header>
        <Content>
          <CardItem>
            {this.state.isLoading ? (
              <View style={{alignItems: 'center'}}>
                <ActivityIndicator />
              </View>
            ) : (
              <View style={{flex: 1}}>
                <View>
                  <Text style={[styles.fontLabel]}>Tanggal Rencana</Text>
                </View>
                <View>
                  <DatePicker
                    style={{width: '100%', height: 50}}
                    date={this.state.tgl_rencana}
                    mode="date"
                    placeholder="Select Date"
                    format="YYYY-MM-DD"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={styles.datePicker}
                    onDateChange={date => {
                      this.setState({tgl_rencana: date});
                    }}
                  />
                </View>
                <View>
                  <Text style={[{marginTop: 10}, styles.fontLabel]}>
                    Uraian Rencana Tindak Lanjut
                  </Text>
                </View>
                <View>
                  <Textarea
                    style={styles.textArea}
                    rowSpan={3}
                    bordered
                    placeholderTextColor={colors.gray02}
                    value={this.state.uraian_rencana}
                    placeholder="Tulis Uraian Rencana .."
                    onChangeText={text => this.setState({uraian_rencana: text})}
                  />
                </View>
                <View>
                  <Text style={[{marginTop: 5}, styles.fontLabel]}>PIC</Text>
                </View>
                <View>
                  <SearchableDropdown
                    data={PIC}
                    searchByApi={false}
                    displayData="NAMA_LIST"
                    selectedMethod={item =>
                      this.setState({
                        pic_pelaksana: item.NAMA_LIST,
                        ID_PIC: item.ID_OPTIONS_LIST,
                      })
                    }
                    placeholder="Pilih PIC ..."
                  />
                </View>
                <View>
                  <Text style={[{marginTop: 10}, styles.fontLabel]}>
                    Bank Usrek
                  </Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 5}}>
                    <SearchableDropdown
                      api="usrek/get_all_data_usrek"
                      searchByApi={false}
                      displayData="no_usrek"
                      selectedMethod={item =>
                        this.setState(
                          {
                            no_usrek: item.no_usrek,
                            id_rab: item.id_usrek,
                          },
                          function() {
                            this.setState({
                              isLoadingBay: false,
                            });
                          },
                        )
                      }
                      placeholder="Pilih Bank Usrek ..."
                    />
                  </View>
                  <View
                    style={{
                      marginLeft: 5,
                      flex: 1,
                      borderRadius: 5,
                    }}>
                    <Button
                      onPress={() =>
                        this.props.navigation.navigate('InputUsrek')
                      }
                      style={{
                        flex: 1,
                        borderRadius: 5,
                        backgroundColor: colors.greenpln,
                      }}>
                      <View style={{flex: 1, alignItems: 'center'}}>
                        <Icon2 name="plus" size={25} color="white" />
                      </View>
                    </Button>
                  </View>
                </View>

                <View style={{marginTop: 20}}>
                  {this.state.isLoadingSave === true ? (
                    <View style={{flex: 1, alignItems: 'center'}}>
                      <ActivityIndicator />
                    </View>
                  ) : (
                    <Button
                      onPress={() => this.onKirimData()}
                      style={{
                        flex: 1,
                        marginLeft: 5,
                        backgroundColor: colors.green01,
                      }}>
                      <View style={{flex: 1}}>
                        <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
                          Simpan
                        </Text>
                      </View>
                    </Button>
                  )}
                </View>
              </View>
            )}
          </CardItem>
        </Content>
      </Container>
    );
  }
}
