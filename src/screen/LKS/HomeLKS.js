/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  StatusBar,
  View,
  ImageBackground,
  ScrollView,
  Platform,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
} from 'react-native';
import {Text, Container, Header, Left, Right, Button} from 'native-base';
import colors from '../../res/colors';
import FloatingButtonLKS from '../../library/component/FloatingButtonLKS';
import ListLKS from './ItemHomeLKS/ListLKS';
import Icon from 'react-native-vector-icons/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import RadioButton from '../../library/component/CustomRadioButton';
import styles from '../../res/styles/Form';
import SearchableDropdown from '../../library/component/SearchableDropdown';
export default class HomeLKS extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      isFilterVisible: false,
      isLoading: false,
      isDisabledGI: true,
      isLoadingGI: false,
      id_ultg: '',
      id_gardu_induk: '',
      status_pemantauan: '',
      status_penyelesaian: '',
      visibleInput: true,
      placeholderGI: 'Pilih ULTG Dulu',
    };
  }

  onResetFilter() {
    this.setState(
      {
        id_ultg: '',
        nm_ultg: 'ALL ULTG',
        id_gardu_induk: '',
        nm_gardu_induk: 'ALL GARDU INDUK',
        isDisabledGI: true,
        status_pemantauan: '',
        status_penyelesaian: '',
      },
      function() {
        this.setState(
          {
            isFilterVisible: false,
            isLoading: true,
          },
          function() {
            this.setState({
              isLoading: false,
            });
          },
        );
      },
    );
  }

  FilterModal = () => {
    this.setState({isFilterVisible: !this.state.isFilterVisible});
  };
  onFilter() {
    this.setState(
      {
        isFilterVisible: false,
        isLoading: true,
      },
      function() {
        this.setState({
          isLoading: false,
        });
      },
    );
  }
  componentDidMount() {
    var a = ['ba', 'af', 'cu', 'we', 'je'];
    a.sort();
    console.log('sort', a);
    // AsyncStorage.getItem('DataUser').then(value =>
    //   this.setState(
    //     {
    //       DataUser: JSON.parse(value),
    //     },
    //     function() {
    //       const jabatan = this.state.DataUser.detail_jabatan[0];
    //       if (jabatan.nm_jenis_pegawai === 'MULTG') {
    //         this.setState({
    //           visibleInput: false,
    //         });
    //       } else {
    //         this.setState({
    //           visibleInput: true,
    //         });
    //       }
    //     },
    //   ),
    // );
  }

  render() {
    return (
      <Container style={{flex: 1, display: 'flex'}}>
        <LinearGradient
          // source={require('../../res/images/BGdashboard.jpg')}
          colors={[colors.greenpln, 'white']}
          style={{
            flex: 1,
          }}>
          <Header
            transparent
            style={{
              marginTop: Platform.OS === 'ios' ? 0 : 0,
              borderBottomWidth: 0,
            }}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'center',
              }}>
              <View style={{flex: 6, justifyContent: 'center'}}>
                <Text
                  style={{
                    flex: 1,
                    marginLeft: 10,
                    textAlignVertical: 'center',
                    fontWeight: 'bold',
                    fontSize: 20,
                    color: 'white',
                  }}>
                  Lembar Ketidaksuaian
                </Text>
              </View>
              <View style={{flex: 1, marginRight: 5}}>
                <Button
                  onPress={() => this.FilterModal()}
                  style={{flex: 1, justifyContent: 'center'}}
                  transparent>
                  <Icon name="sort" size={30} style={{color: 'white'}} />
                </Button>
                <Modal
                  style={{
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                    marginBottom: 0,
                    marginTop: 0,
                    marginRight: 0,
                  }}
                  animationIn={'slideInRight'}
                  isVisible={this.state.isFilterVisible}
                  onBackdropPress={this.FilterModal}
                  backdropOpacity={0}
                  animationOut={'slideOutRight'}
                  animationInTiming={1000}>
                  <LinearGradient
                    colors={[colors.greenpln, 'white']}
                    style={{
                      flex: 1,
                      padding: 15,
                      width: '70%',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        marginTop: 25,
                        borderBottomWidth: 1,
                        paddingBottom: 5,
                        borderColor: colors.greenpln,
                        justifyContent: 'flex-end',
                      }}>
                      <View style={{flex: 2, justifyContent: 'center'}}>
                        <Text
                          style={{
                            marginLeft: 10,
                            fontSize: 20,
                            fontWeight: 'bold',
                          }}>
                          Filter
                        </Text>
                      </View>

                      <TouchableOpacity
                        onPress={() => this.onResetFilter()}
                        style={{
                          flex: 2,
                          borderRadius: 2,
                          justifyContent: 'center',
                          backgroundColor: colors.greenpln,
                        }}>
                        <Text
                          style={{
                            color: 'white',
                            fontWeight: 'bold',
                            textAlign: 'center',
                          }}>
                          Reset Filter
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <ScrollView>
                      <View style={{marginTop: 10}}>
                        <Text
                          style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 12,
                            marginLeft: 5,
                          }}>
                          ULTG
                        </Text>
                      </View>
                      <View style={{marginTop: 10}}>
                        <SearchableDropdown
                          api="ultg/get_all_data_ultg"
                          searchByApi={false}
                          displayData="nm_ultg"
                          otherList={{
                            nm_ultg: 'ALL ULTG',
                            id_ultg: '',
                          }}
                          selectedMethod={item =>
                            this.setState(
                              {
                                isLoadingGI: true,
                                isDisabledGI: false,
                                nm_ultg: item.nm_ultg,
                                id_ultg: item.id_ultg,
                                placeholderGI: 'Pilih Gardu Induk...',
                              },
                              function() {
                                this.setState({
                                  isLoadingGI: false,
                                });
                              },
                            )
                          }
                          selected={this.state.nm_ultg}
                          placeholder="Pilih ULTG..."
                        />
                      </View>
                      <View style={{marginTop: 10}}>
                        <Text
                          style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 12,
                            marginLeft: 5,
                          }}>
                          Gardu Induk
                        </Text>
                      </View>
                      <View style={{marginTop: 10}}>
                        {this.state.isLoadingGI === true ? (
                          <View style={{alignItems: 'center'}}>
                            <ActivityIndicator />
                          </View>
                        ) : (
                          <SearchableDropdown
                            api="gardu_induk/get_all_data_gardu_induk"
                            bodyApi={[
                              {
                                name: 'id_ultg',
                                value: this.state.id_ultg,
                              },
                            ]}
                            searchByApi={false}
                            disabled={this.state.isDisabledGI}
                            displayData="nm_gardu_induk"
                            otherList={{
                              nm_gardu_induk: 'ALL GARDU INDUK',
                              id_gardu_induk: '',
                            }}
                            selectedMethod={item =>
                              this.setState({
                                nm_gardu_induk: item.nm_gardu_induk,
                                id_gardu_induk: item.id_gardu_induk,
                              })
                            }
                            selected={this.state.nm_gardu_induk}
                            placeholder={this.state.placeholderGI}
                          />
                        )}
                      </View>
                      <View style={{marginTop: 10}}>
                        <Text
                          style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 12,
                            marginLeft: 5,
                          }}>
                          Status Pemantauan
                        </Text>
                      </View>
                      <View
                        style={{
                          marginTop: 5,
                          backgroundColor: 'white',
                          borderRadius: 10,
                        }}>
                        <RadioButton
                          option={StatusPemantauan}
                          selected={this.state.status_pemantauan}
                          displayData="NAMA_LIST"
                          onPressMethod={data =>
                            this.setState({
                              status_pemantauan: data.ID_LIST,
                            })
                          }
                        />
                      </View>
                      <View style={{marginTop: 10}}>
                        <Text
                          style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 12,
                            marginLeft: 5,
                          }}>
                          Status Penyelesaian
                        </Text>
                      </View>
                      <View
                        style={{
                          marginTop: 5,
                          backgroundColor: 'white',
                          borderRadius: 10,
                        }}>
                        <RadioButton
                          option={StatusPenyelesaian}
                          selected={this.state.status_penyelesaian}
                          displayData="NAMA_LIST"
                          onPressMethod={data =>
                            this.setState({
                              status_penyelesaian: data.ID_LIST,
                            })
                          }
                        />
                      </View>
                      <View style={{marginTop: 20}}>
                        <Button
                          onPress={() => this.onFilter()}
                          style={{
                            flex: 1,
                            backgroundColor: colors.green01,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Filter Data
                            </Text>
                          </View>
                        </Button>
                      </View>
                    </ScrollView>
                  </LinearGradient>
                </Modal>
              </View>
            </View>
          </Header>
          {this.state.isLoading === true ? (
            <View style={{alignItems: 'center'}}>
              <ActivityIndicator size="large" />
            </View>
          ) : (
            <View style={{flex: 1, flexDirection: 'column'}}>
              <ListLKS
                navigation={this.props.navigation}
                id_ultg={this.state.id_ultg}
                id_gardu_induk={this.state.id_gardu_induk}
                status_pemantauan={this.state.status_pemantauan}
                status_penyelesaian={this.state.status_penyelesaian}
              />
            </View>
          )}
        </LinearGradient>
        <FloatingButtonLKS navigation={this.props.navigation} />

        {/* {this.state.visibleInput === true ? (
          <FloatingButtonLKS navigation={this.props.navigation} />
        ) : null} */}
      </Container>
    );
  }
}

const StatusPemantauan = [
  {NAMA_LIST: 'ALL STATUS', ID_LIST: ''},
  {NAMA_LIST: 'BELUM DITANGGAPI', ID_LIST: 'BELUM DITANGGAPI'},
  {NAMA_LIST: 'BELUM DISELESAIKAN', ID_LIST: 'BELUM DISELESAIKAN'},
  {NAMA_LIST: 'SELESAI', ID_LIST: 'SELESAI'},
];

const StatusPenyelesaian = [
  {NAMA_LIST: 'ALL STATUS', ID_LIST: ''},
  {NAMA_LIST: 'PENDING', ID_LIST: 'PENDING'},
  {NAMA_LIST: 'ON PROGRESS', ID_LIST: 'ON PROGRESS'},
  {NAMA_LIST: 'DONE', ID_LIST: 'DONE'},
  {NAMA_LIST: 'CLOSE', ID_LIST: 'CLOSE'},
];
