/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Image,
  View,
  TextInput,
  Platform,
  AsyncStorage,
  ActivityIndicator,
  ScrollView,
  Alert,
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Button,
  Content,
  CardItem,
  Text,
  Textarea,
} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import SearchableDropdown from '../../../library/component/SearchableDropdown';
import styles from '../../../res/styles/Form';
import colors from '../../../res/colors/index';
import StepIndicator from 'react-native-step-indicator';
import RadioButton from '../../../library/component/CustomRadioButton';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-crop-picker';
import Modal from 'react-native-modal';
import GlobalConfig from '../../../library/network/GlobalConfig';
import FilePickerManager from 'react-native-file-picker';

const ListUsulan = [
  {NAMA_LIST: 'TRANSMISI', ID_OPTIONS_LIST: '1'},
  {NAMA_LIST: 'GARDU INDUK', ID_OPTIONS_LIST: '2'},
  {NAMA_LIST: 'SARANA', ID_OPTIONS_LIST: '3'},
  {NAMA_LIST: 'BANGHAL', ID_OPTIONS_LIST: '4'},
];

const ListPrioritas = [
  {NAMA_LIST: 'Biasa', ID_OPTIONS_LIST: '1'},
  {NAMA_LIST: 'Sedang', ID_OPTIONS_LIST: '2'},
  {NAMA_LIST: 'Urgent', ID_OPTIONS_LIST: '3'},
];

const LokasiPekerjaan = [
  {
    id: 0,
    name: 'GI(Gardu Induk)',
  },
  {
    id: 1,
    name: 'SITE UPT',
  },
  {
    id: 2,
    name: 'SITE ULTG GRESIK',
  },
  {
    id: 3,
    name: 'SITE ULTG GRESIK',
  },
];

const labels = ['Usulan Kerja', 'GI & Bay', 'Detail Pekerjaan'];
export default class InputUsrek extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading1: true,
      currentPosition: 0,
      enabledBay: true,

      isModalVisible: false,
      isModalVisible2: false,

      isOpenImage: false,
      isOpenImage2: false,

      sourcepdf: '-',
      list_image: [],
      list_image2: [],

      //1
      id_bidang: '',
      jenis_fungsi_usulan: '',
      judul_pekerjaan: '',
      justifikasi_usulan: '',
      lokasi_pekerjaan: '',
      wilayah: '',

      //2
      id_gardu_induk: '',
      id_bay: '',
      id_tegangan: '',
      detail_lokasi: '',

      dokumen: '',
    };
  }

  movePage() {
    if (this.state.currentPosition === 0) {
      if (
        this.state.id_bidang === '' ||
        this.state.jenis_fungsi_usulan === '' ||
        this.state.judul_pekerjaan === '' ||
        this.state.justifikasi_usulan === '' ||
        this.state.lokasi_pekerjaan === '' ||
        this.state.wilayah === ''
      ) {
        Alert.alert('Lengkapi Form!', 'Form masih ada yang kosong.', [
          {
            text: 'Okay',
          },
        ]);
      } else {
        this.setState(
          {
            isLoading1: true,
            currentPosition: this.state.currentPosition + 1,
          },
          function() {
            this.setState({isLoading1: false});
          },
        );
      }
    } else if (this.state.currentPosition === 1) {
      if (
        this.state.id_gardu_induk === '' ||
        this.state.id_bay === '' ||
        this.state.id_tegangan === '' ||
        this.state.detail_lokasi === ''
      ) {
        Alert.alert('Lengkapi Form!', 'Form masih ada yang kosong.', [
          {
            text: 'Okay',
          },
        ]);
      } else {
        this.setState(
          {
            isLoading2: true,
            currentPosition: this.state.currentPosition + 1,
          },
          function() {
            this.setState({isLoading2: false});
          },
        );
      }
    }
  }

  backPage() {
    this.setState({
      currentPosition: this.state.currentPosition - 1,
    });
  }

  componentDidMount() {
    AsyncStorage.getItem('DataUser').then(value =>
      this.setState(
        {
          DataUser: JSON.parse(value),
        },
        function() {
          this.setState({
            isLoading1: false,
          });
          console.log('user aa', this.state.DataUser.user.nip);
        },
      ),
    );
  }

  onTambahFile() {
    FilePickerManager.showFilePicker(null, response => {
      console.log('Response = ', response);
      this.setState({
        dokumen: response,
      });
      if (response.didCancel) {
        console.log('User cancelled file picker');
      } else if (response.error) {
        console.log('FilePickerManager Error: ', response.error);
      } else {
        this.setState({
          file: response,
        });
      }
    });
  }

  toggleModal = () => {
    this.setState({isModalVisible: !this.state.isModalVisible});
  };
  toggleModal2 = () => {
    this.setState({isModalVisible2: !this.state.isModalVisible2});
  };

  pickSingle(cropping, mediaType = 'photo') {
    ImagePicker.openPicker({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        console.log('image', image);
        this.setState(
          {
            isModalVisible: false,
            list_image: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
          },
          function() {
            console.log('mlaaku', this.state.list_image);
            this.setState({
              isOpenImage: true,
            });
          },
        );
      })
      .catch(e => {
        console.log(e);
        // Alert.alert(e.message ? e.message : e);
      });
  }

  pickSingleWithCamera(cropping, mediaType = 'photo') {
    ImagePicker.openCamera({
      cropping: cropping,
      compressImageQuality: 0.2,
      includeExif: true,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible: false,
            list_image: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
          },
          function() {
            console.log('mlaaku', this.state.list_image);
            this.setState({
              isOpenImage: true,
            });
          },
        );
      })
      .catch(e => alert(e));
    this.toggleModal;
  }

  pickSingle2(cropping, mediaType = 'photo') {
    ImagePicker.openPicker({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible2: false,
            list_image2: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
          },
          function() {
            console.log('mlaaku', this.state.list_image2);
            this.setState({
              isOpenImage2: true,
            });
          },
        );
      })
      .catch(e => {
        console.log(e);
        // Alert.alert(e.message ? e.message : e);
      });
  }

  pickSingleWithCamera2(cropping, mediaType = 'photo') {
    ImagePicker.openCamera({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible2: false,
            list_image2: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
          },
          function() {
            console.log('mlaaku', this.state.list_image2);
            this.setState({
              isOpenImage2: true,
            });
          },
        );
      })
      .catch(e => alert(e));
    this.toggleModal2;
  }

  onKirimData() {
    this.setState({
      isLoadingSave: true,
    });
    if (
      this.state.id_jenis_pekerjaan === '' ||
      this.state.uraian_detail_rencana === '' ||
      this.state.penjelasan_dokumentasi_satu === '' ||
      this.state.prioritas_user === '' ||
      this.state.perkiraan_biaya === ''
    ) {
      alert('Form belum lengkap');
      this.setState({
        isLoadingSave: false,
      });
    } else {
      var url = GlobalConfig.SERVERHOST + 'usrek/add_data_usrek';
      var formData = new FormData();
      formData.append('nip', this.state.DataUser.user.nip);
      formData.append('id_bidang', this.state.id_bidang);
      formData.append('jenis_fungsi_usulan', this.state.jenis_fungsi_usulan);
      formData.append('judul_pekerjaan', this.state.judul_pekerjaan);
      formData.append('justifikasi_usulan', this.state.justifikasi_usulan);
      formData.append('lokasi_pekerjaan', this.state.lokasi_pekerjaan);
      formData.append('wilayah', this.state.wilayah);
      formData.append('id_gardu_induk', this.state.id_gardu_induk);
      formData.append('id_bay', this.state.id_bay);
      formData.append('id_tegangan', this.state.id_tegangan);
      formData.append('detail_lokasi', this.state.detail_lokasi);
      formData.append('id_jenis_pekerjaan', this.state.id_jenis_pekerjaan);
      formData.append(
        'uraian_detail_rencana',
        this.state.uraian_detail_rencana,
      );
      formData.append('prioritas_user', this.state.prioritas_user);
      formData.append('perkiraan_biaya', this.state.perkiraan_biaya);
      formData.append('dokumen_rab', {
        uri: this.state.dokumen.uri,
        name: this.state.dokumen.fileName,
        type: this.state.dokumen.type,
      })
      this.setState({});
      if (this.state.penjelasan_dokumentasi_satu !== '') {
        formData.append(
          'dokumentasi_satu',
          {
            uri: this.state.list_image[0].uri,
            name: this.state.list_image[0].name + '.jpg',
            type: 'image/jpg',
          },
          'file',
        );
        formData.append(
          'penjelasan_dokumentasi_dua',
          this.state.penjelasan_dokumentasi_dua,
        );
      }

      if (this.state.penjelasan_dokumentasi_dua !== '') {
        formData.append(
          'dokumentasi_dua',
          {
            uri: this.state.list_image2[0].uri,
            name: this.state.list_image2[0].name + '.jpg',
            type: 'image/jpg',
          },
          'file',
        );
        formData.append(
          'penjelasan_dokumentasi_dua',
          this.state.penjelasan_dokumentasi_dua,
        );
      }
      console.log('all FD', formData);
      fetch(url, {
        method: 'POST',

        body: formData,
        redirect: 'follow',
      })
        .then(response => response.json())
        .then(response => {
          console.log('', response);
          if (response.status === 'success') {
            this.setState(
              {
                isLoadingSave: false,
              },
              function() {
                Alert.alert('Data Saved', response.message, [
                  {
                    text: 'Okay',
                  },
                ]);
                AsyncStorage.setItem('SavedUsrek', '1');
                this.props.navigation.goBack();
              },
            );
          } else {
            this.setState({
              isLoadingSave: false,
            });
            Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [
              {
                text: 'Okay',
              },
            ]);
          }
        })
        .catch(error => {
          this.setState({
            isLoadingSave: false,
          });
          Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [
            {
              text: 'Okay',
            },
          ]);
          console.log(error);
        });
    }
  }

  render() {
    return (
      <Container>
        <Header
          transparent
          style={{
            marginTop: Platform.OS === 'ios' ? 0 : 0,
            borderBottomWidth: 0,
            backgroundColor: colors.greenpln,
          }}>
          <View
            style={{
              flex: 2,
              justifyContent: 'center',
            }}>
            <Button
              onPress={() => this.props.navigation.goBack()}
              transparent
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <Icon
                name="md-arrow-round-back"
                size={23}
                style={{color: 'white'}}
              />
              <Text
                uppercase={false}
                style={{
                  flex: 1,
                  textAlignVertical: 'center',
                  fontWeight: 'bold',
                  fontSize: 20,
                  color: 'white',
                }}>
                Input Usrek
              </Text>
            </Button>
          </View>
          <View style={{flex: 1}}/>
        </Header>
        <Content>
          <View style={{marginTop: 20, marginBottom: 10}}>
            <StepIndicator
              customStyles={styles.customStyles}
              currentPosition={this.state.currentPosition}
              labels={labels}
              stepCount={3}
            />
          </View>
          {this.state.currentPosition === 0 ? (
            <View>
              {this.state.isLoading1 === true ? (
                <View style={{alignItems: 'center'}}>
                  <ActivityIndicator size="large" />
                </View>
              ) : (
                <CardItem>
                  <View style={{flex: 1}}>
                    <View>
                      <Text style={styles.fontLabel}>NIP</Text>
                    </View>
                    <View>
                      <TextInput
                        style={styles.fontTextInput}
                        rowSpan={1}
                        editable={true}
                        bordered
                        value={this.state.DataUser.user.nip}
                        placeholder="Input NIP..."
                        onChangeText={text => this.setState({nip: text})}
                      />
                    </View>

                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Bidang Pekerjaan
                      </Text>
                    </View>
                    <View>
                      <SearchableDropdown
                        api="bidang/get_all_data_bidang"
                        displayData="nm_bidang"
                        selectedMethod={item =>
                          this.setState({
                            nm_bidang: item.nm_bidang,
                            id_bidang: item.id_bidang,
                          })
                        }
                        placeholder="Pilih Bidang Pekerjaan..."
                      />
                    </View>

                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Jenis Fungsi Ulasan
                      </Text>
                    </View>
                    <View>
                      <SearchableDropdown
                        data={ListUsulan}
                        displayData="NAMA_LIST"
                        selectedMethod={item =>
                          this.setState({
                            jenis_fungsi_usulan: item.NAMA_LIST,
                            id_fungsi: item.ID_OPTIONS_LIST,
                          })
                        }
                        placeholder="Pilih Jenis Usulan..."
                      />
                    </View>
                    <View style={{marginTop: 10}}>
                      <Text style={styles.fontLabel}>Judul Pekerjaan</Text>
                    </View>
                    <View>
                      <TextInput
                        style={styles.fontTextInput}
                        rowSpan={1}
                        editable={true}
                        bordered
                        value={this.state.judul_pekerjaan}
                        placeholder="Input Judul Pekerjaan..."
                        onChangeText={text =>
                          this.setState({judul_pekerjaan: text})
                        }
                      />
                    </View>

                    <View style={{marginTop: 10}}>
                      <Text style={styles.fontLabel}>Justifikasi Usulan</Text>
                    </View>
                    <View>
                      <TextInput
                        style={styles.fontTextInput}
                        rowSpan={1}
                        editable={true}
                        bordered
                        value={this.state.justifikasi_usulan}
                        placeholder="Input Justifikasi Usulan..."
                        onChangeText={text =>
                          this.setState({justifikasi_usulan: text})
                        }
                      />
                    </View>

                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Lokasi Pekerjaan
                      </Text>
                    </View>
                    <View>
                      <SearchableDropdown
                        data={LokasiPekerjaan}
                        displayData="name"
                        selectedMethod={item =>
                          this.setState({
                            lokasi_pekerjaan: item.name,
                            UNIT_CODE: item.id,
                          })
                        }
                        placeholder="Pilih Lokasi Pekerjaan..."
                      />
                    </View>
                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Wilayah
                      </Text>
                    </View>
                    <View>
                      <SearchableDropdown
                        api="ultg/get_all_data_ultg"
                        displayData="wilayah"
                        selectedMethod={item =>
                          this.setState({
                            wilayah: item.wilayah,
                            id_ultg: item.id_ultg,
                          })
                        }
                        placeholder="Pilih Wilayah Kerja..."
                      />
                    </View>

                    <View style={{marginTop: 40}}>
                      <View style={{flexDirection: 'row'}}>
                        <Button
                          onPress={() => this.backPage()}
                          style={{
                            flex: 1,
                            marginRight: 5,
                            backgroundColor: colors.greenpln,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Prev
                            </Text>
                          </View>
                        </Button>
                        <Button
                          onPress={() => this.movePage()}
                          style={{
                            flex: 1,
                            marginLeft: 5,
                            backgroundColor: colors.greenpln,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Next
                            </Text>
                          </View>
                        </Button>
                      </View>
                    </View>
                  </View>
                </CardItem>
              )}
            </View>
          ) : this.state.currentPosition === 1 ? (
            <CardItem>
              {this.state.isLoading1 ? (
                <View style={{alignItems: 'center', flex: 1}}>
                  <ActivityIndicator />
                </View>
              ) : (
                <View style={{flex: 1}}>
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Gardu Induk
                    </Text>
                  </View>
                  <View>
                    <SearchableDropdown
                      api="gardu_induk/get_all_data_gardu_induk"
                      searchByApi={false}
                      displayData="nm_gardu_induk"
                      selectedMethod={item =>
                        this.setState(
                          {
                            nm_gardu_induk: item.nm_gardu_induk,
                            id_gardu_induk: item.id_gardu_induk,
                            isLoadingBay: true,
                            enabledBay: false,
                          },
                          function() {
                            this.setState({
                              isLoadingBay: false,
                            });
                          },
                        )
                      }
                      placeholder="Pilih Gardu Induk..."
                      selected={this.state.nm_gardu_induk}
                    />
                  </View>

                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>BAY</Text>
                  </View>
                  <View>
                    {!this.state.isLoadingBay ? (
                      <SearchableDropdown
                        api="bay/bay_gardu_induk"
                        searchByApi={false}
                        bodyApi={[
                          {
                            name: 'id_gardu_induk',
                            value: this.state.id_gardu_induk,
                          },
                        ]}
                        displayData="nm_bay"
                        selectedMethod={item =>
                          this.setState({
                            nm_bay: item.nm_bay,
                            id_bay: item.id_bay,
                            nextButton: false,
                          })
                        }
                        disabled={this.state.enabledBay}
                        placeholder="Pilih BAY..."
                      />
                    ) : (
                      <View style={{alignItems: 'center'}}>
                        <ActivityIndicator />
                      </View>
                    )}
                  </View>
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Level Tegangan
                    </Text>
                  </View>
                  <View>
                    <SearchableDropdown
                      api="tegangan/get_all_data_tegangan"
                      searchByApi={false}
                      displayData="volume_tegangan"
                      // otherList={{volume_tegangan: 'Lainnya', id_tegangan: '0'}}
                      selectedMethod={item =>
                        this.setState({
                          volume_tegangan: item.volume_tegangan,
                          id_tegangan: item.id_tegangan,
                        })
                      }
                      placeholder="Pilih Level Tegangan..."
                    />
                  </View>
                  {this.state.id_tegangan === '0' ? (
                    <View>
                      <View>
                        <Text style={[{marginTop: 10}, styles.fontLabel]}>
                          Keterangan Tegangan Lainnya
                        </Text>
                      </View>
                      <View>
                        <Textarea
                          style={styles.textArea}
                          rowSpan={3}
                          bordered
                          placeholderTextColor={colors.gray02}
                          value={this.state.ketTegangan}
                          placeholder="Isi jika level tegangan yang diinput tidak terdapat pada list pilihan ..."
                          onChangeText={text =>
                            this.setState({ketTegangan: text})
                          }
                        />
                      </View>
                    </View>
                  ) : (
                    <View />
                  )}
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Detail Lokasi GI/Jaringan
                    </Text>
                  </View>
                  <View>
                    <Textarea
                      style={styles.textArea}
                      rowSpan={3}
                      bordered
                      placeholderTextColor={colors.gray02}
                      value={this.state.Detail_lokasi_gi}
                      placeholder="Input Detail Lokasi GI/Jaringan ..."
                      onChangeText={text =>
                        this.setState({detail_lokasi: text})
                      }
                    />
                  </View>

                  <View style={{marginTop: 40}}>
                    <View style={{flexDirection: 'row'}}>
                      <Button
                        onPress={() => this.backPage()}
                        style={{
                          flex: 1,
                          marginRight: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Prev
                          </Text>
                        </View>
                      </Button>
                      <Button
                        onPress={() => this.movePage()}
                        style={{
                          flex: 1,
                          marginLeft: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Next
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </View>
                </View>
              )}
            </CardItem>
          ) : (
            <CardItem>
              {this.state.isLoading2 ? (
                <View style={{alignItems: 'center', flex: 1}}>
                  <ActivityIndicator />
                </View>
              ) : (
                <View style={{flex: 1}}>
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Jenis Pekerjaan
                    </Text>
                  </View>
                  <View>
                    <SearchableDropdown
                      api="jenis_pekerjaan/get_all_data_jenis_pekerjaan"
                      displayData="nm_jenis_pekerjaan"
                      selectedMethod={item =>
                        this.setState({
                          nm_jenis_pekerjaan: item.nm_jenis_pekerjaan,
                          id_jenis_pekerjaan: item.id_jenis_pekerjaan,
                        })
                      }
                      placeholder="Pilih Jenis Pekerjaan..."
                    />
                  </View>

                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>
                      Uraian Detail Rencana Pekerjaan
                    </Text>
                  </View>
                  <View>
                    <Textarea
                      style={styles.textArea}
                      rowSpan={3}
                      bordered
                      placeholderTextColor={colors.gray02}
                      value={this.state.uraian_detail_rencana}
                      placeholder="Input Detail Rencana Pekerjaan"
                      onChangeText={text =>
                        this.setState({uraian_detail_rencana: text})
                      }
                    />
                  </View>
                  <View>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <View style={{flex: 1, marginRight: 5}}>
                        <View>
                          <Text style={[{marginTop: 10}, styles.fontLabel]}>
                            Dokumentasi 1
                          </Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            borderColor: colors.greenpln,
                            borderWidth: 1,
                            borderRadius: 5,
                          }}>
                          <View style={{backgroundColor: colors.greenpln}}>
                            <View style={{marginLeft: 10, padding: 5}}>
                              <Text
                                style={{fontWeight: 'bold', color: 'white'}}>
                                Tambah File
                              </Text>
                            </View>
                          </View>
                          <View>
                            <View style={{height: '100%', paddingVertical: 25}}>
                              <Button
                                onPress={this.toggleModal}
                                style={{justifyContent: 'center'}}
                                transparent>
                                {this.state.isOpenImage === true ? (
                                  <Image
                                    source={{uri: this.state.list_image[0].uri}}
                                    style={{
                                      height: 90,
                                      width: 130,
                                      resizeMode: 'contain',
                                    }}
                                  />
                                ) : (
                                  <Image
                                    source={require('../../../res/images/folder.png')}
                                    style={{
                                      height: 90,
                                      width: 130,
                                      resizeMode: 'contain',
                                    }}
                                  />
                                )}
                              </Button>
                            </View>
                          </View>
                        </View>
                        <Textarea
                          style={styles.textArea}
                          rowSpan={1.7}
                          bordered
                          placeholderTextColor={colors.gray02}
                          value={this.state.penjelasan_dokumentasi_satu}
                          placeholder="Penjelasan Dokumentasi 1"
                          onChangeText={text =>
                            this.setState({penjelasan_dokumentasi_satu: text})
                          }
                        />
                      </View>

                      <View style={{flex: 1, marginLeft: 5}}>
                        <View>
                          <Text style={[{marginTop: 10}, styles.fontLabel]}>
                            Dokumentasi 2
                          </Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            borderColor: colors.greenpln,
                            borderWidth: 1,
                            borderRadius: 5,
                          }}>
                          <View style={{backgroundColor: colors.greenpln}}>
                            <View style={{marginLeft: 10, padding: 5}}>
                              <Text
                                style={{fontWeight: 'bold', color: 'white'}}>
                                Tambah File
                              </Text>
                            </View>
                          </View>
                          <View>
                            <View style={{height: '100%', paddingVertical: 25}}>
                              <Button
                                onPress={this.toggleModal2}
                                style={{justifyContent: 'center'}}
                                transparent>
                                {this.state.isOpenImage2 === true ? (
                                  <Image
                                    source={{
                                      uri: this.state.list_image2[0].uri,
                                    }}
                                    style={{
                                      height: 90,
                                      width: 130,
                                      resizeMode: 'contain',
                                    }}
                                  />
                                ) : (
                                  <Image
                                    source={require('../../../res/images/folder.png')}
                                    style={{
                                      height: 90,
                                      width: 130,
                                      resizeMode: 'contain',
                                    }}
                                  />
                                )}
                              </Button>
                            </View>
                          </View>
                        </View>
                        <Textarea
                          style={styles.textArea}
                          rowSpan={1.7}
                          bordered
                          placeholderTextColor={colors.gray02}
                          value={this.state.penjelasan_dokumentasi_dua}
                          placeholder="Penjelasan Dokumentasi 2"
                          onChangeText={text =>
                            this.setState({penjelasan_dokumentasi_dua: text})
                          }
                        />
                      </View>
                    </View>
                  </View>
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Prioritas User
                    </Text>
                  </View>
                  <View>
                    <RadioButton
                      option={ListPrioritas}
                      displayData="NAMA_LIST"
                      onPressMethod={data =>
                        this.setState({
                          prioritas_user: data.ID_OPTIONS_LIST,
                          NAMA_LIST: data.NAMA_LIST,
                        })
                      }
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>Perkiraan Biaya</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.perkiraan_biaya}
                      placeholder="Input Perkiraan Biaya.."
                      onChangeText={text =>
                        this.setState({perkiraan_biaya: text})
                      }
                    />
                  </View>
                  <View style={{flex: 1, marginLeft: 5}}>
                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Upload RAB
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        borderColor: colors.greenpln,
                        borderWidth: 1,
                        borderRadius: 5,
                        flexDirection: 'row',
                      }}>
                      <View
                        style={{
                          flex: 5,
                          flexDirection: 'row',
                          marginLeft: 5,
                          marginVertical: 5,
                          justifyContent: 'center',
                          borderWidth: 0.5,
                          borderRadius: 5,
                          borderColor: colors.greenpln,
                        }}>
                        <View
                          style={{
                            flex: 1,
                            justifyContent: 'center',
                            marginLeft: 5,
                          }}>
                          <Icon2
                            name="file-text"
                            color={colors.greenpln}
                            size={20}
                          />
                        </View>
                        <View style={{flex: 7, justifyContent: 'center'}}>
                          <ScrollView horizontal={true}>
                            <Text style={{textAlignVertical: 'center'}}>
                              {this.state.dokumen.path}
                            </Text>
                          </ScrollView>
                        </View>
                      </View>
                      <View
                        style={{
                          flex: 2,
                          width: '30%',
                          paddingVertical: 0,
                          paddingHorizontal: 5,
                        }}>
                        <Button
                          onPress={() => this.onTambahFile()}
                          style={{
                            marginVertical: 5,
                            justifyContent: 'center',
                            backgroundColor: colors.greenpln,
                          }}
                          transparent>
                          <Text
                            style={{
                              fontSize: 14,
                              textAlign: 'center',
                              fontWeight: 'bold',
                              color: 'white',
                            }}>
                            Select
                          </Text>
                        </Button>
                      </View>
                    </View>
                  </View>

                  {/* modal image 1*/}
                  <Modal
                    style={{justifyContent: 'flex-end'}}
                    backdropOpacity={0.3}
                    animationIn={'slideInUp'}
                    animationOut={'slideOutDown'}
                    isVisible={this.state.isModalVisible}
                    onBackdropPress={this.toggleModal}
                    onBackButtonPress={this.toggleModal}>
                    <View style={{marginBottom: 10}}>
                      <View
                        style={{
                          borderRadius: 5,
                          flexDirection: 'row',
                          paddingHorizontal: 30,
                          paddingVertical: 40,
                          backgroundColor: 'white',
                        }}>
                        <View style={{marginRight: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingleWithCamera(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/camera.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Take a Photo
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                        <View style={{marginLeft: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingle(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/galery.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Select from Galery
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                      </View>
                    </View>

                    <View style={{borderRadius: 5, marginBottom: 10}}>
                      <Button
                        onPress={this.toggleModal}
                        style={{backgroundColor: 'white'}}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{
                              color: colors.greenpln,
                              textAlign: 'center',
                            }}>
                            Cancel
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </Modal>

                  {/* modal image 2*/}
                  <Modal
                    style={{justifyContent: 'flex-end'}}
                    backdropOpacity={0.3}
                    animationIn={'slideInUp'}
                    animationOut={'slideOutDown'}
                    isVisible={this.state.isModalVisible2}
                    onBackdropPress={this.toggleModal2}
                    onBackButtonPress={this.toggleModal2}>
                    <View style={{marginBottom: 10}}>
                      <View
                        style={{
                          borderRadius: 5,
                          flexDirection: 'row',
                          paddingHorizontal: 30,
                          paddingVertical: 40,
                          backgroundColor: 'white',
                        }}>
                        <View style={{marginRight: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingleWithCamera2(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/camera.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Take a Photo
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                        <View style={{marginLeft: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingle2(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/galery.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Select from Galery
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                      </View>
                    </View>

                    <View style={{borderRadius: 5, marginBottom: 10}}>
                      <Button
                        onPress={this.toggleModal2}
                        style={{backgroundColor: 'white'}}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{
                              color: colors.greenpln,
                              textAlign: 'center',
                            }}>
                            Cancel
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </Modal>
                  <View style={{marginTop: 40}}>
                    <View style={{flexDirection: 'row'}}>
                      <Button
                        onPress={() => this.backPage()}
                        style={{
                          flex: 1,
                          marginRight: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Prev
                          </Text>
                        </View>
                      </Button>
                      {this.state.isLoadingSave === true ? (
                        <View style={{flex: 1, alignItems: 'center'}}>
                          <ActivityIndicator />
                        </View>
                      ) : (
                        <Button
                          onPress={() => this.onKirimData()}
                          style={{
                            flex: 1,
                            marginLeft: 5,
                            backgroundColor: colors.green01,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Kirim
                            </Text>
                          </View>
                        </Button>
                      )}
                    </View>
                  </View>
                </View>
              )}
            </CardItem>
          )}
        </Content>
      </Container>
    );
  }
}
