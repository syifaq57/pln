/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  StatusBar,
  ImageBackground,
  ScrollView,
  Platform,
  ActivityIndicator,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import {Container, Text, View, Header, Left, Right, Button} from 'native-base';
import colors from '../../res/colors';
import FloatingButtonUsrek from '../../library/component/FloatingButtonUsrek';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ListUsrek from './ItemHomeUsrek/ListUsrek';
import LinearGradient from 'react-native-linear-gradient';
import RadioButton from '../../library/component/CustomRadioButton';
import SearchableDropdown from '../../library/component/SearchableDropdown';
import Modal from 'react-native-modal';

export default class HomeBankUsrek extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      isFilterVisible: false,
      isLoading: false,
      id_gardu_induk: '',
      visibleInput: true,
    };
  }

  FilterModal = () => {
    this.setState({isFilterVisible: !this.state.isFilterVisible});
  };
  onFilter() {
    this.setState(
      {
        isFilterVisible: false,
        isLoading: true,
      },
      function() {
        this.setState({
          isLoading: false,
        });
      },
    );
  }

  onResetFilter() {
    this.setState(
      {
        id_gardu_induk: '',
        nm_gardu_induk: 'ALL GARDU INDUK',
      },
      function() {
        this.setState(
          {
            isFilterVisible: false,
            isLoading: true,
          },
          function() {
            this.setState({
              isLoading: false,
            });
          },
        );
      },
    );
  }
  componentDidMount() {
    // AsyncStorage.getItem('DataUser').then(value =>
    //   this.setState(
    //     {
    //       DataUser: JSON.parse(value),
    //     },
    //     function () {
    //       const jabatan = this.state.DataUser.detail_jabatan[0];
    //       if (jabatan.nm_jenis_pegawai === 'MULTG') {
    //         this.setState({
    //           visibleInput: false,
    //         });
    //       } else {
    //         this.setState({
    //           visibleInput: true,
    //         });
    //       }
    //     },
    //   ),
    // );
  }

  render() {
    return (
      <Container style={{flex: 1, display: 'flex'}}>
        <LinearGradient
          // source={require('../../res/images/BGdashboard.jpg')}
          colors={[colors.greenpln, 'white']}
          style={{
            flex: 1,
          }}>
          <Header
            transparent
            style={{
              marginTop: Platform.OS === 'ios' ? 0 : 0,
              borderBottomWidth: 0,
            }}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'center',
              }}>
              <View style={{flex: 6, justifyContent: 'center'}}>
                <Text
                  style={{
                    flex: 1,
                    marginLeft: 10,
                    textAlignVertical: 'center',
                    fontWeight: 'bold',
                    fontSize: 20,
                    color: 'white',
                  }}>
                  Bank Usrek
                </Text>
              </View>
              <View style={{flex: 1, marginRight: 5}}>
                <Button
                  onPress={() => this.FilterModal()}
                  style={{flex: 1, justifyContent: 'center'}}
                  transparent>
                  <Icon name="sort" size={30} style={{color: 'white'}} />
                </Button>
                <Modal
                  style={{
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                    marginBottom: 0,
                    marginTop: 0,
                    marginRight: 0,
                  }}
                  animationIn={'slideInRight'}
                  isVisible={this.state.isFilterVisible}
                  onBackdropPress={this.FilterModal}
                  backdropOpacity={0}
                  animationOut={'slideOutRight'}
                  animationInTiming={1000}>
                  <LinearGradient
                    colors={[colors.greenpln, 'white']}
                    style={{
                      flex: 1,
                      padding: 15,
                      width: '70%',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        marginTop: 25,
                        borderBottomWidth: 1,
                        paddingBottom: 5,
                        borderColor: colors.greenpln,
                        justifyContent: 'flex-end',
                      }}>
                      <View style={{flex: 2, justifyContent: 'center'}}>
                        <Text
                          style={{
                            marginLeft: 10,
                            fontSize: 20,
                            fontWeight: 'bold',
                          }}>
                          Filter
                        </Text>
                      </View>

                      <TouchableOpacity
                        onPress={() => this.onResetFilter()}
                        style={{
                          flex: 2,
                          borderRadius: 2,
                          justifyContent: 'center',
                          backgroundColor: colors.greenpln,
                        }}>
                        <Text
                          style={{
                            color: 'white',
                            fontWeight: 'bold',
                            textAlign: 'center',
                          }}>
                          Reset Filter
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <ScrollView>
                      <View style={{marginTop: 10}}>
                        <Text
                          style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 12,
                            marginLeft: 5,
                          }}>
                          Gardu Induk
                        </Text>
                      </View>
                      <View style={{marginTop: 10}}>
                        <SearchableDropdown
                          api="gardu_induk/get_all_data_gardu_induk"
                          searchByApi={false}
                          displayData="nm_gardu_induk"
                          otherList={{
                            nm_gardu_induk: 'ALL GARDU INDUK',
                            id_gardu_induk: '',
                          }}
                          selectedMethod={item =>
                            this.setState({
                              nm_gardu_induk: item.nm_gardu_induk,
                              id_gardu_induk: item.id_gardu_induk,
                            })
                          }
                          selected={this.state.nm_gardu_induk}
                          placeholder="Pilih Gardu Induk..."
                        />
                      </View>

                      <View style={{marginTop: 20}}>
                        <Button
                          onPress={() => this.onFilter()}
                          style={{
                            flex: 1,
                            backgroundColor: colors.green01,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Filter Data
                            </Text>
                          </View>
                        </Button>
                      </View>
                    </ScrollView>
                  </LinearGradient>
                </Modal>
              </View>
            </View>
          </Header>
          {this.state.isLoading === true ? (
            <View style={{alignItems: 'center'}}>
              <ActivityIndicator size="large" />
            </View>
          ) : (
            <View style={{flex: 1, flexDirection: 'column'}}>
              <ListUsrek
                navigation={this.props.navigation}
                id_gardu_induk={this.state.id_gardu_induk}
              />
            </View>
          )}
        </LinearGradient>
        <FloatingButtonUsrek navigation={this.props.navigation} />

        {/* {this.state.visibleInput === true ? (
          <FloatingButtonUsrek navigation={this.props.navigation} />
        ) : null} */}
      </Container>
    );
  }
}
