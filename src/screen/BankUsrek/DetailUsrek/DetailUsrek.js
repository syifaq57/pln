/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Platform,
  Alert,
  ActivityIndicator,
  Image,
  TouchableOpacity,
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Button,
  Content,
  Card,
  Text,
} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';
import GlobalConfig from '../../../library/network/GlobalConfig';
import Modal from 'react-native-modal';

export default class DetailUsrek extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataCoba: 'tes',
      data: [],
      index: 0,
      routes: [
        {key: 'DetailAnomali', title: 'Detail Anomali '},
        {key: 'RencanaRealisasi', title: 'Rencana Realisasi'},
      ],
      images: [],
      isModalVisible: false,
      isModalVisible2: false,
    };
  }

  loadData() {
    this.setState({
      isLoading: true,
    });
    const {id_usrek} = this.props.navigation.state.params;
    this.setState({isLoading: true});
    var url = GlobalConfig.SERVERHOST + 'usrek/get_all_data_usrek';
    var formData = new FormData();
    formData.append('id_usrek', id_usrek);
    console.log('frmdta', formData);
    fetch(url, {
      method: 'POST',
      body: formData,
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.status === 'success') {
          this.setState(
            {
              data: response.data,
            },
            function() {
              console.log('Data all', this.state.data);
              this.setState({
                isLoading: false,
              });
            },
          );
        } else {
          this.setState({
            isLoading: false,
          });
          Alert('Gagal Load Data', [
            {
              text: 'Okay',
            },
          ]);
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        Alert.alert('Error', 'Check Your Internet Connection', [
          {
            text: 'Okay',
          },
        ]);
        console.log(error);
      });
  }

  componentDidMount() {
    this.loadData();
  }

  toggleModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
      // images:[{uri:key}]
    });
  };

  toggleModal2 = () => {
    this.setState({
      isModalVisible2: !this.state.isModalVisible2,
      // images:[{uri:key}]
    });
  };

  render() {
    return (
      <Container>
        <Header
          transparent
          style={{
            marginTop: Platform.OS === 'ios' ? 0 : 0,
            borderBottomWidth: 0,
            backgroundColor: colors.greenpln,
          }}>
          <View
            style={{
              flex: 3,
              justifyContent: 'center',
            }}>
            <Button
              onPress={() => this.props.navigation.goBack()}
              transparent
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <Icon
                name="md-arrow-round-back"
                size={23}
                style={{color: 'white'}}
              />
              <Text
                uppercase={false}
                style={{
                  flex: 1,
                  textAlignVertical: 'center',
                  fontWeight: 'bold',
                  fontSize: 20,
                  color: 'white',
                }}>
                Detail Usulan Kerja
              </Text>
            </Button>
          </View>
          <View style={{flex: 1}}/>
        </Header>
        <Content>
          {this.state.isLoading === true ? (
            <View style={{alignItems: 'center'}}>
              <ActivityIndicator />
            </View>
          ) : (
            <View style={{margin: 15}}>
            <View style={{marginLeft: 10}}>
                <Text style={{fontWeight: 'bold'}}>Detail</Text>
              </View>
              <Card
                style={{
                  borderRadius: 8,
                  flex: 1,
                  padding: 15,
                  borderColor: colors.greenpln,
                  borderWidth: 0,
                }}>
                <View style={{flex: 1, flexDirection: 'column'}}>
                  <Text style={{fontSize: 10}}>Tanggal Usulan</Text>
                  <Text style={{marginBottom: 10}}>
                    {this.state.data.tgl_usulan}
                  </Text>
                  <Text style={{fontSize: 10}}>Nomor Usulan</Text>
                  <Text style={{marginBottom: 10}}>
                    {this.state.data.no_usrek}
                  </Text>
                  <Text style={{fontSize: 10}}>Bidang</Text>
                  <Text style={{marginBottom: 10}}>
                    {this.state.data.nm_bidang}
                  </Text>
                  <Text style={{fontSize: 10}}>Gardu Induk</Text>
                  <Text style={{marginBottom: 10}}>
                    {this.state.data.nm_gardu_induk}
                  </Text>
                  <Text style={{fontSize: 10}}>BAY</Text>
                  <Text style={{marginBottom: 10}}>
                    {this.state.data.nm_bay}
                  </Text>
                  <Text style={{fontSize: 10}}>Jenis Fungsi Usulan</Text>
                  <Text style={{marginBottom: 10}}>
                    {this.state.data.jenis_fungsi_usulan}
                  </Text>
                  <Text style={{fontSize: 10}}>Judul Pekerjaan</Text>
                  <Text style={{marginBottom: 10}}>
                    {this.state.data.judul_pekerjaan}
                  </Text>
                  <Text style={{fontSize: 10}}>Jenis Pekerjaan</Text>
                  <Text style={{marginBottom: 10}}>
                    {this.state.data.nm_jenis_pekerjaan}
                  </Text>
                  <Text style={{fontSize: 10}}>Justifikasi Usulan</Text>
                  <Text style={{marginBottom: 10}}>
                    {this.state.data.justifikasi_usulan}
                  </Text>
                  <Text style={{fontSize: 10}}>
                    Uraian Detail Rencana Pekerjaan
                  </Text>
                  <Text style={{marginBottom: 10}}>
                    {this.state.data.uraian_detail_rencana}
                  </Text>
                  <Text style={{fontSize: 10}}>Prioritas</Text>
                  <Text style={{marginBottom: 10}}>
                    {this.state.data.prioritas_user}
                  </Text>
                  <Text style={{fontSize: 10}}>Perkiraan Biaya</Text>
                  <Text style={{marginBottom: 10}}>
                    {this.state.data.perkiraan_biaya}
                  </Text>
                </View>
              </Card>
              {/* <Card
                style={{
                  borderRadius: 8,
                  flex: 1,
                  flexDirection: 'row',
                  padding: 15,
                  borderColor: colors.greenpln,
                  borderWidth: 0,
                }}>
                <View style={{flex: 1, marginRight: 10}}>
                  <Text
                    style={{marginLeft: 5, fontSize: 10, fontWeight: 'bold'}}>
                    RAB
                  </Text>
                  <View style={{marginTop: 5}}>
                    <Button style={{backgroundColor: colors.greenpln}}>
                      <View style={{flex: 1}}>
                        <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
                          Lihat
                        </Text>
                      </View>
                    </Button>
                  </View>
                </View>
                <View style={{flex: 1, marginLeft: 10}}>
                  <Text
                    style={{marginLeft: 5, fontSize: 10, fontWeight: 'bold'}}>
                    DOKUMEN USULAN
                  </Text>
                  <View style={{marginTop: 5}}>
                    <Button style={{backgroundColor: colors.greenpln}}>
                      <View style={{flex: 1}}>
                        <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
                          Lihat
                        </Text>
                      </View>
                    </Button>
                  </View>
                </View>
              </Card> */}
              <View style={{marginLeft: 10}}>
                <Text style={{fontWeight: 'bold'}}>Dokumentasi</Text>
              </View>
              <Card
                style={{
                  borderRadius: 8,
                  flex: 1,
                  padding: 10,
                  paddingVertical: 20,
                }}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <View style={{flex: 2, justifyContent: 'center'}}>
                    <TouchableOpacity
                      onPress={() => this.toggleModal()}
                      style={{flex: 1}}>
                      <Image
                        style={{
                          alignSelf: 'center',
                          width: 100,
                          height: 60,
                          borderRadius: 3,
                          borderColor: 'black',
                          resizeMode: 'contain',
                        }}
                        source={{
                          uri:
                            GlobalConfig.base_url +
                            'assets/foto-anomali/' +
                            this.state.data.dokumentasi_satu,
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 2, justifyContent: 'center'}}>
                    <TouchableOpacity
                      onPress={() => this.toggleModal2()}
                      style={{flex: 1}}>
                      <Image
                        style={{
                          alignSelf: 'center',
                          width: 100,
                          height: 60,
                          borderRadius: 3,
                          borderColor: 'black',
                          resizeMode: 'contain',
                        }}
                        source={{
                          uri:
                            GlobalConfig.base_url +
                            'assets/foto-anomali/' +
                            this.state.data.dokumentasi_dua,
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </Card>
            </View>
          )}

          <Modal
            style={{
              justifyContent: 'center',
              padding: 10,
              paddingHorizontal: 0,
              marginHorizontal: 0,
            }}
            isVisible={this.state.isModalVisible}
            backdropOpacity={1}
            onBackdropPress={this.toggleModal}
            onBackButtonPress={this.toggleModal}
            animationIn={'slideInUp'}
            animationOut={'slideOutDown'}
            hideModalContentWhileAnimating={true}>
            <View>
              <Button
                style={{
                  width: 40,
                  height: 40,
                  margin: 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: 'red',
                }}
                onPress={() => this.toggleModal()}>
                <Icon2
                  onPress={() => this.toggleModal()}
                  name={'close'}
                  size={20}
                />
              </Button>
            </View>
            <View style={{flex: 1}}>
              <Image
                style={{
                  alignSelf: 'center',
                  width: '100%',
                  height: '100%',
                  resizeMode: 'contain',
                  borderRadius: 3,
                }}
                source={{
                  uri:
                    GlobalConfig.base_url +
                    'assets/foto-anomali/' +
                    this.state.data.dokumentasi_satu,
                }}
              />
            </View>
          </Modal>
          <Modal
            style={{
              justifyContent: 'center',
              padding: 10,
              paddingHorizontal: 0,
              marginHorizontal: 0,
            }}
            isVisible={this.state.isModalVisible2}
            backdropOpacity={1}
            onBackdropPress={this.toggleModal2}
            onBackButtonPress={this.toggleModal2}
            animationIn={'slideInUp'}
            animationOut={'slideOutDown'}
            hideModalContentWhileAnimating={true}>
            <View>
              <Button
                style={{
                  width: 40,
                  height: 40,
                  margin: 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: 'red',
                }}
                onPress={() => this.toggleModal2()}>
                <Icon2
                  onPress={() => this.toggleModal2()}
                  name={'close'}
                  size={20}
                />
              </Button>
            </View>
            <View style={{flex: 1}}>
              <Image
                style={{
                  alignSelf: 'center',
                  width: '100%',
                  height: '100%',
                  resizeMode: 'contain',
                  borderRadius: 3,
                }}
                source={{
                  uri:
                    GlobalConfig.base_url +
                    'assets/foto-anomali/' +
                    this.state.data.dokumentasi_dua,
                }}
              />
            </View>
          </Modal>
        </Content>
      </Container>
    );
  }
}
