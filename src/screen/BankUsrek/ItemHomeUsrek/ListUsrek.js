/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, ActivityIndicator, AsyncStorage} from 'react-native';
import {Input, Content} from 'native-base';
import colors from '../../../res/colors/index';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  UltimateListView,
  UltimateRefreshView,
} from 'react-native-ultimate-listview';
import ItemListUsrek from './ItemListUsrek';
import GlobalConfig from '../../../library/network/GlobalConfig';

export default class ListUsrek extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      limit: 10,
      searchText: '',
    };
  }

  onFetch = async (page = 0, startFetch, abortFetch) => {
    try {
      setTimeout(() => {
        console.log((page - 1) * 10);
        var url = GlobalConfig.SERVERHOST + 'usrek/get_all_data_usrek';
        var formData = new FormData();
        formData.append('start', (page - 1) * 10);
        formData.append('limit', this.state.limit);
        formData.append('keyword', this.state.searchText);
        formData.append('id_gardu_induk', this.props.id_gardu_induk);
        console.log('user tes', formData);
        fetch(url, {
          method: 'POST',
          body: formData,
        })
          .then(response => response.json())
          .then(responseData => {
            console.log('print', responseData);
            startFetch(responseData.data, 10);
            console.log('mlaku');
          })
          .catch(error => {
            console.log('eror 1', error);
            alert('Error Connection ' + error);
          })
          .done(() => {});
      }, 1000);
    } catch (err) {
      abortFetch();
      console.log('eror', err);
    }
  };

  _renderRowView = (item, index, separator) => {
    return (
      <View style={{width: '100%'}}>
        <ItemListUsrek
          id_usrek={item.id_usrek}
          navigation={this.props.navigation}
          no_usrek={item.no_usrek}
          judul_pekerjaan={item.judul_pekerjaan}
          perkiraan_biaya={item.perkiraan_biaya}
          prioritas_user={item.prioritas_user}
        />
      </View>
    );
  };

  onFilter() {
    this.setState({isLoading: true}, function() {
      this.setState({isLoading: false, visibleSort: false});
    });
  }
  loadSearch() {
    this.setState({isLoading: true}, function() {
      this.setState({isLoading: false});
    });
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      'didFocus',
      payload => {
        // AsyncStorage.getItem('SavedUsrek').then(value => {
        //   if (value === '1') {
        //     this.setState({isLoading: true}, function() {
        //       this.setState({isLoading: false});
        //     });
        //     console.log('reload');
        //     AsyncStorage.setItem('SavedUsrek', '0');
        //   }
        // });
        this.setState({isLoading: true}, function() {
          this.setState({isLoading: false});
        });
      },
    );
  }

  render() {
    return (
      <View style={{marginLeft: 0, marginRight: 0, height: '100%'}}>
        <View
          style={{
            paddingHorizontal: 15,
            paddingVertical: 10,
            flexDirection: 'row',
            marginTop: 5,
          }}>
          <View
            style={{
              flexDirection: 'row',
              flex: 1,
              height: 40,
              backgroundColor: colors.white,
              borderRadius: 8,
              marginRight: 10,
            }}>
            <Input
              style={{fontSize: 11, paddingLeft: 15, height: 40}}
              placeholder="Type something here"
              value={this.state.searchText}
              onSubmitEditing={() => this.loadSearch()}
              onEndEditing={() => console.log('canceled')}
              onBlur={() => console.log('canceled')}
              returnKeyType={'search'}
              onChangeText={text => this.setState({searchText: text})}
            />
            {this.state.keyboardShow && (
              <View style={{alignSelf: 'center'}}>
                <Icon
                  onPress={() => this.setState({searchText: ''})}
                  name="close"
                  style={{
                    fontSize: 15,
                    paddingLeft: 0,
                    alignSelf: 'center',
                    marginLeft: 5,
                    marginRight: 10,
                  }}
                />
              </View>
            )}
          </View>
          <View style={{alignSelf: 'center'}}>
            <Icon
              onPress={() => this.setState({visibleSort: true})}
              name="search"
              color={colors.greenpln}
              style={{
                fontSize: 30,
                paddingLeft: 0,
                alignSelf: 'center',
                marginLeft: 5,
                marginRight: 5,
              }}
            />
          </View>
        </View>
        {this.state.isLoading ? (
          <View>
            <ActivityIndicator />
          </View>
        ) : (
          <View style={{flex: 1}}>
            <UltimateListView
              ref={ref => (this._listView = ref)}
              onFetch={this.onFetch}
              headerView={this.renderHeaderView}
              item={this._renderRowView}
              refreshableTitlePull="Pull To Refresh"
              refreshableMode="basic" //basic | advanced
            />
          </View>
        )}
      </View>
    );
  }
}
