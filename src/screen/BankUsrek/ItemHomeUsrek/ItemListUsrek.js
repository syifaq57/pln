/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, TouchableOpacity, AsyncStorage} from 'react-native';
import {Card} from 'native-base';
import colors from '../../../res/colors/index';

export default class ItemListUsrek extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DataSource: [
        {
          tanggal: '7 Januari 2019',
          garduInduk: 'GI 150KV PETROKIMIA',
          prioritas: 'SANGAT URGENT',
          nomorusulan: '007/USREK/M05/III/2020',
        },
        {
          tanggal: '30 Januari 2020',
          garduInduk: 'GI 150KV SEGOROMADU',
          prioritas: 'SANGAT URGENT',
          nomorusulan: '007/USREK/M05/III/2020',
        },
        {
          tanggal: '4 Februari 2020',
          garduInduk: 'GISTET 500KV GRESIK',
          prioritas: 'BIASA',
          nomorusulan: '007/USREK/M05/III/2020',
        },
        {
          tanggal: '7 Januari 2019',
          garduInduk: 'GI 150KV PETROKIMIA',
          prioritas: 'SANGAT URGENT',
          nomorusulan: '007/USREK/M05/III/2020',
        },
        {
          tanggal: '30 Januari 2020',
          garduInduk: 'GI 150KV SEGOROMADU',
          prioritas: 'SANGAT URGENT',
          nomorusulan: '007/USREK/M05/III/2020',
        },
        {
          tanggal: '4 Februari 2020',
          garduInduk: 'GISTET 500KV GRESIK',
          prioritas: 'BIASA',
          nomorusulan: '007/USREK/M05/III/2020',
        },
      ],
    };
  }
  navigateToScreen(route) {
    this.props.navigation.navigate(route, {id_usrek: this.props.id_usrek});
  }

  render() {
    return (
      <View>
        <TouchableOpacity
          onPress={() => this.navigateToScreen('DetailUsrek')}>
          <View style={{marginLeft: 15, marginRight: 15}}>
            <Card
              style={{
                borderRadius: 8,
                flex: 1,
                padding: 15,
                borderColor: 'white',
                borderWidth: 0,
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginLeft: 0,
                  marginRight: 0,
                }}>
                <View style={{flex: 7}}>
                  <View style={{flex: 1, flexDirection: 'row', marginTop: 5}}>
                    <View style={{flex: 4}}>
                      <Text style={{fontSize: 10}}>No. Usulan</Text>
                      <Text style={{marginBottom: 10}}>
                        {this.props.no_usrek}
                      </Text>
                    </View>
                    <View style={{alignItems: 'flex-end', flex: 2.5}}>
                      {this.props.prioritas_user == 1 ? (
                        <Text
                          style={{
                            fontSize: 12,
                            fontWeight: 'bold',
                            color: colors.green01,
                            marginTop: 5,
                          }}>
                          Biasa
                        </Text>
                      ) : this.props.prioritas_user == 2 ? (
                        <Text
                          style={{
                            fontSize: 12,
                            fontWeight: 'bold',
                            color: colors.darkOrange,
                            marginTop: 5,
                          }}>
                          Sedang
                        </Text>
                      ) : (
                        <Text
                          style={{
                            fontSize: 12,
                            fontWeight: 'bold',
                            color: colors.red,
                            marginTop: 5,
                          }}>
                          Urgent
                        </Text>
                      )}
                    </View>
                  </View>
                  <View style={{flex: 1, flexDirection: 'column'}}>
                    <Text style={{fontSize: 10}}>Judul Pekerjaan</Text>
                    <Text style={{marginBottom: 10}}>
                      {this.props.judul_pekerjaan}
                    </Text>
                    <Text style={{fontSize: 10}}>Perkiraan Biaya</Text>
                    <Text style={{marginBottom: 10}}>
                      {this.props.perkiraan_biaya}
                    </Text>
                  </View>
                </View>
              </View>
            </Card>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
