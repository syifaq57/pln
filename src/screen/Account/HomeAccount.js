/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  Image,
  StatusBar,
  KeyboardAvoidingView,
  ScrollView,
  View,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Alert,
  BackHandler,
  Platform,
  DeviceEventEmitter,
  PushNotificationIOS,
  ImageBackground,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {
  Container,
  Header,
  Form,
  Item,
  Label,
  Input,
  Footer,
  Left,
  Right,
  Button,
  Body,
  Title,
  Card,
  CheckBox,
  Content,
} from 'native-base';
import Modal from 'react-native-modal';
import colors from '../../res/colors';

export default class HomeAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  render() {
    return (
      <Container style={{flex: 1, display: 'flex'}}>
        <ImageBackground
          source={require('../../res/images/BGdashboard.jpg')}
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: colors.lightBlack,
            paddingTop: 0,
          }}>
          <StatusBar translucent={true} barStyle="light-content" />
        </ImageBackground>
      </Container>
    );
  }
}
