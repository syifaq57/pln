/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  AsyncStorage,
  Linking,
} from 'react-native';
import {Card, Left, Right, Button} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';
import GlobalConfig from '../../../library/network/GlobalConfig';

export default class ItemListAset extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DataSource: [
        {
          tanggal: '7 Januari 2019',
          garduInduk: 'GI 150KV PLTU GRESIK',
          nomorBA: '036/BAOL/C03/III/2020',
          jenisPekerjaan: 'M02 - Pemeliharaan Alat Kerja',
        },
        {
          tanggal: '28 Agustus 2019',
          garduInduk: 'GI 150KV CERME',
          nomorBA: '037/BAOL/C03/III/2020',
          jenisPekerjaan: 'C01 - Penggantian Aksesoris MTU',
        },
        {
          tanggal: '6 Agustus 2019',
          garduInduk: 'GI 150KV ALTAPRIMA',
          nomorBA: '038/BAOL/C03/III/2020',
          jenisPekerjaan: 'C09 - Penggantian Material Proteksi',
        },
        {
          tanggal: '7 Januari 2019',
          garduInduk: 'GI 150KV PLTU GRESIK',
          nomorBA: '039/BAOL/C03/III/2020',
          jenisPekerjaan: 'M02 - Pemeliharaan Alat Kerja',
        },
        {
          tanggal: '28 Agustus 2019',
          garduInduk: 'GI 150KV CERME',
          nomorBA: '040/BAOL/C03/III/2020',
          jenisPekerjaan: 'C01 - Penggantian Aksesoris MTU',
        },
        {
          tanggal: '6 Agustus 2019',
          garduInduk: 'GI 150KV ALTAPRIMA',
          nomorBA: '041/BAOL/C03/III/2020',
          jenisPekerjaan: 'C09 - Penggantian Material Proteksi',
        },
      ],
    };
  }
  navigateToScreen(route) {
    this.props.navigation.navigate(route, {
      id_penggantian_aset: this.props.id_penggantian_aset,
    });
  }

  openDokBA() {
    var no_ba = this.props.no_baol
      .replace('/', '_')
      .replace('/', '_')
      .replace('/', '_')
      .replace('/', '_');
    var url = GlobalConfig.base_url + 'assets/dokumen-baol/' + no_ba + '.pdf';
    console.log('ba', url);
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        alert('Browser tidak ditemukan!');
        console.log("Don't know how to open URI: " + url);
      }
    });
  }

  render() {
    return (
      <View>
        <TouchableOpacity onPress={() => this.navigateToScreen('DetailAset')}>
          <View style={{marginLeft: 15, marginRight: 15}}>
            <Card
              style={{
                borderRadius: 8,
                flex: 1,
                padding: 15,
                borderColor: 'white',
                borderWidth: 0,
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginLeft: 0,
                  marginRight: 0,
                }}>
                <View style={{flex: 1}}>
                  <View style={{flexDirection: 'row'}}>
                    <View
                      style={{
                        flex: 2,
                        flexDirection: 'column',
                        marginTop: 5,
                      }}>
                      <Text style={{fontSize: 10}}>Approval Status</Text>
                      <Text style={{marginBottom: 10}}>
                        {this.props.nm_bidang}
                      </Text>
                    </View>
                    <View style={{alignItems: 'center', flex: 1}}>
                      <Text
                        style={{
                          fontSize: 12,
                          fontWeight: 'bold',
                          color: colors.green01,
                          marginTop: 5,
                        }}>
                        {this.props.tgl_pekerjaan}
                      </Text>
                    </View>
                  </View>

                  <View style={{flex: 1, flexDirection: 'column'}}>
                    <Text style={{fontSize: 10}}>Nomor BA</Text>
                    <Text style={{marginBottom: 10}}>{this.props.no_baol}</Text>
                    {/* {this.props.is_approve !== null ? (
                      <TouchableOpacity onPress={() => this.openDokBA()}>
                        <Text
                          numberOfLines={2}
                          style={{marginBottom: 10, color: 'blue'}}>
                          {this.props.no_baol}
                        </Text>
                      </TouchableOpacity>
                    ) : (
                      <Text numberOfLines={2} style={{marginBottom: 10}}>
                        {this.props.no_baol}
                      </Text>
                    )} */}
                    <Text style={{fontSize: 10}}>Jenis jenis Pekerjaan</Text>
                    <Text style={{marginBottom: 10}}>
                      {this.props.nm_jenis_pekerjaan}
                    </Text>
                  </View>
                </View>

                {/* <Text
                    style={{
                      alignSelf: 'center',
                      color: colors.greenDefault,
                      fontWeight: 'normal',
                    }}>
                    {data.ultg}
                  </Text> */}
              </View>
            </Card>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
