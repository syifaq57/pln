/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Platform,
  Dimensions,
  Alert,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Button,
  Content,
  Card,
  Text,
  Textarea,
} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';
import styles from '../../../res/styles/Form';
// import ItemDetail from './itemDetail';
import RadioButton from '../../../library/component/RadioButtonHorizontal';
import GlobalConfig from '../../../library/network/GlobalConfig';
import {TabBar, TabView, SceneMap} from 'react-native-tab-view';
import Modal from 'react-native-modal';

export default class DetailAset extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      index: 0,
      routes: [
        {key: 'view1', title: 'Data Penggatian Aset'},
        {key: 'view2', title: 'Data Peralatan'},
        {key: 'view3', title: 'Data Nameplate Phasa'},
        {key: 'view4', title: 'Dokumentasi'},
      ],
      isModalApprove: false,
      visibleapprove: false,
      isLoading: true,
      is_approve: '',
      komentar: '',
    };
  }

  loadData() {
    const {id_penggantian_aset} = this.props.navigation.state.params;
    this.setState({isLoading: true});
    var url =
      GlobalConfig.SERVERHOST +
      'penggantian_aset/get_all_data_penggantian_aset';
    var formData = new FormData();
    formData.append('id_penggantian_aset', id_penggantian_aset);
    console.log('frmdta', formData);
    fetch(url, {
      method: 'POST',
      body: formData,
    })
      .then(response => response.json())
      .then(response => {
        console.log('cc', response);
        if (response.status === 'success') {
          this.setState(
            {
              data: response.data,
            },
            function() {
              const dokumen = this.state.data[0];
              const DataUser = this.state.DataUser;
              const jabatan = this.state.DataUser.detail_jabatan[0];
              console.log('userrr 3', dokumen.is_approve)
              if (
                dokumen.is_approve && dokumen.is_approve !== 'Y'
              ) {
                if (DataUser.user.is_admin === 'Y') {
                  this.setState(
                    {
                      visibleapprove: true,
                    },
                    function() {
                      this.setState({
                        isLoading: false,
                      });
                    },
                  );
                }
                else if(DataUser.jabatan.nm_jenis_pegawai !== 'SPV' || DataUser.jabatan.nm_jenis_pegawai !== 'PJ' ){
                  if(dokumen.id_bidang === 1 && dokumen.id_gardu_induk === jabatan.id_gardu_induk){
                    this.setState(
                      {
                        visibleapprove: true,
                      },
                      function() {
                        this.setState({
                          isLoading: false,
                        });
                      },
                    );
                  } else if ((dokumen.id_bidang === 2 || dokumen.id_bidang === 5) && jabatan.id_jenis_pegawai === 2 ) {
                    if(dokumen.id_ultg === jabatan.id_ultg){
                      this.setState(
                        {
                          visibleapprove: true,
                        },
                        function() {
                          this.setState({
                            isLoading: false,
                          });
                        },
                      );
                    }
                  } else if ((dokumen.id_bidang === 3 || dokumen.id_bidang === 7) && jabatan.id_jenis_pegawai === 4 ) {
                    if(dokumen.id_ultg === jabatan.id_ultg){
                      this.setState(
                        {
                          visibleapprove: true,
                        },
                        function() {
                          this.setState({
                            isLoading: false,
                          });
                        },
                      );
                    }
                  } else if ((dokumen.id_bidang === 4 || dokumen.id_bidang === 6) && jabatan.id_jenis_pegawai === 3 ) {
                    if(dokumen.id_ultg === jabatan.id_ultg){
                      this.setState(
                        {
                          visibleapprove: true,
                        },
                        function() {
                          this.setState({
                            isLoading: false,
                          });
                        },
                      );
                    }
                  } else {
                    if(dokumen.id_bidang === DataUser.id_bidang) {
                      this.setState(
                        {
                          visibleapprove: true,
                        },
                        function() {
                          this.setState({
                            isLoading: false,
                          });
                        },
                      );
                    }
                  }
                  
                }
              } else {
                this.setState(
                  {
                    visibleapprove: false,
                  },
                  function() {
                    this.setState({
                      isLoading: false,
                    });
                  },
                );
              }
            },
          );
        } else {
          this.setState({
            isLoading: false,
          });
          Alert('Gagal Load Data', [
            {
              text: 'Okay',
            },
          ]);
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        Alert.alert('Error', 'Check Your Internet Connection', [
          {
            text: 'Okay',
          },
        ]);
        console.log(error);
      });
  }

  componentDidMount() {
    AsyncStorage.getItem('DataUser').then(value =>
      this.setState(
        {
          DataUser: JSON.parse(value),
        },
        function() {
          console.log('idpgw', this.state.DataUser.user.nip);
          this.loadData();
        },
      ),
    );
  }

  onApprove() {
    this.setState({
      isLoadingSave: true,
    });
    const {id_penggantian_aset} = this.props.navigation.state.params;
    if (this.state.is_approve === '' || this.state.komentar === '') {
      alert('Form belum lengkap');
      this.setState({
        isLoadingSave: false,
      });
    } else {
      var url = GlobalConfig.SERVERHOST + 'penggantian_aset/approve';
      var formData = new FormData();

      formData.append('id_penggantian_aset', id_penggantian_aset);
      formData.append('is_approve', this.state.is_approve);
      formData.append('komentar', this.state.komentar);

      console.log('all FD', formData);
      fetch(url, {
        method: 'POST',
        body: formData,
      })
        .then(response => response.json())
        .then(response => {
          console.log('piye', response);
          if (response.status === 'success') {
            this.setState(
              {
                isLoadingSave: false,
                isModalApprove: false,
              },
              function() {
                Alert.alert('Berhasil Menyimpan Data', response.message, [
                  {
                    text: 'Okay',
                  },
                ]);
                AsyncStorage.setItem('SavedBA', '1');
                this.props.navigation.goBack();
              },
            );
          } else {
            this.setState({
              isLoadingSave: false,
            });
            Alert.alert('Cannot Save Data2', 'Check Your Internet Connection', [
              {
                text: 'Okay',
              },
            ]);
          }
        })
        .catch(error => {
          this.setState({
            isLoadingSave: false,
          });
          Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [
            {
              text: 'Okay',
            },
          ]);
          console.log(error);
        });
    }
  }

  modalApprove = () => {
    this.setState({
      isModalApprove: !this.state.isModalApprove,
      // images:[{uri:key}]
    });
  };

  toggleModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
      // images:[{uri:key}]
    });
  };

  toggleModal2 = () => {
    this.setState({
      isModalVisible2: !this.state.isModalVisible2,
      // images:[{uri:key}]
    });
  };

  render() {
    const view1 = () => (
      <Content>
        <View style={{margin: 15}}>
          <Card
            style={{
              borderRadius: 8,
              flex: 1,
              padding: 15,
            }}>
            {this.state.isLoading === true ? (
              <View style={{flex: 1, alignItems: 'center'}}>
                <ActivityIndicator />
              </View>
            ) : (
              <View style={{flex: 1, flexDirection: 'column'}}>
                <Text style={{fontSize: 10}}>Pegawai</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].nm_pegawai}
                </Text>
                <Text style={{fontSize: 10}}>Nomor BA Penggantian Aset</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].no_penggantian_aset}
                </Text>
                <Text style={{fontSize: 10}}>Tanggal Pekerjaan</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].tgl_pekerjaan}
                </Text>
                <Text style={{fontSize: 10}}>Wilayah</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].wilayah}
                </Text>
                <Text style={{fontSize: 10}}>Bidang</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].nm_bidang}
                </Text>
                <Text style={{fontSize: 10}}>Gardu Induk</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].nm_gardu_induk}
                </Text>
                <Text style={{fontSize: 10}}>BAY</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].nm_bay}
                </Text>
              </View>
            )}
          </Card>
        </View>
        <View style={{height: 50}} />
      </Content>
    );
    const view2 = () => (
      <Content>
        <View style={{margin: 15}}>
          <Card
            style={{
              borderRadius: 8,
              flex: 1,
              padding: 15,
            }}>
            {this.state.isLoading === true ? (
              <View style={{flex: 1, alignItems: 'center'}}>
                <ActivityIndicator />
              </View>
            ) : (
              <View style={{flex: 1, flexDirection: 'column'}}>
                <Text style={{fontSize: 10}}>Peralatan Yang Diganti</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].peralatan_diganti}
                </Text>
                <Text style={{fontSize: 10}}>Alasan Penggantian</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].alasan_diganti}
                </Text>
                <Text style={{fontSize: 10}}>Phasa</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].phasa}
                </Text>
              </View>
            )}
          </Card>
        </View>
        <View style={{height: 50}} />
      </Content>
    );

    const view3 = () => (
      <Content>
        {this.state.isLoading === true ? (
          <View style={{flex: 1, alignItems: 'center'}}>
            <ActivityIndicator />
          </View>
        ) : (
          <View style={{margin: 15}}>
            <Card
              style={{
                borderRadius: 8,
                flex: 1,
                padding: 15,
              }}>
              <View style={{flex: 1, flexDirection: 'column'}}>
                <Text style={{fontSize: 10}}>Merk Phasa Lama</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].detail_phasa[0].merk_lama}
                </Text>
                <Text style={{fontSize: 10}}>Relokasi MTU Lama</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].detail_phasa[0].relokasi_mtu_lama}
                </Text>
                <Text style={{fontSize: 10}}>Type Phasa Lama</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].detail_phasa[0].type_lama}
                </Text>
                <Text style={{fontSize: 10}}>No Seri Phasa Lama</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].detail_phasa[0].no_seri_lama}
                </Text>
                <Text style={{fontSize: 10}}>Tahun Pembuatan Phasa Lama</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].detail_phasa[0].tahun_pembuatan_lama}
                </Text>
                <Text style={{fontSize: 10}}>Nameplate Phasa Lama</Text>
                <View style={{flex: 2, justifyContent: 'center'}}>
                  <TouchableOpacity
                    onPress={() => this.toggleModal()}
                    style={{flex: 1}}>
                    <Image
                      style={{
                        width: 100,
                        height: 60,
                        borderRadius: 3,
                        borderColor: 'black',
                        resizeMode: 'contain',
                      }}
                      source={{
                        uri:
                          GlobalConfig.base_url +
                          'assets/foto-nameplate/' +
                          this.state.data[0].detail_phasa[0]
                            .foto_nameplate_lama,
                      }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </Card>
            <Card
              style={{
                borderRadius: 8,
                flex: 1,
                padding: 15,
              }}>
              <View style={{flex: 1, flexDirection: 'column'}}>
                <Text style={{fontSize: 10}}>Merk Phasa Baru</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].detail_phasa[0].merk_baru}
                </Text>
                <Text style={{fontSize: 10}}>Asal MTU Baru</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].detail_phasa[0].asal_mtu_baru}
                </Text>
                <Text style={{fontSize: 10}}>Type Phasa Baru</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].detail_phasa[0].type_baru}
                </Text>
                <Text style={{fontSize: 10}}>No Seri Phasa Baru</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].detail_phasa[0].no_seri_baru}
                </Text>
                <Text style={{fontSize: 10}}>Tahun Pembuatan Phasa Baru</Text>
                <Text style={{marginBottom: 10}}>
                  {this.state.data[0].detail_phasa[0].tahun_pembuatan_baru}
                </Text>
                <Text style={{fontSize: 10}}>Nameplate Phasa Baru</Text>
                <View style={{flex: 2, justifyContent: 'center'}}>
                  <TouchableOpacity
                    onPress={() => this.toggleModal()}
                    style={{flex: 1}}>
                    <Image
                      style={{
                        width: 100,
                        height: 60,
                        borderRadius: 3,
                        borderColor: 'black',
                        resizeMode: 'contain',
                      }}
                      source={{
                        uri:
                          GlobalConfig.base_url +
                          'assets/foto-nameplate/' +
                          this.state.data[0].detail_phasa[0]
                            .foto_nameplate_baru,
                      }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </Card>
            <Modal
              style={{
                justifyContent: 'center',
                padding: 10,
                paddingHorizontal: 0,
                marginHorizontal: 0,
              }}
              isVisible={this.state.isModalVisible}
              backdropOpacity={1}
              onBackdropPress={this.toggleModal}
              onBackButtonPress={this.toggleModal}
              animationIn={'slideInUp'}
              animationOut={'slideOutDown'}
              hideModalContentWhileAnimating={true}>
              <View>
                <Button
                  style={{
                    width: 40,
                    height: 40,
                    margin: 20,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: 'red',
                  }}
                  onPress={() => this.toggleModal()}>
                  <Icon2
                    onPress={() => this.toggleModal()}
                    name={'close'}
                    size={20}
                  />
                </Button>
              </View>
              <View style={{flex: 1}}>
                <Image
                  style={{
                    alignSelf: 'center',
                    width: '100%',
                    height: '100%',
                    resizeMode: 'contain',
                    borderRadius: 3,
                  }}
                  source={{
                    uri:
                      GlobalConfig.base_url +
                      'assets/foto-nameplate/' +
                      this.state.data[0].detail_phasa[0].foto_nameplate_lama,
                  }}
                />
              </View>
            </Modal>
          </View>
        )}

        <View style={{height: 50}} />
      </Content>
    );

    const view4 = () => (
      <Content>
        {this.state.isLoading === true ? (
          <View style={{flex: 1, alignItems: 'center'}}>
            <ActivityIndicator />
          </View>
        ) : (
          <View style={{margin: 15}}>
            <View style={{marginLeft: 10}}>
              <Text style={{fontWeight: 'bold'}}>Dokumentasi Pekerjaan</Text>
            </View>
            <Card
              style={{
                marginHorizontal: 10,
                borderRadius: 8,
                flex: 1,
                padding: 10,
                paddingVertical: 20,
              }}>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 2, justifyContent: 'center'}}>
                  <TouchableOpacity
                    onPress={() => this.toggleModal()}
                    style={{flex: 1}}>
                    <Image
                      style={{
                        alignSelf: 'center',
                        width: 100,
                        height: 60,
                        borderRadius: 3,
                        borderColor: 'black',
                        resizeMode: 'contain',
                      }}
                      source={{
                        uri:
                          GlobalConfig.base_url +
                          'assets/foto-penggantian-aset/' +
                          this.state.data[0].dokumentasi_pekerjaan_satu,
                      }}
                    />
                  </TouchableOpacity>
                </View>
                <View style={{flex: 2, justifyContent: 'center'}}>
                  <TouchableOpacity
                    onPress={() => this.toggleModal2()}
                    style={{flex: 1}}>
                    <Image
                      style={{
                        alignSelf: 'center',
                        width: 100,
                        height: 60,
                        borderRadius: 3,
                        borderColor: 'black',
                        resizeMode: 'contain',
                      }}
                      source={{
                        uri:
                          GlobalConfig.base_url +
                          'assets/foto-penggantian-aset/' +
                          this.state.data[0].dokumentasi_pekerjaan_dua,
                      }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </Card>
            <Modal
              style={{
                justifyContent: 'center',
                padding: 10,
                paddingHorizontal: 0,
                marginHorizontal: 0,
              }}
              isVisible={this.state.isModalVisible}
              backdropOpacity={1}
              onBackdropPress={this.toggleModal}
              onBackButtonPress={this.toggleModal}
              animationIn={'slideInUp'}
              animationOut={'slideOutDown'}
              hideModalContentWhileAnimating={true}>
              <View>
                <Button
                  style={{
                    width: 40,
                    height: 40,
                    margin: 20,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: 'red',
                  }}
                  onPress={() => this.toggleModal()}>
                  <Icon2
                    onPress={() => this.toggleModal()}
                    name={'close'}
                    size={20}
                  />
                </Button>
              </View>
              <View style={{flex: 1}}>
                <Image
                  style={{
                    alignSelf: 'center',
                    width: '100%',
                    height: '100%',
                    resizeMode: 'contain',
                    borderRadius: 3,
                  }}
                  source={{
                    uri:
                      GlobalConfig.base_url +
                      'assets/foto-nameplate/' +
                      this.state.data[0].detail_phasa[0].foto_nameplate_lama,
                  }}
                />
              </View>
            </Modal>
            <Modal
              style={{
                justifyContent: 'center',
                padding: 10,
                paddingHorizontal: 0,
                marginHorizontal: 0,
              }}
              isVisible={this.state.isModalVisible2}
              backdropOpacity={1}
              onBackdropPress={this.toggleModal2}
              onBackButtonPress={this.toggleModal2}
              animationIn={'slideInUp'}
              animationOut={'slideOutDown'}
              hideModalContentWhileAnimating={true}>
              <View>
                <Button
                  style={{
                    width: 40,
                    height: 40,
                    margin: 20,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: 'red',
                  }}
                  onPress={() => this.toggleModal2()}>
                  <Icon2
                    onPress={() => this.toggleModal2()}
                    name={'close'}
                    size={20}
                  />
                </Button>
              </View>
              <View style={{flex: 1}}>
                <Image
                  style={{
                    alignSelf: 'center',
                    width: '100%',
                    height: '100%',
                    resizeMode: 'contain',
                    borderRadius: 3,
                  }}
                  source={{
                    uri:
                      GlobalConfig.base_url +
                      'assets/foto-anomali/' +
                      this.state.data.dokumen_anomali_dua,
                  }}
                />
              </View>
            </Modal>
            <View style={{height: 50}} />
          </View>
        )}
      </Content>
    );

    return (
      <Container>
        <Header
          transparent
          style={{
            marginTop: Platform.OS === 'ios' ? 0 : 0,
            borderBottomWidth: 0,
            backgroundColor: colors.greenpln,
          }}>
          <View
            style={{
              flex: 2,
              justifyContent: 'center',
            }}>
            <Button
              onPress={() => this.props.navigation.goBack()}
              transparent
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <Icon
                name="md-arrow-round-back"
                size={23}
                style={{color: 'white'}}
              />
              <Text
                uppercase={false}
                style={{
                  flex: 1,
                  textAlignVertical: 'center',
                  fontWeight: 'bold',
                  fontSize: 20,
                  color: 'white',
                }}>
                Detail Berita Acara
              </Text>
            </Button>
          </View>
          <View style={{flex: 1}} />
        </Header>
        {this.state.isLoading === true ? (
          <View />
        ) : (
          <View>
            {this.state.visibleapprove === false ? (
              <View />
            ) : (
              <Button
                onPress={() => this.modalApprove()}
                style={{
                  borderRadius: 0,
                  backgroundColor: colors.green01,
                }}>
                <View style={{flex: 1}}>
                  <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
                    APPROVE
                  </Text>
                </View>
              </Button>
            )}
          </View>
        )}
        <Modal
          animationIn={'slideInDown'}
          isVisible={this.state.isModalApprove}
          onBackdropPress={this.modalApprove}
          backdropOpacity={0.2}
          animationOut={'slideOutUp'}
          animationInTiming={1000}>
          <View
            style={{backgroundColor: 'white', padding: 15, borderRadius: 10}}>
            <View style={{marginTop: 10}}>
              <Text style={{fontSize: 10}}>Status Penyelesaian</Text>
            </View>
            <View style={{marginTop: 5}}>
              <RadioButton
                option={YesNo}
                selected={this.state.thermovisi_peralatan_mtu}
                displayData="NAMA_LIST"
                onPressMethod={data =>
                  this.setState({
                    is_approve: data.is_approve,
                    NAMA_LIST: data.NAMA_LIST,
                  })
                }
              />
            </View>
            <View style={{marginTop: 10}}>
              <Text style={{fontSize: 10}}>Komentar</Text>
            </View>
            <View>
              <Textarea
                style={styles.textArea}
                rowSpan={3}
                bordered
                placeholderTextColor={colors.gray02}
                value={this.state.komentar}
                placeholder="Isi Komentar..."
                onChangeText={text => this.setState({komentar: text})}
              />
            </View>
            <View style={{marginVertical: 10}}>
              {this.state.isLoadingSave === true ? (
                <View style={{flex: 1, alignItems: 'center'}}>
                  <ActivityIndicator />
                </View>
              ) : (
                <Button
                  onPress={() => this.onApprove()}
                  style={{
                    marginLeft: 5,
                    backgroundColor: colors.green01,
                  }}>
                  <View style={{flex: 1}}>
                    <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
                      Simpan
                    </Text>
                  </View>
                </Button>
              )}
            </View>
            <View />
          </View>
        </Modal>

        <Content>
          <TabView
            navigationState={this.state}
            renderScene={SceneMap({
              view1: view1,
              view2: view2,
              view3: view3,
              view4: view4,
            })}
            onIndexChange={index => this.setState({index})}
            initialLayout={{width: Dimensions.get('window').width}}
            renderTabBar={props => (
              <TabBar
                scrollEnabled={true}
                {...props}
                indicatorStyle={{backgroundColor: 'white'}}
                tabStyle={{alignItems: 'flex-start', width: 'auto'}}
                style={{backgroundColor: colors.greenpln}}
                labelStyle={{fontWeight: 'bold', fontSize: 10}}
              />
            )}
          />
        </Content>
      </Container>
    );
  }
}

const YesNo = [
  {NAMA_LIST: 'Yes', is_approve: 'Y'},
  {NAMA_LIST: 'No', is_approve: 'N'},
];
