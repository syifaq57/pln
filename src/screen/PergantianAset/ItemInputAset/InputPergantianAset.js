/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Image,
  View,
  TextInput,
  Platform,
  AsyncStorage,
  ActivityIndicator,
  Alert,
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Button,
  Content,
  CardItem,
  Text,
  Textarea,
} from 'native-base';
import DatePicker from 'react-native-datepicker';
import Icon from 'react-native-vector-icons/Ionicons';
import SearchableDropdown from '../../../library/component/SearchableDropdown';
import RadioButton from '../../../library/component/CustomRadioButton';
import styles from '../../../res/styles/Form';
import colors from '../../../res/colors/index';
import StepIndicator from 'react-native-step-indicator';
import ImagePicker from 'react-native-image-crop-picker';
import Modal from 'react-native-modal';
import GlobalConfig from '../../../library/network/GlobalConfig';

export default class InputPergantianAset extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading1: true,
      isLoading2: false,
      isLoading3: false,
      isLoading4: false,
      isLoadingNoLKS: false,
      currentPosition: 0,

      isLoadingBay: false,
      enabledBay: true,

      isModalVisible: false,
      isModalVisible2: false,

      isModalBaru: false,
      isModalLama: false,

      isOpenImage: false,
      isOpenImage2: false,

      isNameplate1: false,
      isNameplate2: false,

      foto_nameplate_baru: [],
      foto_nameplate_lama: [],

      list_image: [],
      list_image2: [],

      tgl_pekerjaan: '',
      id_jenis_pekerjaan: '',
      id_bidang: '',
      wilayah: '',
      id_gardu_induk: '',
      id_bay: '',
      //2
      peralatan_diganti: '',
      alasan_diganti: '',
      phasa: '',
      //3

      merk_lama: '',
      no_seri_lama: '',
      relokasi_mtu_lama: '',
      tahun_pembuatan_lama: '',
      type_lama: '',
      merk_baru: '',
      no_seri_baru: '',
      asal_mtu_baru: '',
      tahun_pembuatan_baru: '',
      type_baru: '',

      //4
      penjelasan_dokumentasi_satu: '',
      penjelasan_dokumentasi_dua: '',
    };
  }

  onKirimData() {
    this.setState({
      isLoadingSave: true,
    });
    if (
      this.state.list_image.length === 0 ||
      this.state.penjelasan_dokumentasi_satu === ''
    ) {
      alert('Form belum lengkap');
      this.setState({
        isLoadingSave: false,
      });
    } else {
      var url = GlobalConfig.SERVERHOST + 'penggantian_aset/add_data_penggantian_aset';
      var formData = new FormData();
      formData.append('nip', this.state.DataUser.user.nip);
      formData.append('tgl_pekerjaan', this.state.tgl_pekerjaan);
      formData.append('id_jenis_pekerjaan', this.state.id_jenis_pekerjaan);
      formData.append('wilayah', this.state.wilayah);
      formData.append('id_bidang', this.state.id_bidang);
      formData.append('id_gardu_induk', this.state.id_gardu_induk);
      formData.append('id_bay', this.state.id_bay);

      formData.append('peralatan_diganti', this.state.peralatan_diganti);
      formData.append('alasan_diganti', this.state.alasan_diganti);
      formData.append('phasa', this.state.phasa);

      formData.append('merk_lama', this.state.merk_lama);
      formData.append('no_seri_lama', this.state.no_seri_lama);
      formData.append('relokasi_mtu_lama', this.state.relokasi_mtu_lama);
      formData.append('tahun_pembuatan_lama', this.state.tahun_pembuatan_lama);
      formData.append('type_lama', this.state.type_lama);
      formData.append(
        'foto_nameplate_lama',
        {
          uri: this.state.foto_nameplate_lama[0].uri,
          name: this.state.foto_nameplate_lama[0].name + '.jpg',
          type: 'image/jpg',
        },
        'file',
      );

      formData.append('merk_baru', this.state.merk_baru);
      formData.append('no_seri_baru', this.state.no_seri_baru);
      formData.append('asal_mtu_baru', this.state.asal_mtu_baru);
      formData.append('tahun_pembuatan_baru', this.state.tahun_pembuatan_baru);
      formData.append('type_baru', this.state.type_baru);
      formData.append(
        'foto_nameplate_baru',
        {
          uri: this.state.foto_nameplate_baru[0].uri,
          name: this.state.foto_nameplate_baru[0].name + '.jpg',
          type: 'image/jpg',
        },
        'file',
      );

      if (this.state.list_image.length !== 0) {
        formData.append(
          'dokumentasi_pekerjaan_satu',
          {
            uri: this.state.list_image[0].uri,
            name: this.state.list_image[0].name + '.jpg',
            type: 'image/jpg',
          },
          'file',
        );
        formData.append(
          'penjelasan_dokumentasi_satu',
          this.state.penjelasan_dokumentasi_satu,
        );
      }

      if (this.state.list_image2.length !== 0) {
        formData.append(
          'dokumentasi_pekerjaan_dua',
          {
            uri: this.state.list_image2[0].uri,
            name: this.state.list_image2[0].name + '.jpg',
            type: 'image/jpg',
          },
          'file',
        );
        formData.append(
          'penjelasan_dokumentasi_dua',
          this.state.penjelasan_dokumentasi_dua,
        );
      }

      console.log('all FD', formData);
      fetch(url, {
        method: 'POST',
        body: formData,
      })
        .then(response => response.json())
        .then(response => {
          console.log('piye', response);
          if (response.status === 'success') {
            this.setState(
              {
                isLoadingSave: false,
              },
              function() {
                Alert.alert('Berhasil Menyimpan Data', response.message, [
                  {
                    text: 'Okay',
                  },
                ]);
                AsyncStorage.setItem('SavedBA', '1');
                AsyncStorage.setItem('SavedRencana', '1');
                this.props.navigation.goBack();
              },
            );
          } else {
            this.setState({
              isLoadingSave: false,
            });
            Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [
              {
                text: 'Okay',
              },
            ]);
          }
        })
        .catch(error => {
          this.setState({
            isLoadingSave: false,
          });
          Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [
            {
              text: 'Okay',
            },
          ]);
          console.log(error);
        });
    }
  }

  toggleModal = () => {
    this.setState({isModalVisible: !this.state.isModalVisible});
  };
  toggleModal2 = () => {
    this.setState({isModalVisible2: !this.state.isModalVisible2});
  };

  toggleLama = () => {
    this.setState({isModalLama: !this.state.isModalLama});
  };
  toggleBaru = () => {
    this.setState({isModalBaru: !this.state.isModalBaru});
  };

  pickNameplateBaru(cropping, mediaType = 'photo') {
    ImagePicker.openPicker({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalBaru: false,
            foto_nameplate_baru: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
            images: null,
          },
          function() {
            console.log('mlaaku', this.state.foto_nameplate_baru[0].uri);
            this.setState({
              isNameplate2: true,
            });
          },
        );
      })
      .catch(e => {
        console.log(e);
        // Alert.alert(e.message ? e.message : e);
      });
  }

  pickNameplateBaruWithCamera(cropping, mediaType = 'photo') {
    ImagePicker.openCamera({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalBaru: false,
            foto_nameplate_baru: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
            images: null,
          },
          function() {
            console.log('mlaaku', this.state.foto_nameplate_baru);
            this.setState({
              isNameplate2: true,
            });
          },
        );
      })
      .catch(e => alert(e));
    this.toggleModal;
  }

  pickNameplateLama(cropping, mediaType = 'photo') {
    ImagePicker.openPicker({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalLama: false,
            foto_nameplate_lama: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
            images: null,
          },
          function() {
            console.log('mlaaku', this.state.foto_nameplate_lama[0].uri);
            this.setState({
              isNameplate1: true,
            });
          },
        );
      })
      .catch(e => {
        console.log(e);
        // Alert.alert(e.message ? e.message : e);
      });
  }

  pickNameplateLamaWithCamera(cropping, mediaType = 'photo') {
    ImagePicker.openCamera({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalLama: false,
            foto_nameplate_lama: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
            images: null,
          },
          function() {
            console.log('mlaaku', this.state.foto_nameplate_lama);
            this.setState({
              isNameplate1: true,
            });
          },
        );
      })
      .catch(e => alert(e));
    this.toggleModal;
  }

  pickSingle(cropping, mediaType = 'photo') {
    ImagePicker.openPicker({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible: false,
            list_image: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
            images: null,
          },
          function() {
            console.log('mlaaku', this.state.list_image[0].uri);
            this.setState({
              isOpenImage: true,
            });
          },
        );
      })
      .catch(e => {
        console.log(e);
        // Alert.alert(e.message ? e.message : e);
      });
  }

  pickSingleWithCamera(cropping, mediaType = 'photo') {
    ImagePicker.openCamera({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible: false,
            list_image: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
            images: null,
          },
          function() {
            console.log('mlaaku', this.state.list_image);
            this.setState({
              isOpenImage: true,
            });
          },
        );
      })
      .catch(e => alert(e));
    this.toggleModal;
  }

  pickSingle2(cropping, mediaType = 'photo') {
    ImagePicker.openPicker({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible2: false,
            list_image2: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
            images: null,
          },
          function() {
            console.log('mlaaku', this.state.list_image2);
            this.setState({
              isOpenImage2: true,
            });
          },
        );
      })
      .catch(e => {
        console.log(e);
        // Alert.alert(e.message ? e.message : e);
      });
  }

  pickSingleWithCamera2(cropping, mediaType = 'photo') {
    ImagePicker.openCamera({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible2: false,
            list_image2: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
            images: null,
            uriPicked: this.state.list_image.map(data => data.uri),
            typePicked: this.state.list_image.map(data => data.mime),
          },
          function() {
            console.log('mlaaku', this.state.list_image);
            this.setState({
              isOpenImage2: true,
            });
          },
        );
      })
      .catch(e => alert(e));
    this.toggleModal;
  }

  componentDidMount() {
    AsyncStorage.getItem('DataUser').then(value =>
      this.setState(
        {
          DataUser: JSON.parse(value),
        },
        function() {
          console.log('user aa', this.state.DataUser.user.id_pegawai);
        },
      ),
    );
    AsyncStorage.getItem('id_lkso').then(value =>
      this.setState(
        {
          id_lkso: value,
        },
        function() {
          console.log('idlkso 2', this.state.id_lkso);
          AsyncStorage.getItem('no_lkso').then(value2 =>
            this.setState(
              {
                no_lkso: value2,
              },
              function() {
                this.setState({
                  isLoading1: false,
                });
              },
            ),
          );
        },
      ),
    );
  }

  movePage() {
    if (this.state.currentPosition === 0) {
      console.log('masuk');
      if (
        this.state.nip === '' ||
        this.state.tgl_pekerjaan === '' ||
        this.state.id_jenis_pekerjaan === '' ||
        this.state.wilayah === '' ||
        this.state.id_bidang === '' ||
        this.state.id_gardu_induk === '' ||
        this.state.id_bay === ''
      ) {
        Alert.alert('Lengkapi Form!', 'Form masih ada yang kosong.', [
          {
            text: 'Okay',
          },
        ]);
      } else {
        console.log('gagal');
        this.setState({
          isLoading2: false,
          currentPosition: this.state.currentPosition + 1,
        });
      }
    } else if (this.state.currentPosition === 1) {
      this.setState({
        isLoading3: true,
      });
      // console.log('masuk');
      if (
        this.state.peralatan_diganti === '' ||
        this.state.alasan_diganti === '' ||
        this.state.phasa === ''
      ) {
        Alert.alert('Lengkapi Form!', 'Form masih ada yang kosong.', [
          {
            text: 'Okay',
          },
        ]);
      } else {
        console.log('gagal');
        this.setState(
          {
            currentPosition: this.state.currentPosition + 1,
          },
          function() {
            this.setState({
              isLoading3: false,
            });
          },
        );
      }
    } else if (this.state.currentPosition === 2) {
      this.setState({
        isLoading4: true,
      });
      // console.log('masuk');
      if (
        this.state.merk_lama === '' ||
        this.state.no_seri_lama === '' ||
        this.state.relokasi_mtu_lama === '' ||
        this.state.tahun_pembuatan_lama === '' ||
        this.state.type_lama === '' ||
        this.state.merk_baru === '' ||
        this.state.no_seri_baru === '' ||
        this.state.asal_mtu_baru === '' ||
        this.state.tahun_pembuatan_lama === '' ||
        this.state.type_baru === '' ||
        this.state.foto_nameplate_lama.length === 0 ||
        this.state.foto_nameplate_baru.length === 0
      ) {
        Alert.alert('Lengkapi Form!', 'Form masih ada yang kosong.', [
          {
            text: 'Okay',
          },
        ]);
      } else {
        console.log('gagal');
        this.setState(
          {
            currentPosition: this.state.currentPosition + 1,
          },
          function() {
            this.setState({
              isLoading4: false,
            });
          },
        );
      }
    }
  }

  backPage() {
    this.setState({
      currentPosition: this.state.currentPosition - 1,
    });
  }

  render() {
    return (
      <Container>
        <Header
          transparent
          style={{
            marginTop: Platform.OS === 'ios' ? 0 : 0,
            borderBottomWidth: 0,
            backgroundColor: colors.greenpln,
          }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
            }}>
            <Button
              onPress={() => this.props.navigation.goBack()}
              transparent
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <Icon
                name="md-arrow-round-back"
                size={23}
                style={{color: 'white'}}
              />
              <Text
                uppercase={false}
                style={{
                  flex: 1,
                  textAlignVertical: 'center',
                  fontWeight: 'bold',
                  fontSize: 20,
                  color: 'white',
                }}>
                Input Pergantian Aset
              </Text>
            </Button>
          </View>
        </Header>
        <Content>
          <View style={{marginTop: 20, marginBottom: 10}}>
            <StepIndicator
              customStyles={styles.customStyles}
              currentPosition={this.state.currentPosition}
              labels={labels}
              stepCount={4}
            />
          </View>
          {this.state.currentPosition === 0 ? (
            <View>
              {this.state.isLoading1 === true ? (
                <View style={{alignItems: 'center'}}>
                  <ActivityIndicator size="large" />
                </View>
              ) : (
                <CardItem>
                  <View style={{flex: 1}}>
                    <View style={{marginTop: 0}}>
                      <Text style={styles.fontLabel}>NIP</Text>
                    </View>
                    <View>
                      <TextInput
                        style={styles.fontTextInput}
                        rowSpan={1}
                        editable={true}
                        bordered
                        value={this.state.DataUser.user.nip}
                        placeholder="Input NIP..."
                        onChangeText={text => this.setState({nip: text})}
                      />
                    </View>
                    <View style={{marginTop: 10}}>
                      <Text style={[styles.fontLabel]}>Tanggal Pekerjaan</Text>
                    </View>
                    <View>
                      <DatePicker
                        style={{width: '100%', height: 50}}
                        date={this.state.tgl_pekerjaan}
                        mode="date"
                        placeholder="Select Date"
                        format="YYYY-MM-DD"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={styles.datePicker}
                        onDateChange={date => {
                          this.setState({tgl_pekerjaan: date});
                        }}
                      />
                    </View>
                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Jenis Pekerjaan
                      </Text>
                    </View>
                    <View>
                      <SearchableDropdown
                        api="jenis_pekerjaan/get_all_data_jenis_pekerjaan"
                        searchByApi={false}
                        displayData="nm_jenis_pekerjaan"
                        bodyApi={[
                            {
                              name: 'keyword',
                              value: 'C03,C08',
                            },
                          ]}
                        selectedMethod={item =>
                          this.setState({
                            nm_jenis_pekerjaan: item.nm_jenis_pekerjaan,
                            id_jenis_pekerjaan: item.id_jenis_pekerjaan,
                          })
                        }
                        placeholder="Pilih Pekerjaan..."
                      />
                    </View>
                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Wilayah
                      </Text>
                    </View>
                    <View>
                      <SearchableDropdown
                        api="ultg/get_all_data_ultg"
                        searchByApi={false}
                        displayData="wilayah"
                        selectedMethod={item =>
                          this.setState(
                            {
                              wilayah: item.wilayah,
                              id_ultg: item.id_ultg,
                            },
                            function() {
                              console.log('idultg', this.state.id_ultg);
                            },
                          )
                        }
                        placeholder="Pilih Wilayah..."
                      />
                    </View>
                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Bidang
                      </Text>
                    </View>
                    <View>
                      <SearchableDropdown
                        api="bidang/get_all_data_bidang"
                        displayData="nm_bidang"
                        selectedMethod={item =>
                          this.setState({
                            nm_bidang: item.nm_bidang,
                            id_bidang: item.id_bidang,
                          })
                        }
                        placeholder="Pilih Lokasi Pekerjaan..."
                      />
                    </View>
                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Gardu Induk
                      </Text>
                    </View>
                    <View>
                      <SearchableDropdown
                        api="gardu_induk/get_all_data_gardu_induk"
                        searchByApi={false}
                        displayData="nm_gardu_induk"
                        selectedMethod={item =>
                          this.setState(
                            {
                              nm_gardu_induk: item.nm_gardu_induk,
                              id_gardu_induk: item.id_gardu_induk,
                              isLoadingBay: true,
                              enabledBay: false,
                            },
                            function() {
                              this.setState({
                                isLoadingBay: false,
                              });
                            },
                          )
                        }
                        selected={this.state.nm_gardu_induk}
                        placeholder="Pilih Gardu Induk..."
                      />
                    </View>

                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        BAY
                      </Text>
                    </View>
                    <View>
                      {!this.state.isLoadingBay ? (
                        <SearchableDropdown
                          api="bay/bay_gardu_induk"
                          searchByApi={false}
                          bodyApi={[
                            {
                              name: 'id_gardu_induk',
                              value: this.state.id_gardu_induk,
                            },
                          ]}
                          displayData="nm_bay"
                          selectedMethod={item =>
                            this.setState({
                              nm_bay: item.nm_bay,
                              id_bay: item.id_bay,
                              nextButton: false,
                            })
                          }
                          disabled={this.state.enabledBay}
                          placeholder="Pilih BAY..."
                        />
                      ) : (
                        <View style={{alignItems: 'center'}}>
                          <ActivityIndicator />
                        </View>
                      )}
                    </View>
                    <View style={{marginTop: 40}}>
                      <View style={{flexDirection: 'row'}}>
                        <Button
                          disabled={true}
                          style={{
                            flex: 1,
                            marginRight: 5,
                            backgroundColor: colors.grey,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Prev
                            </Text>
                          </View>
                        </Button>
                        <Button
                          onPress={() => this.movePage()}
                          style={{
                            flex: 1,
                            marginLeft: 5,
                            backgroundColor: colors.greenpln,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Next
                            </Text>
                          </View>
                        </Button>
                      </View>
                    </View>
                  </View>
                </CardItem>
              )}
            </View>
          ) : this.state.currentPosition === 1 ? (
            <CardItem>
              {this.state.isLoading2 === true ? (
                <View style={{alignItems: 'center'}}>
                  <ActivityIndicator size="large" />
                </View>
              ) : (
                <View style={{flex: 1}}>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>
                      Peralatan yang di ganti
                    </Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.pelaksana}
                      placeholder="*Contoh : CT 150kV"
                      onChangeText={text =>
                        this.setState({peralatan_diganti: text})
                      }
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>Alasan Penggantian</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.pelaksana}
                      placeholder="*Contoh : Hasil uji tangen delta CT 150 kV sudah buruk"
                      onChangeText={text =>
                        this.setState({alasan_diganti: text})
                      }
                    />
                  </View>
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Phasa
                    </Text>
                  </View>
                  <View>
                    <RadioButton
                      option={Phasa}
                      displayData="NAMA_LIST"
                      onPressMethod={data =>
                        this.setState({
                          id_phasa: data.ID_OPTIONS_LIST,
                          phasa: data.NAMA_LIST,
                        })
                      }
                    />
                  </View>
                  <View style={{marginTop: 40}}>
                    <View style={{flexDirection: 'row'}}>
                      <Button
                        onPress={() => this.backPage()}
                        style={{
                          flex: 1,
                          marginRight: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Prev
                          </Text>
                        </View>
                      </Button>
                      <Button
                        onPress={() => this.movePage()}
                        style={{
                          flex: 1,
                          marginLeft: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Next
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </View>
                </View>
              )}
            </CardItem>
          ) : this.state.currentPosition === 2 ? (
            <CardItem>
              {this.state.isLoading3 === true ? (
                <View style={{alignItems: 'center'}}>
                  <ActivityIndicator size="large" />
                </View>
              ) : (
                <View style={{flex: 1}}>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>Merk Phasa R Lama</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.merk_lama}
                      placeholder="Input Jawaban..."
                      onChangeText={text => this.setState({merk_lama: text})}
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>
                      Relokasi MTU Lama Phasa R Lama
                    </Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.relokasi_mtu_lama}
                      placeholder="Input Jawaban..."
                      onChangeText={text =>
                        this.setState({relokasi_mtu_lama: text})
                      }
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>Type Phasa R Lama</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.type_lama}
                      placeholder="Input Jawaban..."
                      onChangeText={text => this.setState({type_lama: text})}
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>No Seri Phasa R Lama</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.no_seri_lama}
                      placeholder="Input Jawaban..."
                      onChangeText={text => this.setState({no_seri_lama: text})}
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>
                      Tahun Buat Phasa R Lama
                    </Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.tahun_pembuatan_lama}
                      placeholder="Input Jawaban..."
                      onChangeText={text =>
                        this.setState({tahun_pembuatan_lama: text})
                      }
                    />
                  </View>
                  <View style={{flex: 1, marginLeft: 0}}>
                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Upload Foto Nameplate Phasa R Lama
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        borderColor: colors.greenpln,
                        borderWidth: 1,
                        borderRadius: 5,
                      }}>
                      <View style={{backgroundColor: colors.greenpln}}>
                        <View style={{marginLeft: 10, padding: 5}}>
                          <Text style={{fontWeight: 'bold', color: 'white'}}>
                            Tambah File
                          </Text>
                        </View>
                      </View>
                      <View>
                        <View style={{height: '100%', paddingVertical: 25}}>
                          <Button
                            onPress={this.toggleLama}
                            style={{justifyContent: 'center'}}
                            transparent>
                            {this.state.isNameplate1 === true ? (
                              <Image
                                source={{
                                  uri: this.state.foto_nameplate_lama[0].uri,
                                }}
                                style={{
                                  height: 90,
                                  width: 130,
                                  resizeMode: 'contain',
                                }}
                              />
                            ) : (
                              <Image
                                source={require('../../../res/images/folder.png')}
                                style={{
                                  height: 90,
                                  width: 130,
                                  resizeMode: 'contain',
                                }}
                              />
                            )}
                          </Button>
                        </View>
                      </View>
                    </View>
                  </View>

                  <View style={{alignItems: 'center', marginTop: 15}}>
                    <Icon
                      name="ios-arrow-dropdown-circle"
                      size={40}
                      color="black"
                    />
                  </View>

                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>Merk Phasa R Baru</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.merk_baru}
                      placeholder="Input Jawaban..."
                      onChangeText={text => this.setState({merk_baru: text})}
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>Asal MTU Baru Phasa R</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.asal_mtu_baru}
                      placeholder="Input Jawaban..."
                      onChangeText={text =>
                        this.setState({asal_mtu_baru: text})
                      }
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>Type Phasa R Baru</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.type_baru}
                      placeholder="Input Jawaban..."
                      onChangeText={text => this.setState({type_baru: text})}
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>No Seri Phasa R Baru</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.no_seri_baru}
                      placeholder="Input Jawaban..."
                      onChangeText={text => this.setState({no_seri_baru: text})}
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>
                      Tahun Buat Phasa R Baru
                    </Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.tahun_pembuatan_baru}
                      placeholder="Input Jawaban..."
                      onChangeText={text =>
                        this.setState({tahun_pembuatan_baru: text})
                      }
                    />
                  </View>
                  <View style={{flex: 1, marginLeft: 5}}>
                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Upload Foto Nameplate Phasa R Baru
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        borderColor: colors.greenpln,
                        borderWidth: 1,
                        borderRadius: 5,
                      }}>
                      <View style={{backgroundColor: colors.greenpln}}>
                        <View style={{marginLeft: 10, padding: 5}}>
                          <Text style={{fontWeight: 'bold', color: 'white'}}>
                            Tambah File
                          </Text>
                        </View>
                      </View>
                      <View>
                        <View style={{height: '100%', paddingVertical: 25}}>
                          <Button
                            onPress={this.toggleBaru}
                            style={{justifyContent: 'center'}}
                            transparent>
                            {this.state.isNameplate2 === true ? (
                              <Image
                                source={{
                                  uri: this.state.foto_nameplate_baru[0].uri,
                                }}
                                style={{
                                  height: 90,
                                  width: 130,
                                  resizeMode: 'contain',
                                }}
                              />
                            ) : (
                              <Image
                                source={require('../../../res/images/folder.png')}
                                style={{
                                  height: 90,
                                  width: 130,
                                  resizeMode: 'contain',
                                }}
                              />
                            )}
                          </Button>
                        </View>
                      </View>
                    </View>
                  </View>
                  {/* modal lama*/}
                  <Modal
                    style={{justifyContent: 'flex-end'}}
                    backdropOpacity={0.3}
                    animationIn={'slideInUp'}
                    animationOut={'slideOutDown'}
                    isVisible={this.state.isModalLama}
                    onBackdropPress={this.toggleLama}
                    onBackButtonPress={this.toggleLama}>
                    <View style={{marginBottom: 10}}>
                      <View
                        style={{
                          borderRadius: 5,
                          flexDirection: 'row',
                          paddingHorizontal: 30,
                          paddingVertical: 40,
                          backgroundColor: 'white',
                        }}>
                        <View style={{marginRight: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() =>
                              this.pickNameplateLamaWithCamera(false)
                            }>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/camera.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Take a Photo
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                        <View style={{marginLeft: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickNameplateLama(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/galery.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Select from Galery
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                      </View>
                    </View>

                    <View style={{borderRadius: 5, marginBottom: 10}}>
                      <Button
                        onPress={this.toggleLama}
                        style={{backgroundColor: 'white'}}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{
                              color: colors.greenpln,
                              textAlign: 'center',
                            }}>
                            Cancel
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </Modal>

                  {/* modal baru*/}
                  <Modal
                    style={{justifyContent: 'flex-end'}}
                    backdropOpacity={0.3}
                    animationIn={'slideInUp'}
                    animationOut={'slideOutDown'}
                    isVisible={this.state.isModalBaru}
                    onBackdropPress={this.toggleBaru}
                    onBackButtonPress={this.toggleBaru}>
                    <View style={{marginBottom: 10}}>
                      <View
                        style={{
                          borderRadius: 5,
                          flexDirection: 'row',
                          paddingHorizontal: 30,
                          paddingVertical: 40,
                          backgroundColor: 'white',
                        }}>
                        <View style={{marginRight: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() =>
                              this.pickNameplateBaruWithCamera(false)
                            }>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/camera.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Take a Photo
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                        <View style={{marginLeft: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickNameplateBaru(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/galery.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Select from Galery
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                      </View>
                    </View>

                    <View style={{borderRadius: 5, marginBottom: 10}}>
                      <Button
                        onPress={this.toggleBaru}
                        style={{backgroundColor: 'white'}}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{
                              color: colors.greenpln,
                              textAlign: 'center',
                            }}>
                            Cancel
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </Modal>
                  <View style={{marginTop: 40}}>
                    <View style={{flexDirection: 'row'}}>
                      <Button
                        onPress={() => this.backPage()}
                        style={{
                          flex: 1,
                          marginRight: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Prev
                          </Text>
                        </View>
                      </Button>
                      <Button
                        onPress={() => this.movePage()}
                        style={{
                          flex: 1,
                          marginLeft: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Next
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </View>
                </View>
              )}
            </CardItem>
          ) : (
            <CardItem>
              {this.state.isLoading4 === true ? (
                <View style={{alignItems: 'center'}}>
                  <ActivityIndicator size="large" />
                </View>
              ) : (
                <View style={{flex: 1}}>
                  <View style={{flex: 1, marginLeft: 5}}>
                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Dokumentasi Pekerjaan 1
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        borderColor: colors.greenpln,
                        borderWidth: 1,
                        borderRadius: 5,
                      }}>
                      <View style={{backgroundColor: colors.greenpln}}>
                        <View style={{marginLeft: 10, padding: 5}}>
                          <Text style={{fontWeight: 'bold', color: 'white'}}>
                            Tambah File
                          </Text>
                        </View>
                      </View>
                      <View>
                        <View style={{height: '100%', paddingVertical: 25}}>
                          <Button
                            onPress={this.toggleModal}
                            style={{justifyContent: 'center'}}
                            transparent>
                            {this.state.isOpenImage === true ? (
                              <Image
                                source={{
                                  uri: this.state.list_image[0].uri,
                                }}
                                style={{
                                  height: 90,
                                  width: 130,
                                  resizeMode: 'contain',
                                }}
                              />
                            ) : (
                              <Image
                                source={require('../../../res/images/folder.png')}
                                style={{
                                  height: 90,
                                  width: 130,
                                  resizeMode: 'contain',
                                }}
                              />
                            )}
                          </Button>
                        </View>
                      </View>
                    </View>
                    <Textarea
                      style={styles.textArea}
                      rowSpan={1.7}
                      bordered
                      placeholderTextColor={colors.gray02}
                      value={this.state.penjelasan_dokumentasi_satu}
                      placeholder="Penjelasan Dokumentasi 1"
                      onChangeText={text =>
                        this.setState({penjelasan_dokumentasi_satu: text})
                      }
                    />
                  </View>
                  <View style={{flex: 1, marginLeft: 5}}>
                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Dokumentasi Pekerjaan 2
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        borderColor: colors.greenpln,
                        borderWidth: 1,
                        borderRadius: 5,
                      }}>
                      <View style={{backgroundColor: colors.greenpln}}>
                        <View style={{marginLeft: 10, padding: 5}}>
                          <Text style={{fontWeight: 'bold', color: 'white'}}>
                            Tambah File
                          </Text>
                        </View>
                      </View>
                      <View>
                        <View style={{height: '100%', paddingVertical: 25}}>
                          <Button
                            onPress={this.toggleModal2}
                            style={{justifyContent: 'center'}}
                            transparent>
                            {this.state.isOpenImage2 === true ? (
                              <Image
                                source={{
                                  uri: this.state.list_image2[0].uri,
                                }}
                                style={{
                                  height: 90,
                                  width: 130,
                                  resizeMode: 'contain',
                                }}
                              />
                            ) : (
                              <Image
                                source={require('../../../res/images/folder.png')}
                                style={{
                                  height: 90,
                                  width: 130,
                                  resizeMode: 'contain',
                                }}
                              />
                            )}
                          </Button>
                        </View>
                      </View>
                    </View>
                    <Textarea
                      style={styles.textArea}
                      rowSpan={1.7}
                      bordered
                      placeholderTextColor={colors.gray02}
                      value={this.state.penjelasan_dokumentasi_dua}
                      placeholder="Penjelasan Dokumentasi 2"
                      onChangeText={text =>
                        this.setState({penjelasan_dokumentasi_dua: text})
                      }
                    />
                  </View>
                  {/* modal image 1*/}
                  <Modal
                    style={{justifyContent: 'flex-end'}}
                    backdropOpacity={0.3}
                    animationIn={'slideInUp'}
                    animationOut={'slideOutDown'}
                    isVisible={this.state.isModalVisible}
                    onBackdropPress={this.toggleModal}
                    onBackButtonPress={this.toggleModal}>
                    <View style={{marginBottom: 10}}>
                      <View
                        style={{
                          borderRadius: 5,
                          flexDirection: 'row',
                          paddingHorizontal: 30,
                          paddingVertical: 40,
                          backgroundColor: 'white',
                        }}>
                        <View style={{marginRight: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingleWithCamera(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/camera.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Take a Photo
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                        <View style={{marginLeft: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingle(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/galery.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Select from Galery
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                      </View>
                    </View>

                    <View style={{borderRadius: 5, marginBottom: 10}}>
                      <Button
                        onPress={this.toggleModal}
                        style={{backgroundColor: 'white'}}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{
                              color: colors.greenpln,
                              textAlign: 'center',
                            }}>
                            Cancel
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </Modal>

                  {/* modal image 2*/}
                  <Modal
                    style={{justifyContent: 'flex-end'}}
                    backdropOpacity={0.3}
                    animationIn={'slideInUp'}
                    animationOut={'slideOutDown'}
                    isVisible={this.state.isModalVisible2}
                    onBackdropPress={this.toggleModal2}
                    onBackButtonPress={this.toggleModal2}>
                    <View style={{marginBottom: 10}}>
                      <View
                        style={{
                          borderRadius: 5,
                          flexDirection: 'row',
                          paddingHorizontal: 30,
                          paddingVertical: 40,
                          backgroundColor: 'white',
                        }}>
                        <View style={{marginRight: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingleWithCamera2(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/camera.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Take a Photo
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                        <View style={{marginLeft: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingle2(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/galery.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Select from Galery
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                      </View>
                    </View>

                    <View style={{borderRadius: 5, marginBottom: 10}}>
                      <Button
                        onPress={this.toggleModal2}
                        style={{backgroundColor: 'white'}}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{
                              color: colors.greenpln,
                              textAlign: 'center',
                            }}>
                            Cancel
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </Modal>
                  <View style={{marginTop: 40}}>
                    <View style={{flexDirection: 'row'}}>
                      <Button
                        onPress={() => this.backPage()}
                        style={{
                          flex: 1,
                          marginRight: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Prev
                          </Text>
                        </View>
                      </Button>
                      {this.state.isLoadingSave === true ? (
                        <View style={{flex: 1, alignItems: 'center'}}>
                          <ActivityIndicator />
                        </View>
                      ) : (
                        <Button
                          onPress={() => this.onKirimData()}
                          style={{
                            flex: 1,
                            marginLeft: 5,
                            backgroundColor: colors.green01,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Kirim
                            </Text>
                          </View>
                        </Button>
                      )}
                    </View>
                  </View>
                </View>
              )}
            </CardItem>
          )}
        </Content>
      </Container>
    );
  }
}
const Phasa = [
  {NAMA_LIST: 'R', ID_OPTIONS_LIST: 'R'},
  {NAMA_LIST: 'S', ID_OPTIONS_LIST: 'S'},
  {NAMA_LIST: 'T', ID_OPTIONS_LIST: 'T'},
  {NAMA_LIST: 'RST', ID_OPTIONS_LIST: 'RST'},
  {NAMA_LIST: 'RST SET', ID_OPTIONS_LIST: 'RST SET'},
];

const JenisBeritaAcara = [
  {NAMA_LIST: 'Pekerjaan Umum', id_lkso: '0'},
  {NAMA_LIST: 'LKS Online', id_lkso: '1'},
];

const LokasiPekerjaan = [
  {NAMA_LIST: 'GI (GARDU INDUK)', ID_OPTIONS_LIST: '1'},
  {NAMA_LIST: 'SITE UPT', ID_OPTIONS_LIST: '2'},
  {NAMA_LIST: 'SITE ULTG GRESIK', ID_OPTIONS_LIST: '3'},
  {NAMA_LIST: 'SITE ULTG SAMPANG', ID_OPTIONS_LIST: '4'},
  {NAMA_LIST: 'GUDANG TANDES', ID_OPTIONS_LIST: '5'},
];

const Wilayah = [
  {NAMA_LIST: 'Gresik', ID_OPTIONS_LIST: '1'},
  {NAMA_LIST: 'Sampang', ID_OPTIONS_LIST: '2'},
];

const Bidang = [
  {NAMA_LIST: 'Gardu Induk', ID_OPTIONS_LIST: '1'},
  {NAMA_LIST: 'Hargi', ID_OPTIONS_LIST: '2'},
];

const labels = ['Data BA', 'Peralatan', 'Nameplate Phasa', 'Dokumentasi'];
