/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Image,
  StatusBar,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Alert,
  BackHandler,
  Platform,
  DeviceEventEmitter,
  PushNotificationIOS,
  ImageBackground,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {
  View,
  Text,
  Container,
  Header,
  Form,
  Item,
  Label,
  Input,
  Footer,
  Left,
  Right,
  Button,
  Body,
  Title,
  Card,
  CheckBox,
  Content,
} from 'native-base';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from '../../res/colors';
import ListAset from './ItemHomeAset/ListAset';
import FloatingButtonBA from '../../library/component/FloatingButtonAset';
import LinearGradient from 'react-native-linear-gradient';
import RadioButton from '../../library/component/CustomRadioButton';
import SearchableDropdown from '../../library/component/SearchableDropdown';

export default class HomeAset extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      isFilterVisible: false,
      isLoading: false,
      id_gardu_induk: '',
      nama_status: '',
      visibleInput: true,
      status: '',
    };
  }

  onResetFilter() {
    this.setState(
      {
        id_gardu_induk: '',
        nm_gardu_induk: 'ALL GARDU INDUK',
        nama_status: '',
        status: '',
      },
      function() {
        this.setState(
          {
            isFilterVisible: false,
            isLoading: true,
          },
          function() {
            this.setState({
              isLoading: false,
            });
          },
        );
      },
    );
  }

  FilterModal = () => {
    this.setState({isFilterVisible: !this.state.isFilterVisible});
  };
  onFilter() {
    this.setState(
      {
        isFilterVisible: false,
        isLoading: true,
      },
      function() {
        this.setState({
          isLoading: false,
        });
      },
    );
  }

  componentDidMount() {
    // AsyncStorage.getItem('DataUser').then(value =>
    //   this.setState(
    //     {
    //       DataUser: JSON.parse(value),
    //     },
    //     function () {
    //       const jabatan = this.state.DataUser.detail_jabatan[0];
    //       if (jabatan.nm_jenis_pegawai === 'MULTG') {
    //         this.setState({
    //           visibleInput: false,
    //         });
    //       } else {
    //         this.setState({
    //           visibleInput: true,
    //         });
    //       }
    //     },
    //   ),
    // );
  }

  render() {
    return (
      <Container style={{flex: 1, display: 'flex'}}>
        <LinearGradient
          // source={require('../../res/images/BGdashboard.jpg')}
          colors={[colors.greenpln, 'white']}
          style={{
            flex: 1,
          }}>
          <Header
            transparent
            style={{
              marginTop: Platform.OS === 'ios' ? 0 : 0,
              borderBottomWidth: 0,
            }}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'center',
              }}>
              <View style={{flex: 6, justifyContent: 'center'}}>
                <Text
                  style={{
                    flex: 1,
                    marginLeft: 10,
                    textAlignVertical: 'center',
                    fontWeight: 'bold',
                    fontSize: 20,
                    color: 'white',
                  }}>
                  Pergantian Aset
                </Text>
              </View>
              <View style={{flex: 1, marginRight: 5}}>
                <Button
                  onPress={() => this.FilterModal()}
                  style={{flex: 1, justifyContent: 'center'}}
                  transparent>
                  <Icon name="sort" size={30} style={{color: 'white'}} />
                </Button>
                <Modal
                  style={{
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                    marginBottom: 0,
                    marginTop: 0,
                    marginRight: 0,
                  }}
                  animationIn={'slideInRight'}
                  isVisible={this.state.isFilterVisible}
                  onBackdropPress={this.FilterModal}
                  backdropOpacity={0}
                  animationOut={'slideOutRight'}
                  animationInTiming={1000}>
                  <LinearGradient
                    colors={[colors.greenpln, 'white']}
                    style={{
                      flex: 1,
                      padding: 15,
                      width: '70%',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        marginTop: 25,
                        borderBottomWidth: 1,
                        paddingBottom: 5,
                        borderColor: colors.greenpln,
                        justifyContent: 'flex-end',
                      }}>
                      <View style={{flex: 2, justifyContent: 'center'}}>
                        <Text
                          style={{
                            marginLeft: 10,
                            fontSize: 20,
                            fontWeight: 'bold',
                          }}>
                          Filter
                        </Text>
                      </View>

                      <TouchableOpacity
                        onPress={() => this.onResetFilter()}
                        style={{
                          flex: 2,
                          borderRadius: 2,
                          justifyContent: 'center',
                          backgroundColor: colors.greenpln,
                        }}>
                        <Text
                          style={{
                            color: 'white',
                            fontWeight: 'bold',
                            textAlign: 'center',
                          }}>
                          Reset Filter
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <ScrollView>
                      <View style={{marginTop: 10}}>
                        <Text
                          style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 12,
                            marginLeft: 5,
                          }}>
                          Gardu Induk
                        </Text>
                      </View>
                      <View style={{marginTop: 10}}>
                        <SearchableDropdown
                          api="gardu_induk/get_all_data_gardu_induk"
                          searchByApi={false}
                          displayData="nm_gardu_induk"
                          otherList={{
                            nm_gardu_induk: 'ALL GARDU INDUK',
                            id_gardu_induk: '',
                          }}
                          selectedMethod={item =>
                            this.setState({
                              nm_gardu_induk: item.nm_gardu_induk,
                              id_gardu_induk: item.id_gardu_induk,
                            })
                          }
                          selected={this.state.nm_gardu_induk}
                          placeholder="Pilih Gardu Induk..."
                        />
                      </View>
                      <View style={{marginTop: 10}}>
                        <Text
                          style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 12,
                            marginLeft: 5,
                          }}>
                          Status Approve
                        </Text>
                      </View>
                      <View
                        style={{
                          marginTop: 5,
                          backgroundColor: 'white',
                          borderRadius: 10,
                        }}>
                        <RadioButton
                          option={StatusApprove}
                          selected={this.state.nama_status}
                          displayData="NAMA_LIST"
                          onPressMethod={data =>
                            this.setState({
                              nama_status: data.NAMA_LIST,
                              status: data.ID_LIST,
                            })
                          }
                        />
                      </View>
                      <View style={{marginTop: 20}}>
                        <Button
                          onPress={() => this.onFilter()}
                          style={{
                            flex: 1,
                            backgroundColor: colors.green01,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Filter Data
                            </Text>
                          </View>
                        </Button>
                      </View>
                    </ScrollView>
                  </LinearGradient>
                </Modal>
              </View>
            </View>
          </Header>
          {this.state.isLoading === true ? (
            <View style={{alignItems: 'center'}}>
              <ActivityIndicator size="large" />
            </View>
          ) : (
            <View style={{flex: 1, flexDirection: 'column'}}>
              <ListAset
                navigation={this.props.navigation}
                id_gardu_induk={this.state.id_gardu_induk}
                status={this.state.status}
              />
            </View>
          )}
        </LinearGradient>
        <FloatingButtonBA navigation={this.props.navigation} />

        {/* {this.state.visibleInput === true ? (
          <FloatingButtonBA navigation={this.props.navigation} />
        ) : null} */}
      </Container>
    );
  }
}
const StatusApprove = [
  {NAMA_LIST: 'ALL STATUS', ID_LIST: ''},
  {NAMA_LIST: 'BELUM KONFIRMASI', ID_LIST: ''},
  {NAMA_LIST: 'APPROVE', ID_LIST: 'Y'},
  {NAMA_LIST: 'REJECT', ID_LIST: 'N'},
];
