/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Image,
  View,
  TextInput,
  Platform,
  AsyncStorage,
  ActivityIndicator,
  Alert,
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Button,
  Content,
  CardItem,
  Text,
  Textarea,
} from 'native-base';
import DatePicker from 'react-native-datepicker';
import Icon from 'react-native-vector-icons/Ionicons';
import SearchableDropdown from '../../../library/component/SearchableDropdown';
import RadioButton from '../../../library/component/CustomRadioButton';
import styles from '../../../res/styles/Form';
import colors from '../../../res/colors/index';
import StepIndicator from 'react-native-step-indicator';
import ImagePicker from 'react-native-image-crop-picker';
import Modal from 'react-native-modal';
import GlobalConfig from '../../../library/network/GlobalConfig';

export default class InputBA extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading1: true,
      isLoading2: false,
      isLoading3: false,
      isLoading4: false,
      isLoadingNoLKS: false,
      currentPosition: 0,

      isLoadingBay: false,
      enabledBay: true,

      isModalVisible: false,
      isModalVisible2: false,
      isModalVisible3: false,

      isOpenImage: false,
      isOpenImage2: false,
      isOpenImage3: false,

      list_image: [],
      list_image2: [],
      list_image3: [],

      tgl_realisasi: '',
      Jenis_BA: 'LKS Online',
      id_jenis_ba: '1',
      status_penyelesaian: '',
      keterangan_tambahan: '',
      //2
      pelaksana: '',
      id_bidang: '',
      wilayah: '',
      lokasi_pekerjaan: '',
      //3
      id_gardu_induk: '',
      id_bay: '',
      id_tegangan: '',
      detail_lokasi: '',

      //4
      id_Lokasi_Pekerjaan: '',
      id_jenis_pekerjaan: '',
      penjelasan_dokumentasi_satu: '',
      penjelasan_dokumentasi_dua: '',
      penjelasan_dokumentasi_tiga: '',
    };
  }

  onKirimData() {
    this.setState({
      isLoadingSave: true,
    });
    if (
      this.state.uraian_detail_pekerjaan === '' ||
      this.state.uraian_hasil_pekerjaan === '' ||
      this.state.penjelasan_dokumentasi_satu === '' ||
      this.state.id_jenis_pekerjaan === ''
    ) {
      alert('Form belum lengkap');
      this.setState({
        isLoadingSave: false,
      });
    } else {
      var url = GlobalConfig.SERVERHOST + 'baol/add_data_baol';
      var formData = new FormData();
      formData.append('nip', this.state.DataUser.user.nip);
      formData.append('tgl_realisasi', this.state.tgl_realisasi);
      formData.append('jenis_ba', this.state.id_jenis_ba);
      if (this.state.id_jenis_ba === '1') {
        formData.append('id_lkso', this.state.no_lkso);
      }
      formData.append('status_penyelesaian', this.state.status_penyelesaian);
      formData.append('keterangan_tambahan', this.state.keterangan_tambahan);
      formData.append('lokasi_pekerjaan', this.state.lokasi_pekerjaan);
      formData.append('wilayah', this.state.id_wilayah);
      formData.append('id_bidang', this.state.id_bidang);
      formData.append('pelaksana', this.state.pelaksana);
      formData.append('id_gardu_induk', this.state.id_gardu_induk);
      formData.append('id_bay', this.state.id_bay);
      formData.append('id_tegangan', this.state.id_tegangan);
      formData.append('detail_lokasi', this.state.detail_lokasi);
      formData.append('id_jenis_pekerjaan', this.state.id_jenis_pekerjaan);
      formData.append(
        'uraian_detail_pekerjaan',
        this.state.uraian_detail_pekerjaan,
      );
      formData.append(
        'uraian_hasil_pekerjaan',
        this.state.uraian_hasil_pekerjaan,
      );
      if (this.state.penjelasan_dokumentasi_satu !== '') {
        formData.append(
          'dokumentasi_satu',
          {
            uri: this.state.list_image[0].uri,
            name: this.state.list_image[0].name + '.jpg',
            type: 'image/jpg',
          },
          'file',
        );
        formData.append(
          'penjelasan_dokumentasi_satu',
          this.state.penjelasan_dokumentasi_satu,
        );
      }

      if (this.state.penjelasan_dokumentasi_dua !== '') {
        formData.append(
          'dokumentasi_dua',
          {
            uri: this.state.list_image2[0].uri,
            name: this.state.list_image2[0].name + '.jpg',
            type: 'image/jpg',
          },
          'file',
        );
        formData.append(
          'penjelasan_dokumentasi_dua',
          this.state.penjelasan_dokumentasi_dua,
        );
      }
      if (this.state.penjelasan_dokumentasi_tiga !== '') {
        formData.append(
          'dokumentasi_tiga',
          {
            uri: this.state.list_image3[0].uri,
            name: this.state.list_image3[0].name + '.jpg',
            type: 'image/jpg',
          },
          'file',
        );
        formData.append(
          'penjelasan_dokumentasi_tiga',
          this.state.penjelasan_dokumentasi_tiga,
        );
      }

      console.log('all FD', formData);
      fetch(url, {
        method: 'POST',
        body: formData,
      })
        .then(response => response.json())
        .then(response => {
          console.log('piye', response);
          if (response.status === 'success') {
            this.setState(
              {
                isLoadingSave: false,
              },
              function() {
                Alert.alert('Berhasil Menyimpan Data', response.message, [
                  {
                    text: 'Okay',
                  },
                ]);
                AsyncStorage.setItem('SavedBA', '1');
                AsyncStorage.setItem('SavedRencana', '1');
                this.props.navigation.goBack();
              },
            );
          } else {
            this.setState({
              isLoadingSave: false,
            });
            Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [
              {
                text: 'Okay',
              },
            ]);
          }
        })
        .catch(error => {
          this.setState({
            isLoadingSave: false,
          });
          Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [
            {
              text: 'Okay',
            },
          ]);
          console.log(error);
        });
    }
  }

  toggleModal = () => {
    this.setState({isModalVisible: !this.state.isModalVisible});
  };
  toggleModal2 = () => {
    this.setState({isModalVisible2: !this.state.isModalVisible2});
  };

  toggleModal3 = () => {
    this.setState({isModalVisible3: !this.state.isModalVisible3});
  };

  pickSingle(cropping, mediaType = 'photo') {
    ImagePicker.openPicker({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible: false,
            list_image: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
            images: null,
          },
          function() {
            console.log('mlaaku', this.state.list_image[0].uri);
            this.setState({
              isOpenImage: true,
            });
          },
        );
      })
      .catch(e => {
        console.log(e);
        // Alert.alert(e.message ? e.message : e);
      });
  }

  pickSingleWithCamera(cropping, mediaType = 'photo') {
    ImagePicker.openCamera({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible: false,
            list_image: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
            images: null,
          },
          function() {
            console.log('mlaaku', this.state.list_image);
            this.setState({
              isOpenImage: true,
            });
          },
        );
      })
      .catch(e => alert(e));
    this.toggleModal;
  }

  pickSingle2(cropping, mediaType = 'photo') {
    ImagePicker.openPicker({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible2: false,
            list_image2: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
            images: null,
            uriPicked: this.state.list_image.map(data => data.uri),
            typePicked: this.state.list_image.map(data => data.mime),
          },
          function() {
            console.log('mlaaku', this.state.list_image2);
            this.setState({
              isOpenImage2: true,
            });
          },
        );
      })
      .catch(e => {
        console.log(e);
        // Alert.alert(e.message ? e.message : e);
      });
  }

  pickSingleWithCamera2(cropping, mediaType = 'photo') {
    ImagePicker.openCamera({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible2: false,
            list_image2: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
            images: null,
            uriPicked: this.state.list_image.map(data => data.uri),
            typePicked: this.state.list_image.map(data => data.mime),
          },
          function() {
            console.log('mlaaku', this.state.list_image);
            this.setState({
              isOpenImage2: true,
            });
          },
        );
      })
      .catch(e => alert(e));
    this.toggleModal;
  }

  pickSingle3(cropping, mediaType = 'photo') {
    ImagePicker.openPicker({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible3: false,
            list_image3: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
            images: null,
            uriPicked: this.state.list_image.map(data => data.uri),
            typePicked: this.state.list_image.map(data => data.mime),
          },
          function() {
            console.log('mlaaku', this.state.list_image);
            this.setState({
              isOpenImage3: true,
            });
          },
        );
      })
      .catch(e => {
        console.log(e);
        // Alert.alert(e.message ? e.message : e);
      });
  }

  pickSingleWithCamera3(cropping, mediaType = 'photo') {
    ImagePicker.openCamera({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible3: false,
            list_image3: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
            images: null,
            uriPicked: this.state.list_image.map(data => data.uri),
            typePicked: this.state.list_image.map(data => data.mime),
          },
          function() {
            console.log('mlaaku', this.state.list_image);
            this.setState({
              isOpenImage3: true,
            });
          },
        );
      })
      .catch(e => alert(e));
    this.toggleModal;
  }

  componentDidMount() {
    AsyncStorage.getItem('DataUser').then(value =>
      this.setState(
        {
          DataUser: JSON.parse(value),
        },
        function() {
          console.log('user aa', this.state.DataUser.user.id_pegawai);
        },
      ),
    );
    AsyncStorage.getItem('id_lkso').then(value =>
      this.setState(
        {
          id_lkso: value,
        },
        function() {
          console.log('idlkso 2', this.state.id_lkso);
          AsyncStorage.getItem('no_lkso').then(value2 =>
            this.setState(
              {
                no_lkso: value2,
              },
              function() {
                console.log('idlkso 2', this.state.no_lkso);
                this.setState({
                  isLoading1: false,
                });
              },
            ),
          );
        },
      ),
    );
  }

  movePage() {
    if (this.state.currentPosition === 0) {
      console.log('masuk');
      if (
        this.state.nip === '' ||
        this.state.tgl_realisasi === '' ||
        this.state.id_lkso === '' ||
        this.state.status_penyelesaian === '' ||
        this.state.keterangan_tambahan === ''
      ) {
        Alert.alert('Lengkapi Form!', 'Form masih ada yang kosong.', [
          {
            text: 'Okay',
          },
        ]);
      } else {
        console.log('gagal');
        this.setState({
          isLoading2: false,
          currentPosition: this.state.currentPosition + 1,
        });
      }
    } else if (this.state.currentPosition === 1) {
      this.setState({
        isLoading3: true,
      });
      // console.log('masuk');
      if (
        this.state.lokasi_pekerjaan === '' ||
        this.state.wilayah === '' ||
        this.state.id_bidang === '' ||
        this.state.pelaksana === ''
      ) {
        Alert.alert('Lengkapi Form!', 'Form masih ada yang kosong.', [
          {
            text: 'Okay',
          },
        ]);
      } else {
        console.log('gagal');
        this.setState(
          {
            currentPosition: this.state.currentPosition + 1,
          },
          function() {
            this.setState({
              isLoading3: false,
            });
          },
        );
      }
    } else if (this.state.currentPosition === 2) {
      this.setState({
        isLoading4: true,
      });
      // console.log('masuk');
      if (
        this.state.id_gardu_induk === '' ||
        this.state.id_bay === '' ||
        this.state.id_tegangan === '' ||
        this.state.detail_lokasi === ''
      ) {
        Alert.alert('Lengkapi Form!', 'Form masih ada yang kosong.', [
          {
            text: 'Okay',
          },
        ]);
      } else {
        console.log('gagal');
        this.setState(
          {
            currentPosition: this.state.currentPosition + 1,
          },
          function() {
            this.setState({
              isLoading4: false,
            });
          },
        );
      }
    }
  }

  backPage() {
    this.setState({
      currentPosition: this.state.currentPosition - 1,
    });
  }

  render() {
    return (
      <Container>
        <Header
          transparent
          style={{
            marginTop: Platform.OS === 'ios' ? 0 : 0,
            borderBottomWidth: 0,
            backgroundColor: colors.greenpln,
          }}>
          <View
            style={{
              flex: 2,
              justifyContent: 'center',
            }}>
            <Button
              onPress={() => this.props.navigation.goBack()}
              transparent
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <Icon
                name="md-arrow-round-back"
                size={23}
                style={{color: 'white'}}
              />
              <Text
                uppercase={false}
                style={{
                  flex: 1,
                  textAlignVertical: 'center',
                  fontWeight: 'bold',
                  fontSize: 20,
                  color: 'white',
                }}>
                Input Berita Acara
              </Text>
            </Button>
          </View>
          <View style={{flex: 1}} />
        </Header>
        <Content>
          <View style={{marginTop: 20, marginBottom: 10}}>
            <StepIndicator
              customStyles={styles.customStyles}
              currentPosition={this.state.currentPosition}
              labels={labels}
              stepCount={4}
            />
          </View>
          {this.state.currentPosition === 0 ? (
            <View>
              {this.state.isLoading1 === true ? (
                <View style={{alignItems: 'center'}}>
                  <ActivityIndicator size="large" />
                </View>
              ) : (
                <CardItem>
                  <View style={{flex: 1}}>
                    <View style={{marginTop: 0}}>
                      <Text style={styles.fontLabel}>NIP</Text>
                    </View>
                    <View>
                      <TextInput
                        style={styles.fontTextInput}
                        rowSpan={1}
                        editable={true}
                        bordered
                        value={this.state.DataUser.user.nip}
                        placeholder="Input NIP..."
                        onChangeText={text => this.setState({nip: text})}
                      />
                    </View>
                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Jenis Berita Acara
                      </Text>
                    </View>
                    <View>
                      <SearchableDropdown
                        items={JenisBeritaAcara}
                        displayData="NAMA_LIST"
                        selected={this.state.Jenis_BA}
                        selectedMethod={item =>
                          this.setState(
                            {
                              Jenis_BA: item.NAMA_LIST,
                              id_jenis_ba: item.id_lkso,
                              isLoadingNoLKS: true,
                            },
                            function() {
                              this.setState({
                                isLoadingNoLKS: false,
                              });
                            },
                          )
                        }
                        placeholder="Pilih Jenis Berita Acara..."
                      />
                    </View>

                    {this.state.isLoadingNoLKS ? (
                      <View>
                        <ActivityIndicator />
                      </View>
                    ) : (
                      <View>
                        {this.state.Jenis_BA === 'LKS Online' ? (
                          <View>
                            <View>
                              <Text style={[{marginTop: 10}, styles.fontLabel]}>
                                No LKS Online
                              </Text>
                            </View>
                            <View>
                              <SearchableDropdown
                                api="lkso/get_all_data_lkso"
                                searchByApi={false}
                                displayData="no_lkso"
                                selectedMethod={item =>
                                  this.setState(
                                    {
                                      no_lkso: item.no_lkso,
                                      id_lkso: item.id_lkso,
                                    },
                                    function() {
                                      console.log('nolks', this.state.id_lkso);
                                    },
                                  )
                                }
                                selected={this.state.no_lkso}
                                placeholder="Pilih No LKS..."
                              />
                            </View>
                          </View>
                        ) : (
                          <View />
                        )}
                      </View>
                    )}

                    <View style={{marginTop: 10}}>
                      <Text style={[styles.fontLabel]}>Tanggal Realisasi</Text>
                    </View>
                    <View>
                      <DatePicker
                        style={{width: '100%', height: 50}}
                        date={this.state.tgl_realisasi}
                        mode="date"
                        placeholder="Select Date"
                        format="YYYY-MM-DD"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={styles.datePicker}
                        onDateChange={date => {
                          this.setState({tgl_realisasi: date});
                        }}
                      />
                    </View>
                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Status Penyelesaian
                      </Text>
                    </View>
                    <View>
                      <RadioButton
                        option={ListPrioritas}
                        displayData="NAMA_LIST"
                        onPressMethod={data =>
                          this.setState({
                            id_status_penyelesaian: data.ID_OPTIONS_LIST,
                            status_penyelesaian: data.NAMA_LIST,
                          })
                        }
                      />
                    </View>
                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Keterangan Tambahan
                      </Text>
                    </View>
                    <View>
                      <Textarea
                        style={styles.textArea}
                        rowSpan={3}
                        bordered
                        placeholderTextColor={colors.gray02}
                        value={this.state.keterangan_tambahan}
                        placeholder="Isi keterangan tambahan ..."
                        onChangeText={text =>
                          this.setState({keterangan_tambahan: text})
                        }
                      />
                    </View>

                    <View style={{marginTop: 40}}>
                      <View style={{flexDirection: 'row'}}>
                        <Button
                          disabled={true}
                          style={{
                            flex: 1,
                            marginRight: 5,
                            backgroundColor: colors.grey,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Prev
                            </Text>
                          </View>
                        </Button>
                        <Button
                          onPress={() => this.movePage()}
                          style={{
                            flex: 1,
                            marginLeft: 5,
                            backgroundColor: colors.greenpln,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Next
                            </Text>
                          </View>
                        </Button>
                      </View>
                    </View>
                  </View>
                </CardItem>
              )}
            </View>
          ) : this.state.currentPosition === 1 ? (
            <CardItem>
              {this.state.isLoading2 === true ? (
                <View style={{alignItems: 'center'}}>
                  <ActivityIndicator size="large" />
                </View>
              ) : (
                <View style={{flex: 1}}>
                  <View>
                    <Text style={[{marginTop: 0}, styles.fontLabel]}>
                      Lokasi Pekerjaan
                    </Text>
                  </View>
                  <View>
                    <SearchableDropdown
                      items={LokasiPekerjaan}
                      displayData="NAMA_LIST"
                      selectedMethod={item =>
                        this.setState({
                          lokasi_pekerjaan: item.NAMA_LIST,
                          id_Lokasi_Pekerjaan: item.ID_OPTIONS_LIST,
                        })
                      }
                      placeholder="Pilih Lokasi Pekerjaan..."
                    />
                  </View>
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Wilayah
                    </Text>
                  </View>
                  <View>
                    <SearchableDropdown
                      api="ultg/get_all_data_ultg"
                      searchByApi={false}
                      displayData="wilayah"
                      selectedMethod={item =>
                        this.setState(
                          {
                            wilayah: item.wilayah,
                            id_wilayah: item.id_ultg,
                          },
                          function() {
                            console.log('idultg', this.state.id_wilayah);
                          },
                        )
                      }
                      placeholder="Pilih Wilayah..."
                    />
                  </View>
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Bidang
                    </Text>
                  </View>
                  <View>
                    <SearchableDropdown
                      api="bidang/get_all_data_bidang"
                      displayData="nm_bidang"
                      selectedMethod={item =>
                        this.setState({
                          nm_bidang: item.nm_bidang,
                          id_bidang: item.id_bidang,
                        })
                      }
                      placeholder="Pilih Lokasi Pekerjaan..."
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>Pelakasana Pekerjaan</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.pelaksana}
                      placeholder="Inputkan Nama Pelakasana"
                      onChangeText={text => this.setState({pelaksana: text})}
                    />
                  </View>

                  <View style={{marginTop: 40}}>
                    <View style={{flexDirection: 'row'}}>
                      <Button
                        onPress={() => this.backPage()}
                        style={{
                          flex: 1,
                          marginRight: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Prev
                          </Text>
                        </View>
                      </Button>
                      <Button
                        onPress={() => this.movePage()}
                        style={{
                          flex: 1,
                          marginLeft: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Next
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </View>
                </View>
              )}
            </CardItem>
          ) : this.state.currentPosition === 2 ? (
            <CardItem>
              {this.state.isLoading3 === true ? (
                <View style={{alignItems: 'center'}}>
                  <ActivityIndicator size="large" />
                </View>
              ) : (
                <View style={{flex: 1}}>
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Gardu Induk
                    </Text>
                  </View>
                  <View>
                    <SearchableDropdown
                      api="gardu_induk/get_all_data_gardu_induk"
                      searchByApi={false}
                      displayData="nm_gardu_induk"
                      selectedMethod={item =>
                        this.setState(
                          {
                            nm_gardu_induk: item.nm_gardu_induk,
                            id_gardu_induk: item.id_gardu_induk,
                            isLoadingBay: true,
                            enabledBay: false,
                          },
                          function() {
                            this.setState({
                              isLoadingBay: false,
                            });
                          },
                        )
                      }
                      selected={this.state.nm_gardu_induk}
                      placeholder="Pilih Gardu Induk..."
                    />
                  </View>

                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>BAY</Text>
                  </View>
                  <View>
                    {!this.state.isLoadingBay ? (
                      <SearchableDropdown
                        api="bay/bay_gardu_induk"
                        searchByApi={false}
                        bodyApi={[
                          {
                            name: 'id_gardu_induk',
                            value: this.state.id_gardu_induk,
                          },
                        ]}
                        displayData="nm_bay"
                        selectedMethod={item =>
                          this.setState({
                            nm_bay: item.nm_bay,
                            id_bay: item.id_bay,
                            nextButton: false,
                          })
                        }
                        disabled={this.state.enabledBay}
                        placeholder="Pilih BAY..."
                      />
                    ) : (
                      <View style={{alignItems: 'center'}}>
                        <ActivityIndicator />
                      </View>
                    )}
                  </View>
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Level Tegangan
                    </Text>
                  </View>
                  <View>
                    <SearchableDropdown
                      api="tegangan/get_all_data_tegangan"
                      searchByApi={false}
                      displayData="volume_tegangan"
                      otherList={{
                        volume_tegangan: 'Lainnya',
                        id_tegangan: '0',
                      }}
                      selectedMethod={item =>
                        this.setState({
                          volume_tegangan: item.volume_tegangan,
                          id_tegangan: item.id_tegangan,
                        })
                      }
                      placeholder="Pilih Level Tegangan..."
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>
                      Detail Lokasi GI/Jaringan
                    </Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.detail_lokasi}
                      placeholder="Input Detail Lokasi GI/Jaringan..."
                      onChangeText={text =>
                        this.setState({detail_lokasi: text})
                      }
                    />
                  </View>
                  <View style={{marginTop: 40}}>
                    <View style={{flexDirection: 'row'}}>
                      <Button
                        onPress={() => this.backPage()}
                        style={{
                          flex: 1,
                          marginRight: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Prev
                          </Text>
                        </View>
                      </Button>
                      <Button
                        onPress={() => this.movePage()}
                        style={{
                          flex: 1,
                          marginLeft: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Next
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </View>
                </View>
              )}
            </CardItem>
          ) : (
            <CardItem>
              {this.state.isLoading4 === true ? (
                <View style={{alignItems: 'center'}}>
                  <ActivityIndicator size="large" />
                </View>
              ) : (
                <View style={{flex: 1}}>
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Jenis Pekerjaan
                    </Text>
                  </View>
                  <View>
                    <SearchableDropdown
                      api="jenis_pekerjaan/get_all_data_jenis_pekerjaan"
                      searchByApi={false}
                      displayData="nm_jenis_pekerjaan"
                      selectedMethod={item =>
                        this.setState({
                          nm_jenis_pekerjaan: item.nm_jenis_pekerjaan,
                          id_jenis_pekerjaan: item.id_jenis_pekerjaan,
                        })
                      }
                      placeholder="Pilih Peralatan..."
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>
                      Uraian Detail Pekerjaan
                    </Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.uraian_detail_pekerjaan}
                      placeholder="Input Uraian Detail Pekerjaan..."
                      onChangeText={text =>
                        this.setState({uraian_detail_pekerjaan: text})
                      }
                    />
                  </View>
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Uraian Hasil Pekerjaan
                    </Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.uraian_hasil_pekerjaan}
                      placeholder="Input Uraian Detail Pekerjaan..."
                      onChangeText={text =>
                        this.setState({uraian_hasil_pekerjaan: text})
                      }
                    />
                  </View>
                  <View>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <View style={{flex: 1, marginRight: 5}}>
                        <View>
                          <Text style={[{marginTop: 10}, styles.fontLabel]}>
                            Dokumentasi 1
                          </Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            borderColor: colors.greenpln,
                            borderWidth: 1,
                            borderRadius: 5,
                          }}>
                          <View style={{backgroundColor: colors.greenpln}}>
                            <View style={{marginLeft: 10, padding: 5}}>
                              <Text
                                style={{fontWeight: 'bold', color: 'white'}}>
                                Tambah File
                              </Text>
                            </View>
                          </View>
                          <View>
                            <View style={{height: '100%', paddingVertical: 25}}>
                              <Button
                                onPress={this.toggleModal}
                                style={{justifyContent: 'center'}}
                                transparent>
                                {this.state.isOpenImage === true ? (
                                  <Image
                                    source={{
                                      uri: this.state.list_image[0].uri,
                                    }}
                                    style={{
                                      height: 90,
                                      width: 130,
                                      resizeMode: 'contain',
                                    }}
                                  />
                                ) : (
                                  <Image
                                    source={require('../../../res/images/folder.png')}
                                    style={{
                                      height: 90,
                                      width: 130,
                                      resizeMode: 'contain',
                                    }}
                                  />
                                )}
                              </Button>
                            </View>
                          </View>
                        </View>
                        <Textarea
                          style={styles.textArea}
                          rowSpan={1.7}
                          bordered
                          placeholderTextColor={colors.gray02}
                          value={this.state.penjelasan_dokumentasi_satu}
                          placeholder="Penjelasan Dokumentasi 1"
                          onChangeText={text =>
                            this.setState({penjelasan_dokumentasi_satu: text})
                          }
                        />
                      </View>

                      <View style={{flex: 1, marginLeft: 5}}>
                        <View>
                          <Text style={[{marginTop: 10}, styles.fontLabel]}>
                            Dokumentasi 2
                          </Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            borderColor: colors.greenpln,
                            borderWidth: 1,
                            borderRadius: 5,
                          }}>
                          <View style={{backgroundColor: colors.greenpln}}>
                            <View style={{marginLeft: 10, padding: 5}}>
                              <Text
                                style={{fontWeight: 'bold', color: 'white'}}>
                                Tambah File
                              </Text>
                            </View>
                          </View>
                          <View>
                            <View style={{height: '100%', paddingVertical: 25}}>
                              <Button
                                onPress={this.toggleModal2}
                                style={{justifyContent: 'center'}}
                                transparent>
                                {this.state.isOpenImage2 === true ? (
                                  <Image
                                    source={{
                                      uri: this.state.list_image2[0].uri,
                                    }}
                                    style={{
                                      height: 90,
                                      width: 130,
                                      resizeMode: 'contain',
                                    }}
                                  />
                                ) : (
                                  <Image
                                    source={require('../../../res/images/folder.png')}
                                    style={{
                                      height: 90,
                                      width: 130,
                                      resizeMode: 'contain',
                                    }}
                                  />
                                )}
                              </Button>
                            </View>
                          </View>
                        </View>
                        <Textarea
                          style={styles.textArea}
                          rowSpan={1.7}
                          bordered
                          placeholderTextColor={colors.gray02}
                          value={this.state.penjelasan_dokumentasi_dua}
                          placeholder="Penjelasan Dokumentasi 2"
                          onChangeText={text =>
                            this.setState({penjelasan_dokumentasi_dua: text})
                          }
                        />
                      </View>
                    </View>
                  </View>
                  <View style={{flex: 1, marginLeft: 5}}>
                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Dokumentasi Pekerjaan 3
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        borderColor: colors.greenpln,
                        borderWidth: 1,
                        borderRadius: 5,
                      }}>
                      <View style={{backgroundColor: colors.greenpln}}>
                        <View style={{marginLeft: 10, padding: 5}}>
                          <Text style={{fontWeight: 'bold', color: 'white'}}>
                            Tambah File
                          </Text>
                        </View>
                      </View>
                      <View>
                        <View style={{height: '100%', paddingVertical: 25}}>
                          <Button
                            onPress={this.toggleModal3}
                            style={{justifyContent: 'center'}}
                            transparent>
                            {this.state.isOpenImage3 === true ? (
                              <Image
                                source={{
                                  uri: this.state.list_image3[0].uri,
                                }}
                                style={{
                                  height: 90,
                                  width: 130,
                                  resizeMode: 'contain',
                                }}
                              />
                            ) : (
                              <Image
                                source={require('../../../res/images/folder.png')}
                                style={{
                                  height: 90,
                                  width: 130,
                                  resizeMode: 'contain',
                                }}
                              />
                            )}
                          </Button>
                        </View>
                      </View>
                    </View>
                    <Textarea
                      style={styles.textArea}
                      rowSpan={1.7}
                      bordered
                      placeholderTextColor={colors.gray02}
                      value={this.state.penjelasan_dokumentasi_tiga}
                      placeholder="Penjelasan Dokumentasi 3"
                      onChangeText={text =>
                        this.setState({penjelasan_dokumentasi_tiga: text})
                      }
                    />
                  </View>

                  {/* modal image 1*/}
                  <Modal
                    style={{justifyContent: 'flex-end'}}
                    backdropOpacity={0.3}
                    animationIn={'slideInUp'}
                    animationOut={'slideOutDown'}
                    isVisible={this.state.isModalVisible}
                    onBackdropPress={this.toggleModal}
                    onBackButtonPress={this.toggleModal}>
                    <View style={{marginBottom: 10}}>
                      <View
                        style={{
                          borderRadius: 5,
                          flexDirection: 'row',
                          paddingHorizontal: 30,
                          paddingVertical: 40,
                          backgroundColor: 'white',
                        }}>
                        <View style={{marginRight: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingleWithCamera(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/camera.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Take a Photo
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                        <View style={{marginLeft: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingle(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/galery.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Select from Galery
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                      </View>
                    </View>

                    <View style={{borderRadius: 5, marginBottom: 10}}>
                      <Button
                        onPress={this.toggleModal}
                        style={{backgroundColor: 'white'}}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{
                              color: colors.greenpln,
                              textAlign: 'center',
                            }}>
                            Cancel
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </Modal>

                  {/* modal image 2*/}
                  <Modal
                    style={{justifyContent: 'flex-end'}}
                    backdropOpacity={0.3}
                    animationIn={'slideInUp'}
                    animationOut={'slideOutDown'}
                    isVisible={this.state.isModalVisible2}
                    onBackdropPress={this.toggleModal2}
                    onBackButtonPress={this.toggleModal2}>
                    <View style={{marginBottom: 10}}>
                      <View
                        style={{
                          borderRadius: 5,
                          flexDirection: 'row',
                          paddingHorizontal: 30,
                          paddingVertical: 40,
                          backgroundColor: 'white',
                        }}>
                        <View style={{marginRight: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingleWithCamera2(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/camera.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Take a Photo
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                        <View style={{marginLeft: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingle2(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/galery.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Select from Galery
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                      </View>
                    </View>

                    <View style={{borderRadius: 5, marginBottom: 10}}>
                      <Button
                        onPress={this.toggleModal2}
                        style={{backgroundColor: 'white'}}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{
                              color: colors.greenpln,
                              textAlign: 'center',
                            }}>
                            Cancel
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </Modal>

                  {/* modal image 3*/}
                  <Modal
                    style={{justifyContent: 'flex-end'}}
                    backdropOpacity={0.3}
                    animationIn={'slideInUp'}
                    animationOut={'slideOutDown'}
                    isVisible={this.state.isModalVisible3}
                    onBackdropPress={this.toggleModal3}
                    onBackButtonPress={this.toggleModal3}>
                    <View style={{marginBottom: 10}}>
                      <View
                        style={{
                          borderRadius: 5,
                          flexDirection: 'row',
                          paddingHorizontal: 30,
                          paddingVertical: 40,
                          backgroundColor: 'white',
                        }}>
                        <View style={{marginRight: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingleWithCamera3(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/camera.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Take a Photo
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                        <View style={{marginLeft: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingle3(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/galery.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Select from Galery
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                      </View>
                    </View>

                    <View style={{borderRadius: 5, marginBottom: 10}}>
                      <Button
                        onPress={this.toggleModal3}
                        style={{backgroundColor: 'white'}}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{
                              color: colors.greenpln,
                              textAlign: 'center',
                            }}>
                            Cancel
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </Modal>

                  <View style={{marginTop: 40}}>
                    <View style={{flexDirection: 'row'}}>
                      {this.state.isLoadingSave === true ? (
                        <View style={{flex: 1, alignItems: 'center'}}>
                          <ActivityIndicator />
                        </View>
                      ) : (
                        <Button
                          onPress={() => this.onKirimData()}
                          style={{
                            flex: 1,
                            marginLeft: 5,
                            backgroundColor: colors.green01,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Kirim
                            </Text>
                          </View>
                        </Button>
                      )}
                    </View>
                  </View>
                </View>
              )}
            </CardItem>
          )}
        </Content>
      </Container>
    );
  }
}
const ListPrioritas = [
  {NAMA_LIST: 'Pending', ID_OPTIONS_LIST: '1'},
  {NAMA_LIST: 'On Progress', ID_OPTIONS_LIST: '2'},
  {NAMA_LIST: 'Done', ID_OPTIONS_LIST: '3'},
  {NAMA_LIST: 'Cancel', ID_OPTIONS_LIST: '4'},
];

const JenisBeritaAcara = [
  {NAMA_LIST: 'Pekerjaan Umum', id_lkso: ''},
  {NAMA_LIST: 'LKS Online', id_lkso: '1'},
];

const LokasiPekerjaan = [
  {NAMA_LIST: 'GI (GARDU INDUK)', ID_OPTIONS_LIST: '1'},
  {NAMA_LIST: 'SITE UPT', ID_OPTIONS_LIST: '2'},
  {NAMA_LIST: 'SITE ULTG GRESIK', ID_OPTIONS_LIST: '3'},
  {NAMA_LIST: 'SITE ULTG SAMPANG', ID_OPTIONS_LIST: '4'},
  {NAMA_LIST: 'GUDANG TANDES', ID_OPTIONS_LIST: '5'},
];

const Wilayah = [
  {NAMA_LIST: 'Gresik', ID_OPTIONS_LIST: '1'},
  {NAMA_LIST: 'Sampang', ID_OPTIONS_LIST: '2'},
];

const Bidang = [
  {NAMA_LIST: 'Gardu Induk', ID_OPTIONS_LIST: '1'},
  {NAMA_LIST: 'Hargi', ID_OPTIONS_LIST: '2'},
];

const labels = ['Realisasi', 'Data BA', 'GI & BAY', 'Detail Pekerjaan'];
