/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Platform} from 'react-native';
import {
  Container,
  Header,
  Left,
  Button,
  Content,
  CardItem,
  Text,
  Textarea,
} from 'native-base';
import DatePicker from 'react-native-datepicker';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from '../../../res/styles/Form';
import colors from '../../../res/colors/index';

export default class InputApprovel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
  }

  render() {
    return (
      <Container>
        <Header
          transparent
          style={{
            marginTop: Platform.OS === 'ios' ? 0 : 0,
            borderBottomWidth: 0,
            backgroundColor: colors.greenpln,
          }}>
          <View
            style={{
              flex: 2,
              justifyContent: 'center',
            }}>
            <Button
              onPress={() => this.props.navigation.goBack()}
              transparent
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <Icon
                name="md-arrow-round-back"
                size={23}
                style={{color: 'white'}}
              />
              <Text
                uppercase={false}
                style={{
                  flex: 1,
                  textAlignVertical: 'center',
                  fontWeight: 'bold',
                  fontSize: 20,
                  color: 'white',
                }}>
                Input Aprrovel
              </Text>
            </Button>
          </View>
          <View style={{flex: 1}}/>
        </Header>
        <Content>
          <CardItem>
            <View style={{flex: 1}}>
              <View>
                <Text style={[styles.fontLabel]}>Tanggal Rencana</Text>
              </View>
              <View>
                <DatePicker
                  style={{width: '100%', height: 50}}
                  date={this.state.REPORT_DATE}
                  mode="date"
                  placeholder="Select Date"
                  format="YYYY-MM-DD"
                  minDate={this.state.minDate}
                  maxDate={this.state.maxDate}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={styles.datePicker}
                  onDateChange={date => {
                    this.setState({REPORT_DATE: date});
                  }}
                />
              </View>
              <View>
                <Text style={[{marginTop: 10}, styles.fontLabel]}>
                  Uraian Rencana Tindak Lanjut
                </Text>
              </View>
              <View>
                <Textarea
                  style={styles.textArea}
                  rowSpan={3}
                  bordered
                  placeholderTextColor={colors.gray02}
                  value={this.state.CATATAN}
                  placeholder="Tulis Keterangan Tambahan"
                  onChangeText={text => this.setState({CATATAN: text})}
                />
              </View>
              <View style={{marginTop: 10}}>
                <Button
                  onPress={() => this.movePagea()}
                  style={{
                    flex: 1,
                    backgroundColor: colors.green01,
                  }}>
                  <View style={{flex: 1}}>
                    <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
                      Kirim
                    </Text>
                  </View>
                </Button>
              </View>
            </View>
          </CardItem>
        </Content>
      </Container>
    );
  }
}
