/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Image,
  StatusBar,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Alert,
  BackHandler,
  Platform,
  DeviceEventEmitter,
  PushNotificationIOS,
  ImageBackground,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {
  View,
  Text,
  Container,
  Header,
  Form,
  Item,
  Label,
  Input,
  Footer,
  Left,
  Right,
  Button,
  Body,
  Title,
  Card,
  CheckBox,
  Content,
} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/Ionicons';
import colors from '../../res/colors';
import SwipeChart from './DashboardItem/SwipeChart';
import DataUpt from './DashboardItem/DataUpt';
import BukuPintar from './DashboardItem/BukuPintar';
import Profile from './DashboardItem/Profile';
import ListButtonDashboard from './DashboardItem/ListButtonDashboard';
import ItemDataMasuk from './ButtonDashboardItem/itemDataMasuk';
import ExitModal from './DashboardItem/exitModal';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default class HomeDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isExitVisible: false,
      chartOptions: {
        series: [
          {
            data: [1, 2, 3],
          },
        ],
      },
    };
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      'didFocus',

      payload => {
        console.log('metu');
        this.backHandler = BackHandler.addEventListener(
          'hardwareBackPress',
          this.handleBackPress,
        );
        this.setState({registerToken: ''});
      },
    );

    const didBlurSubscription = this.props.navigation.addListener(
      'didBlur',
      payload => {
        //   console.log("masuk blur")
        this.backHandler.remove();
      },
    );
  }

  handleBackPress = () => {
    this.exitApp(); // works best when the goBack is async
    return true;
  };

  exitApp() {
    Alert.alert(
      'Confirmation',
      'Exit UPT ON HAND?',
      [
        {
          text: 'No',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Yes', onPress: () => BackHandler.exitApp()},
      ],
      {cancelable: false},
    );
  }

  LogOutAlert() {
    Alert.alert(
      'Confirmation',
      'Logout Akun Ini?',
      [
        {
          text: 'No',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Yes', onPress: () => this.onLogout()},
      ],
      {cancelable: false},
    );
  }

  onLogout() {
    this.props.navigation.navigate('Login');
    AsyncStorage.setItem('username', '');
    AsyncStorage.setItem('password', '');
  }

  ExitModal = () => {
    this.setState({isExitVisible: !this.state.isExitVisible});
  };

  render() {
    return (
      <Container style={{flex: 1, display: 'flex'}}>
        <LinearGradient
          // source={require('../../res/images/BGdashboard.jpg')}
          colors={[colors.greenpln, 'white']}
          style={{
            flex: 1,
          }}>
          <Header
            transparent
            style={{
              marginTop: Platform.OS === 'ios' ? 0 : 0,
              borderBottomWidth: 0,
            }}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'center',
              }}>
              <View style={{flex: 6, justifyContent: 'center'}}>
                <Text
                  style={{
                    flex: 1,
                    marginLeft: wp('3%'),
                    textAlignVertical: 'center',
                    fontWeight: 'bold',
                    fontSize: wp('6%'),
                    color: 'white',
                  }}>
                  Dashboard
                </Text>
              </View>
              {/* <View style={{flex: 1}}>
                <Button style={{flex: 1, justifyContent: 'center'}} transparent>
                  <Icon
                    name="notifications"
                    size={30}
                    style={{color: 'white'}}
                  />
                </Button>
              </View> */}
              <View style={{flex: 1, marginRight: 5}}>
                <Button
                  onPress={() => this.LogOutAlert()}
                  style={{flex: 1, justifyContent: 'center'}}
                  transparent>
                  <Icon2 name="md-exit" size={hp('3.9%')} style={{color: 'white'}} />
                </Button>
              </View>
            </View>
          </Header>

          <Content style={{paddingHorizontal: wp('5%')}}>
            <Profile />
            <ScrollView horizontal={true}>
              <ListButtonDashboard navigation={this.props.navigation} />
            </ScrollView>

            <BukuPintar />
            <View style={{height: 100}} />
          </Content>

          <Modal
            style={{justifyContent: 'flex-end'}}
            animationIn={'slideInUp'}
            isVisible={this.state.isExitVisible}
            onBackdropPress={this.ExitModal}
            backdropOpacity={0.2}
            animationOut={'slideInUp'}
            animationInTiming={1000}>
            <View style={{marginTop: 20}}>
              <TouchableOpacity
                onPress={() => BackHandler.exitApp()}
                style={{
                  marginBottom: 10,
                  backgroundColor: 'rgba(255,255,255,1)',
                  borderRadius: 10,
                  padding: 15,
                }}>
                <View style={{alignItems: 'center', marginBottom: 0}}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: 20,
                      color: colors.greenpln,
                    }}>
                    Exit App
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  marginBottom: 20,
                  backgroundColor: 'rgba(255,255,255,1)',
                  borderRadius: 10,
                  padding: 15,
                }}>
                <View style={{alignItems: 'center', marginBottom: 0}}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: 20,
                      color: colors.greenpln,
                    }}>
                    Log Out
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: 'rgba(255,255,255,0.9)',
                  borderRadius: 10,
                  padding: 15,
                  marginHorizontal: 20,
                }}>
                <View style={{alignItems: 'center', marginBottom: 0}}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: 20,
                      color: colors.greenpln,
                    }}>
                    Cancel
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </Modal>
        </LinearGradient>
      </Container>
    );
  }
}
