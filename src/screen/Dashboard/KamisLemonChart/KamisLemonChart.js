/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Image,
  StatusBar,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Alert,
  BackHandler,
  Platform,
  DeviceEventEmitter,
  PushNotificationIOS,
  ImageBackground,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {
  View,
  Text,
  Container,
  Header,
  Form,
  Item,
  Label,
  Input,
  Footer,
  Left,
  Right,
  Button,
  Body,
  Title,
  Card,
  CheckBox,
  Content,
} from 'native-base';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from '../../../res/colors';
import FloatingButtonBA from '../../../library/component/FloatingButtonBA';
import LinearGradient from 'react-native-linear-gradient';
import SwipeChart from '../DashboardItem/SwipeChart';

export default class KamisLemonChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  render() {
    return (
      <Container style={{flex: 1, display: 'flex'}}>
        <LinearGradient
          // source={require('../../res/images/BGdashboard.jpg')}
          colors={[colors.greenpln, 'white']}
          style={{
            flex: 1,
          }}>
          <Header
            transparent
            style={{
              marginTop: Platform.OS === 'ios' ? 0 : 0,
              borderBottomWidth: 0,
            }}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'center',
              }}>
              <View style={{flex: 6, justifyContent: 'center'}}>
                <Text
                  style={{
                    flex: 1,
                    marginLeft: 10,
                    textAlignVertical: 'center',
                    fontWeight: 'bold',
                    fontSize: 20,
                    color: 'white',
                  }}>
                  Chart Kamis Lemon
                </Text>
              </View>
              <View style={{flex: 1, marginRight: 5}}>
                <Button style={{flex: 1, justifyContent: 'center'}} transparent>
                  <Icon name="sort" size={30} style={{color: 'white'}} />
                </Button>
              </View>
            </View>
          </Header>
          <Content style={{flex: 1, flexDirection: 'column'}}>
            <SwipeChart navigation={this.props.navigation} />
          </Content>
        </LinearGradient>
      </Container>
    );
  }
}
