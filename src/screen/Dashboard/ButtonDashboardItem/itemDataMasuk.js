/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  View,
  Dimensions,
  processColor,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  FlatList,
  Alert,
} from 'react-native';
import {Card, Button, Text} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';
import GlobalConfig from '../../../library/network/GlobalConfig';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const {width} = Dimensions.get('window');

export default class ListButtonDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingData: false,
      dataMasuk: [
        {title: 'Berita Acara', value: '8', color: '#FC766AFF'},
        {title: 'Pergantian Aset', value: '12', color: '#5B84B1FF'},
        {title: 'LKS Masuk', value: '9', color: '#00203FFF'},
        {title: 'Bank Usrek', value: '10', color: '#27ae60'},
      ],
    };
  }

  loadData() {
    this.setState({
      isLoading: true,
    });
    this.setState({isLoading: true});
    var url = GlobalConfig.SERVERHOST + 'dashboard/rekap_masuk';
    fetch(url, {
      method: 'POST',
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        this.setState(
          {
            lkso: response.lkso,
            baol: response.baol,
            usrek: response.usrek,
            kamis_lemon: response.kamis_lemon,
          },
          function() {
            console.log('Data all', this.state.lkso);
            this.setState({
              isLoading: false,
            });
          },
        );
      })
      .catch(error => {
        this.setState({isLoading: false});
        Alert.alert('Error', 'Check Your Internet Connection', [
          {
            text: 'Okay',
          },
        ]);
        console.log(error);
      });
  }

  componentDidMount() {
    this.loadData();
  }

  render() {
    return (
      <View style={{marginTop: 45}}>
        <View
          style={{
            backgroundColor: 'rgba(255,255,255,0.9)',
            borderRadius: 10,
            padding: wp('5%'),
          }}>
          <View style={{alignItems: 'center', marginBottom: 5}}>
            <Text
              style={{
                fontSize: wp('5%'),
                fontWeight: 'bold',
                color: colors.greenpln,
              }}>
              Dokumen UPT Masuk
            </Text>
          </View>
          {this.state.isLoading === true ? (
            <View style={{alignItems: 'center'}}>
              <ActivityIndicator />
            </View>
          ) : (
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 1, flexDirection: 'column'}}>
                <View
                  style={[
                    styleData.boxData,
                    {
                      backgroundColor: '#FC766AFF',
                    },
                  ]}>
                  <View style={{marginBottom: 5}}>
                    <Text style={styleData.textData}>LKS MASUK</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styleData.valueData}>{this.state.lkso}</Text>
                    <View>
                      <Icon
                        style={{marginTop: hp('1%')}}
                        name="file-text"
                        size={hp('3%')}
                        color={'white'}
                      />
                    </View>
                  </View>
                </View>
                <View
                  style={[
                    styleData.boxData,
                    {
                      backgroundColor: '#5B84B1FF',
                    },
                  ]}>
                  <View style={{marginBottom: 5}}>
                    <Text style={styleData.textData}>Berita Acara</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styleData.valueData}>{this.state.baol}</Text>
                    <View>
                      <Icon
                        style={{marginTop: hp('1%')}}
                        name="file-text"
                        size={hp('3%')}
                        color={'white'}
                      />
                    </View>
                  </View>
                </View>
              </View>
              <View style={{flex: 1, flexDirection: 'column'}}>
                <View
                  style={[
                    styleData.boxData,
                    {
                      backgroundColor: '#00203FFF',
                    },
                  ]}>
                  <View style={{marginBottom: 5}}>
                    <Text style={styleData.textData}>Usulan Kerja</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styleData.valueData}>{this.state.usrek}</Text>
                    <View>
                      <Icon
                        style={{marginTop: hp('1%')}}
                        name="file-text"
                        size={hp('3%')}
                        color={'white'}
                      />
                    </View>
                  </View>
                </View>
                <View style={[styleData.boxData, {backgroundColor: '#27ae65'}]}>
                  <View style={{marginBottom: 5}}>
                    <Text style={styleData.textData}>Kamis Lemon</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styleData.valueData}>
                      {this.state.kamis_lemon}
                    </Text>
                    <View>
                      <Icon
                        style={{marginTop: hp('1%')}}
                        name="file-text"
                        size={hp('3%')}
                        color={'white'}
                      />
                    </View>
                  </View>
                </View>
              </View>
            </View>
          )}
        </View>
      </View>
    );
  }
}

const styleData = StyleSheet.create({
  boxData: {
    alignItems: 'center',
    marginHorizontal: wp('1%'),
    marginVertical: hp('1%'),
    paddingVertical: hp('1%'),
    borderRadius: wp('2%'),
  },
  textData: {
    fontSize: wp('4%'),
    fontWeight: 'bold',
    color: 'white',
  },
  valueData: {
    color: 'white',
    fontSize: wp('8%'),
    marginRight: 10,
  },
});
