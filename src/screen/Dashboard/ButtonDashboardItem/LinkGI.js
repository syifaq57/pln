/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  View,
  Dimensions,
  processColor,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  FlatList,
  Linking,
  Alert,
} from 'react-native';
import {Card, Button, Text} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';
import GlobalConfig from '../../../library/network/GlobalConfig';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const {width} = Dimensions.get('window');

export default class LinkGI extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingData: false,
      data: [],
    };
  }

  onOpenLink(link) {
    var url = link;
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        alert('Browser tidak ditemukan!');
        console.log("Don't know how to open URI: " + url);
      }
    });
  }

  loadData() {
    this.setState({
      isLoading: true,
    });
    this.setState({isLoading: true});
    var url = GlobalConfig.SERVERHOST + 'gardu_induk/get_all_data_gardu_induk';
    fetch(url, {
      method: 'POST',
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.status === 'success') {
          this.setState(
            {
              data: response.data,
            },
            function() {
              console.log('Data all', this.state.data);
              this.setState({
                isLoading: false,
              });
            },
          );
        } else {
          this.setState({
            isLoading: false,
          });
          Alert('Gagal Load Data', [
            {
              text: 'Okay',
            },
          ]);
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        Alert.alert('Error', 'Check Your Internet Connection', [
          {
            text: 'Okay',
          },
        ]);
        console.log(error);
      });
  }

  componentDidMount() {
    this.loadData();
  }

  render() {
    return (
      <View style={{marginTop: 50, height: hp('50%')}}>
        <View
          style={{
            backgroundColor: 'rgba(255,255,255,0.9)',
            borderRadius: wp('2%'),
            padding: wp('5%'),
          }}>
          <View
            style={{
              alignItems: 'center',
              marginBottom: hp('0.5%'),
              padding: hp('0.5%'),
            }}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: wp('5%'),
                color: colors.greenpln,
              }}>
              LINK GARDU INDUK
            </Text>
          </View>
          <ScrollView>
            {this.state.data.map(data => (
              <Card style={{padding: hp('1%')}}>
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 5, justifyContent: 'center'}}>
                    <Text
                      style={{textAlignVertical: 'center', fontSize: wp('4%')}}>
                      {data.nm_gardu_induk}
                    </Text>
                  </View>
                  <View style={{flex: 1.6}}>
                    <TouchableOpacity
                      onPress={() => this.onOpenLink(data.link_gi)}
                      style={{
                        backgroundColor: colors.greenpln,
                        paddingVertical: hp('1%'),
                        borderRadius: 5,
                      }}>
                      <View style={{flex: 1}}>
                        <Text style={{textAlign: 'center'}}>Open</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </Card>
            ))}
          </ScrollView>
        </View>
      </View>
    );
  }
}
