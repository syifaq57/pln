/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  View,
  Dimensions,
  processColor,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  FlatList,
  Linking,
  Alert,
  AsyncStorage,
} from 'react-native';
import {Card, Button, Text} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';
import GlobalConfig from '../../../library/network/GlobalConfig';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const {width} = Dimensions.get('window');
const month = [
  'ALL',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember',
];

export default class DataApproval extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoadingData: false,
    };
  }

  loadData() {
    var date = new Date();
    var year = date.getFullYear();
    this.setState({
      isLoading: true,
    });
    this.setState({isLoading: true});

    var url = GlobalConfig.SERVERHOST + 'notifikasi';
    var formData = new FormData();
    formData.append('id_pegawai', this.state.DataUser.user.id_pegawai);
    console.log('frmdta 1', formData);
    fetch(url, {
      method: 'POST',
      body: formData,
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        this.setState({
          total: response.total,
        });
        if (response.status === 'success') {
          this.setState(
            {
              data: response.data,
            },
            function() {
              console.log('Data all', this.state.data);
              this.setState({
                isLoading: false,
              });
            },
          );
        } else {
          this.setState({
            isLoading: false,
          });
          Alert('Data Kosong', [
            {
              text: 'Okay',
            },
          ]);
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        // Alert.alert('Error', 'Check Your Internet Connection', [
        //   {
        //     text: 'Okay',
        //   },
        // ]);
        console.log(error);
      });
  }

  componentDidMount() {
    AsyncStorage.getItem('DataUser').then(value =>
      this.setState(
        {
          DataUser: JSON.parse(value),
        },
        function() {
          this.loadData();
        },
      ),
    );
  }

  navigateToScreen(route, id_baol) {
    this.props.visible();
    this.props.navigation.navigate(route, {
      id_baol: id_baol,
    });
  }

  render() {
    return (
      <View style={{marginTop: 45}}>
        <View
          style={{
            backgroundColor: 'rgba(255,255,255,0.9)',
            borderRadius: 10,
            padding: 15,
          }}>
          <View style={{alignItems: 'center', marginBottom: 5}}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: wp('4.5%'),
                color: colors.greenpln,
              }}>
              Data Approval
            </Text>
          </View>
          <View>
            <View>
              <Image
                source={require('../../../res/images/approved.png')}
                style={{
                  width: wp('15%'),
                  height: hp('10%'),
                  resizeMode: 'contain',
                  alignSelf: 'center',
                }}
              />
            </View>
            <View style={{marginVertical: 10, height: 200}}>
              <View style={{marginLeft: 10, marginBottom: 5}}>
                <Text style={{fontWeight: 'bold'}}>Berita Acara</Text>
              </View>
              <ScrollView>
                {this.state.total === 0 ? (
                  <View style={{alignItems: 'center', marginTop: 20}}>
                    <Text>- Tidak Ada BA Yang Perlu Di Setujui -</Text>
                  </View>
                ) : (
                  <View>
                    {this.state.data.map(data => (
                      <View
                        style={{
                          marginBottom: 5,
                          padding: 15,
                          backgroundColor: colors.greenpln,
                          borderRadius: 5,
                        }}>
                        <TouchableOpacity
                          style={{flexDirection: 'row'}}
                          onPress={() =>
                            this.navigateToScreen('DetailBA', data.id_baol)
                          }>
                          <View style={{flex: 6}}>
                            <Text style={{fontSize: 14, color: 'white'}}>
                              Berita Acara Nomor:
                            </Text>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                color: 'white',
                                fontStyle: 'italic',
                              }}>
                              {data.no_baol}
                            </Text>
                            <Text style={{fontSize: 14, color: 'white'}}>
                              Menunggu untuk di setujui
                            </Text>
                          </View>
                          <View style={{flex: 1, justifyContent: 'center'}}>
                            <Image
                              source={require('../../../res/images/approved.png')}
                              style={{
                                width: 40,
                                height: 40,
                                alignSelf: 'center',
                              }}
                              resizeMode={'center'}
                            />
                          </View>
                        </TouchableOpacity>
                      </View>
                    ))}
                  </View>
                )}
              </ScrollView>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
