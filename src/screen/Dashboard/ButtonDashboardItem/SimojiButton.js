/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  View,
  Dimensions,
  processColor,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  FlatList,
  Alert,
  Linking,
} from 'react-native';
import {Card, Button, Text} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';
import GlobalConfig from '../../../library/network/GlobalConfig';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const {width} = Dimensions.get('window');

export default class SimojiButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingData: false,
      dataMasuk: [
        {title: 'Berita Acara', value: '8', color: '#FC766AFF'},
        {title: 'Pergantian Aset', value: '12', color: '#5B84B1FF'},
        {title: 'LKS Masuk', value: '9', color: '#00203FFF'},
        {title: 'Bank Usrek', value: '10', color: '#27ae60'},
      ],
    };
  }

  loadData() {
    this.setState({
      isLoading: true,
    });
    this.setState({isLoading: true});
    var url = GlobalConfig.SERVERHOST + 'dashboard/rekap_masuk';
    fetch(url, {
      method: 'POST',
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        this.setState(
          {
            lkso: response.lkso,
            baol: response.baol,
            usrek: response.usrek,
            kamis_lemon: response.kamis_lemon,
          },
          function() {
            console.log('Data all', this.state.lkso);
            this.setState({
              isLoading: false,
            });
          },
        );
      })
      .catch(error => {
        this.setState({isLoading: false});
        Alert.alert('Error', 'Check Your Internet Connection', [
          {
            text: 'Okay',
          },
        ]);
        console.log(error);
      });
  }

  componentDidMount() {
    this.loadData();
  }

  render() {
    return (
      <View style={{marginTop: hp('5%')}}>
        <View
          style={{
            backgroundColor: 'rgba(255,255,255,0.9)',
            borderRadius: 10,
            padding: wp('2%'),
          }}>
          <View style={{alignItems: 'center', marginBottom: 10}}>
            <Text
              style={{
                fontSize: wp('4.5%'),
                fontWeight: 'bold',
                color: colors.greenpln,
              }}>
              Dokumen UPT Masuk
            </Text>
          </View>
          {this.state.isLoading === true ? (
            <View style={{alignItems: 'center'}}>
              <ActivityIndicator />
            </View>
          ) : (
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 1, flexDirection: 'column'}}>
                <View
                  style={[
                    styleSimoji.boxData,
                    {
                      backgroundColor: '#FC766AFF',
                    },
                  ]}>
                  <View style={{marginBottom: 10}}>
                    <Text style={styleSimoji.textData}>Alat Uji</Text>
                  </View>
                  <TouchableOpacity
                    onPress={() =>
                      Linking.openURL(
                        'https://docs.google.com/spreadsheets/d/1Zr_StSvc_68RBRdIAtjzTApmx-FuP62bnrghQEMYpsI/edit#gid=1831492504',
                      )
                    }
                    style={styleSimoji.buttonLink}>
                    <Text style={styleSimoji.textLink}>Open</Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={[
                    styleSimoji.boxData,
                    {
                      backgroundColor: '#5B84B1FF',
                    },
                  ]}>
                  <View style={{marginBottom: 10}}>
                    <Text style={styleSimoji.textData}>Simoji</Text>
                  </View>
                  <TouchableOpacity
                    onPress={() =>
                      Linking.openURL(
                        'https://sites.google.com/view/gresik-satu/input-data/simoji?authuser=0',
                      )
                    }
                    style={styleSimoji.buttonLink}>
                    <Text style={styleSimoji.textLink}>Open</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{flex: 1, flexDirection: 'column'}}>
                <View
                  style={[
                    styleSimoji.boxData,
                    {
                      backgroundColor: '#27ae65',
                    },
                  ]}>
                  <View style={{marginBottom: 10}}>
                    <Text style={styleSimoji.textData}>GIS PMT</Text>
                  </View>
                  <TouchableOpacity
                    onPress={() =>
                      Linking.openURL(
                        'https://docs.google.com/spreadsheets/d/1CSV2RrMjzakbSFeQBcpjAJZ48WjSC4tcw8i3PcIlyBY/edit#gid=0',
                      )
                    }
                    style={styleSimoji.buttonLink}>
                    <Text style={styleSimoji.textLink}>Open</Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={[
                    styleSimoji.boxData,
                    {
                      backgroundColor: '#00203FFF',
                    },
                  ]}>
                  <View style={{marginBottom: 10}}>
                    <Text style={styleSimoji.textData}>Arus Bocor</Text>
                  </View>
                  <TouchableOpacity
                    onPress={() =>
                      Linking.openURL(
                        'https://sites.google.com/view/gresik-satu/input-data/arus-bocor-kabel-power-20-kv?authuser=0',
                      )
                    }
                    style={styleSimoji.buttonLink}>
                    <Text style={styleSimoji.textLink}>Open</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}
        </View>
      </View>
    );
  }
}

const styleSimoji = StyleSheet.create({
  boxData: {
    alignItems: 'center',
    marginHorizontal: wp('1%'),
    marginVertical: hp('1%'),
    paddingVertical: hp('1%'),
    borderRadius: wp('2%'),
  },
  textData: {
    fontSize: wp('5%'),
    fontWeight: 'bold',
    color: 'white',
  },
  buttonLink: {
    flexDirection: 'row',
    paddingHorizontal: wp('5%'),
    paddingVertical: hp('0.7%'),
    backgroundColor: 'rgba(255,255,255,0.7)',
    borderRadius: hp('0.5%'),
  },
  textLink: {
    color: colors.greenpln,
    fontWeight: 'bold',
    fontSize: wp('4%'),
  },
});
