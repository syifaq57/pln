import React, {Component, Fragment} from 'react';
import {
  Image,
  StatusBar,
  KeyboardAvoidingView,
  ScrollView,
  View,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Alert,
  BackHandler,
  Platform,
  DeviceEventEmitter,
  PushNotificationIOS,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  Container,
  Header,
  Form,
  Item,
  Label,
  Input,
  Footer,
  Left,
  Right,
  Button,
  Body,
  Title,
  CheckBox,
  Content,
  Card,
  CardItem,
  Text,
  Textarea,
} from 'native-base';
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton,
} from 'react-native-popup-dialog';
import DatePicker from 'react-native-datepicker';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import SearchableDropdown from '../../../library/component/SearchableDropdown';
import styles from '../../../res/styles/Form';
import colors from '../../../res/colors/index';
import GlobalConfig from '../../../library/network/GlobalConfig';

const PIC = [
  {NAMA_LIST: 'MULTG', ID_OPTIONS_LIST: '1'},
  {NAMA_LIST: 'SPV HARGI', ID_OPTIONS_LIST: '2'},
  {NAMA_LIST: 'SPV HARJAR', ID_OPTIONS_LIST: '3'},
  {NAMA_LIST: 'SPV HARJAR', ID_OPTIONS_LIST: '4'},
];

export default class TambahKegiatan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      enabledBay: true,
      nip_pegawai: '',
      tgl_kegiatan: '',
      id_gardu_induk: '',
      gardu_induk_lainnya: '',
      id_bay: '',
      bay_lainnya: '',
      nama_kegiatan: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('DataUser').then(value =>
      this.setState(
        {
          DataUser: JSON.parse(value),
        },
        function() {
          console.log('user aa', this.state.DataUser);
        },
      ),
    );
  }

  onKirimData() {
    this.setState({
      isLoadingSave: true,
    });
    if (
      this.state.tgl_kegiatan === '' ||
      this.state.id_gardu_induk === '' ||
      this.state.id_bay === '' ||
      this.state.nama_kegiatan === ''
    ) {
      alert('Form belum lengkap');
      this.setState({
        isLoadingSave: false,
      });
    } else {
      var url = GlobalConfig.SERVERHOST + 'kegiatan/add_data_kegiatan';
      var formData = new FormData();
      formData.append('nip_pegawai', this.state.DataUser.user.nip);
      formData.append('id_gardu_induk', this.state.id_gardu_induk);
      formData.append('gardu_induk_lainnya', this.state.gardu_induk_lainnya);
      formData.append('id_bay', this.state.id_bay);
      formData.append('bay_lainnya', this.state.bay_lainnya);
      formData.append('tgl_kegiatan', this.state.tgl_kegiatan);
      formData.append('nama_kegiatan', this.state.nama_kegiatan);

      console.log('all FD', formData);
      fetch(url, {
        method: 'POST',
        body: formData,
      })
        .then(response => response.json())
        .then(response => {
          console.log('', response);
          if (response.status === 'success') {
            this.setState(
              {
                isLoadingSave: false,
              },
              function() {
                Alert.alert('Data Saved', response.message, [
                  {
                    text: 'Okay',
                  },
                ]);
                AsyncStorage.setItem('SavedRencana', '1');
                this.props.navigation.goBack();
              },
            );
          } else {
            this.setState({
              isLoadingSave: false,
            });
            Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [
              {
                text: 'Okay',
              },
            ]);
          }
        })
        .catch(error => {
          this.setState({
            isLoadingSave: false,
          });
          Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [
            {
              text: 'Okay',
            },
          ]);
          console.log(error);
        });
    }
  }

  render() {
    return (
      <Container>
        <Header
          transparent
          style={{
            marginTop: Platform.OS === 'ios' ? 0 : 0,
            borderBottomWidth: 0,
            backgroundColor: colors.greenpln,
          }}>
          <View
            style={{
              flex: 2,
              justifyContent: 'center',
            }}>
            <Button
              onPress={() => this.props.navigation.goBack()}
              transparent
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <Icon
                name="md-arrow-round-back"
                size={23}
                style={{color: 'white'}}
              />
              <Text
                uppercase={false}
                style={{
                  flex: 1,
                  textAlignVertical: 'center',
                  fontWeight: 'bold',
                  fontSize: 20,
                  color: 'white',
                }}>
                Tambah Kegiatan
              </Text>
            </Button>
          </View>
          <View style={{flex: 1}}/>
        </Header>
        <Content>
          <CardItem>
            {this.state.isLoading ? (
              <View style={{alignItems: 'center'}}>
                <ActivityIndicator />
              </View>
            ) : (
              <View style={{flex: 1}}>
                <View>
                  <Text style={[styles.fontLabel]}>Tanggal Kegiatan</Text>
                </View>
                <View>
                  <DatePicker
                    style={{width: '100%', height: 50}}
                    date={this.state.tgl_kegiatan}
                    mode="date"
                    placeholder="Select Date"
                    format="YYYY-MM-DD"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={styles.datePicker}
                    onDateChange={date => {
                      this.setState({tgl_kegiatan: date});
                    }}
                  />
                </View>
                <View>
                  <Text style={[{marginTop: 10}, styles.fontLabel]}>
                    Gardu Induk
                  </Text>
                </View>
                <View>
                  <SearchableDropdown
                    api="gardu_induk/get_all_data_gardu_induk"
                    searchByApi={false}
                    displayData="nm_gardu_induk"
                    otherList={{nm_gardu_induk: 'Lainnya', id_gardu_induk: '0'}}
                    selectedMethod={item =>
                      this.setState(
                        {
                          nm_gardu_induk: item.nm_gardu_induk,
                          id_gardu_induk: item.id_gardu_induk,
                          isLoadingBay: true,
                          enabledBay: false,
                        },
                        function() {
                          this.setState({
                            isLoadingBay: false,
                          });
                        },
                      )
                    }
                    selected={this.state.nm_gardu_induk}
                    placeholder="Pilih Gardu Induk..."
                  />
                </View>
                {this.state.id_gardu_induk === '0' ? (
                  <View>
                    <View style={{marginTop: 10}}>
                      <Text style={styles.fontLabel}>Gardu Induk Lain</Text>
                    </View>
                    <View>
                      <TextInput
                        style={styles.fontTextInput}
                        rowSpan={1}
                        editable={true}
                        bordered
                        value={this.state.gardu_induk_lainnya}
                        placeholder="Input GI Lain..."
                        onChangeText={text =>
                          this.setState({gardu_induk_lainnya: text})
                        }
                      />
                    </View>
                  </View>
                ) : (
                  <View />
                )}

                <View>
                  <Text style={[{marginTop: 10}, styles.fontLabel]}>BAY</Text>
                </View>
                <View>
                  {!this.state.isLoadingBay ? (
                    <SearchableDropdown
                      api="bay/bay_gardu_induk"
                      searchByApi={false}
                      bodyApi={[
                        {
                          name: 'id_gardu_induk',
                          value: this.state.id_gardu_induk,
                        },
                      ]}
                      displayData="nm_bay"
                      otherList={{nm_bay: 'Lainnya', id_bay: '0'}}
                      selectedMethod={item =>
                        this.setState({
                          nm_bay: item.nm_bay,
                          id_bay: item.id_bay,
                        })
                      }
                      disabled={this.state.enabledBay}
                      placeholder="Pilih BAY..."
                    />
                  ) : (
                    <View style={{alignItems: 'center'}}>
                      <ActivityIndicator />
                    </View>
                  )}
                </View>

                {this.state.id_bay === '0' ? (
                  <View>
                    <View style={{marginTop: 10}}>
                      <Text style={styles.fontLabel}>BAY Lain</Text>
                    </View>
                    <View>
                      <TextInput
                        style={styles.fontTextInput}
                        rowSpan={1}
                        editable={true}
                        bordered
                        value={this.state.bay_lainnya}
                        placeholder="Input BAY Lain..."
                        onChangeText={text =>
                          this.setState({bay_lainnya: text})
                        }
                      />
                    </View>
                  </View>
                ) : (
                  <View />
                )}

                <View>
                  <Text style={[{marginTop: 10}, styles.fontLabel]}>
                    Nama Kegiatan
                  </Text>
                </View>
                <View>
                  <Textarea
                    style={styles.textArea}
                    rowSpan={3}
                    bordered
                    placeholderTextColor={colors.gray02}
                    value={this.state.nama_kegiatan}
                    placeholder="Input Nama Kegiatan"
                    onChangeText={text => this.setState({nama_kegiatan: text})}
                  />
                </View>

                <View style={{marginTop: 20}}>
                  {this.state.isLoadingSave === true ? (
                    <View style={{flex: 1, alignItems: 'center'}}>
                      <ActivityIndicator />
                    </View>
                  ) : (
                    <Button
                      onPress={() => this.onKirimData()}
                      style={{
                        flex: 1,
                        marginLeft: 5,
                        backgroundColor: colors.green01,
                      }}>
                      <View style={{flex: 1}}>
                        <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
                          Simpan Kegiatan
                        </Text>
                      </View>
                    </Button>
                  )}
                </View>
              </View>
            )}
          </CardItem>
        </Content>
      </Container>
    );
  }
}
