/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  View,
  Dimensions,
  processColor,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  FlatList,
  Linking,
  Alert,
} from 'react-native';
import {Card, Button, Text} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';
import GlobalConfig from '../../../library/network/GlobalConfig';
import Modal from 'react-native-modal';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const {width} = Dimensions.get('window');
const month = [
  'ALL',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember',
];

export default class Kegiatan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoadingData: false,
      dataMasuk: [
        {title: 'Berita Acara', value: '8', color: '#FC766AFF'},
        {title: 'Pergantian Aset', value: '12', color: '#5B84B1FF'},
        {title: 'LKS Masuk', value: '9', color: '#00203FFF'},
        {title: 'Bank Usrek', value: '10', color: '#27ae60'},
      ],
    };
  }

  loadData() {
    var date = new Date();
    var year = date.getFullYear();
    this.setState({
      isLoading: true,
    });
    this.setState({isLoading: true});
    var url = GlobalConfig.SERVERHOST + 'kegiatan/get_data_kegiatan';
    fetch(url, {
      method: 'POST',
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.status === 'success') {
          this.setState(
            {
              data: response.data,
            },
            function() {
              console.log('Data all', this.state.data);
              this.setState({
                isLoading: false,
              });
            },
          );
        } else {
          this.setState({
            isLoading: false,
          });
          Alert('Gagal Load Data', [
            {
              text: 'Okay',
            },
          ]);
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        Alert.alert('Error', 'Check Your Internet Connection', [
          {
            text: 'Okay',
          },
        ]);
        console.log(error);
      });
  }

  componentDidMount() {
    this.loadData();
  }

  onOpenLink() {
    var url =
      'https://docs.google.com/spreadsheets/d/1gwWfUtXXjvnibRS3mHov2dUoaTEkA3U4PuBV7qkJkvM/edit#gid=297762209';
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        alert('Browser tidak ditemukan!');
        console.log("Don't know how to open URI: " + url);
      }
    });
  }
  onOpenBulan(name) {
    var url = GlobalConfig.base_url + 'assets/dokumen-kinerja/' + name;
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        alert('Browser tidak ditemukan!');
        console.log("Don't know how to open URI: " + url);
      }
    });
  }

  render() {
    return (
      <View style={{marginVertical: hp('2%'), height: hp('50%')}}>
        <View style={{marginLeft: wp('2%'), marginBottom: 5}}>
          <Text style={{fontSize: wp('4.5%')}}>List Kegiatan</Text>
        </View>
        <ScrollView>
          {this.state.data.map((data, index) => (
            <View
              key={index}
              style={{
                marginBottom: hp('1%'),
                flexDirection: 'row',
                backgroundColor: colors.greenpln,
                borderRadius: wp('1%'),
              }}>
              <View
                style={{flex: 6, padding: wp('2%')}}
                onPress={() => this.onOpenBulan(data.file_kinerja)}>
                <Text
                  style={{
                    fontSize: wp('4.5%'),
                    color: 'white',
                  }}>
                  {data.title}
                </Text>
                <Text
                  style={{
                    marginLeft: wp('1%'),
                    fontSize: wp('4%'),
                    color: 'white',
                  }}>
                  {data.start}
                </Text>
              </View>
              <TouchableOpacity style={{flex: 1, justifyContent: 'center'}}>
                <Icon name="calendar" size={hp('4%')} color={'white'} />
              </TouchableOpacity>
            </View>
          ))}
        </ScrollView>
      </View>
    );
  }
}
