/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  View,
  Dimensions,
  processColor,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  FlatList,
  Linking,
  Alert,
} from 'react-native';
import {Card, Button, Text} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';
import GlobalConfig from '../../../library/network/GlobalConfig';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const {width} = Dimensions.get('window');
const month = [
  'ALL',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember',
];

export default class Kinerja extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoadingData: false,
    };
  }

  loadData() {
    var date = new Date();
    var year = date.getFullYear();
    this.setState({
      isLoading: true,
    });
    this.setState({isLoading: true});
    var url =
      GlobalConfig.SERVERHOST + 'dokumen_kinerja/detail_data_dokumen_kinerja';
    var formData = new FormData();
    formData.append('tahun', year);
    console.log('frmdta', formData);
    fetch(url, {
      method: 'POST',
      body: formData,
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.status === 'success') {
          this.setState(
            {
              data: response.data,
            },
            function() {
              console.log('Data all', this.state.data);
              this.setState({
                isLoading: false,
              });
            },
          );
        } else {
          this.setState({
            isLoading: false,
          });
          Alert('Gagal Load Data', [
            {
              text: 'Okay',
            },
          ]);
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        Alert.alert('Error', 'Check Your Internet Connection', [
          {
            text: 'Okay',
          },
        ]);
        console.log(error);
      });
  }

  componentDidMount() {
    this.loadData();
  }

  onOpenLink() {
    var url =
      'https://docs.google.com/spreadsheets/d/1gwWfUtXXjvnibRS3mHov2dUoaTEkA3U4PuBV7qkJkvM/edit#gid=297762209';
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        alert('Browser tidak ditemukan!');
        console.log("Don't know how to open URI: " + url);
      }
    });
  }
  onOpenBulan(name) {
    var url = GlobalConfig.base_url + 'assets/dokumen-kinerja/' + name;
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        alert('Browser tidak ditemukan!');
        console.log("Don't know how to open URI: " + url);
      }
    });
  }

  render() {
    return (
      <View style={{marginTop: hp('5%')}}>
        <View
          style={{
            backgroundColor: 'rgba(255,255,255,0.9)',
            borderRadius: wp('2%'),
            padding: hp('1.5%'),
          }}>
          <View style={{alignItems: 'center', marginBottom: 5}}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: wp('5%'),
                color: colors.greenpln,
              }}>
              Pencapaian & Kinerja
            </Text>
          </View>
          <View style={{marginTop: hp('1%')}}>
            <View style={{marginBottom: hp('1%')}}>
              <Image
                source={require('../../../res/images/performance.png')}
                style={{
                  width: wp('20%'),
                  height: hp('10%'),
                  resizeMode: 'contain',
                  alignSelf: 'center',
                }}
              />
            </View>
            <Button
              onPress={() => this.onOpenLink()}
              style={{backgroundColor: colors.greenpln}}>
              <Text
                style={{
                  flex: 1,
                  fontSize: wp('4.5%'),
                  textAlign: 'center',
                  fontWeight: 'bold',
                }}>
                Lihat Detail
              </Text>
            </Button>
            <View style={{marginVertical: hp('1%'), height: hp('50%')}}>
              <View style={{marginLeft: 10, marginBottom: 5}}>
                <Text style={{fontSize: wp('4.5%')}}>Download Laporan</Text>
              </View>
              <ScrollView>
                {this.state.data.map(data => (
                  <View style={{marginBottom: hp('0.7%')}}>
                    <Button
                      style={{backgroundColor: colors.greenpln}}
                      onPress={() => this.onOpenBulan(data.file_kinerja)}>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          fontSize: wp('4.5%'),
                          fontStyle: 'italic',
                        }}>
                        - Bulan {month[data.bulan]}
                      </Text>
                    </Button>
                  </View>
                ))}
              </ScrollView>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
