import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  Text,
  View,
  Dimensions,
  processColor,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {Card} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';
export default class ChartKit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingData: false,
      datachart: [
        {title: 'Berita Acara', value: '8', color: '#FC766AFF'},
        {title: 'Pergantian Aset', value: '12', color: '#5B84B1FF'},
        {title: 'LKS Masuk', value: '9', color: '#00203FFF'},
        {title: 'Bank Usrek', value: '10', color: '#27ae60'},
      ],
    };
  }

  render() {
    return (
      <View>
        {this.state.isLoadingData === true ? (
          <View style={{alignItems: 'center', marginTop: 15}}>
            <ActivityIndicator size="large" />
          </View>
        ) : (
          <View style={{}}>
            <View style={{flexDirection: 'row', marginHorizontal: 10}}>
              <Card
                style={{
                  flex: 1,
                  width: 180,
                  borderRadius: 5,
                  borderColor: 'rgba(243,151,70,0.9)',
                  backgroundColor: 'rgba(243,151,70,0.9)',
                }}>
                <View style={{padding: 15, alignItems: 'center'}}>
                  <View style={{marginBottom: 5}}>
                    <Text
                      style={{
                        fontSize: 15,
                        fontWeight: 'bold',
                        color: 'white',
                      }}>
                      LKS Masuk
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row', flex: 1}}>
                    <Text
                      style={{
                        color: 'white',
                        fontWeight: 'bold',
                        fontSize: 30,
                        marginRight: 10,
                      }}>
                      13
                    </Text>
                    <View>
                      <Icon
                        style={{marginTop: 7}}
                        name="file-text"
                        size={23}
                        color={'white'}
                      />
                    </View>
                  </View>
                </View>
              </Card>
              <Card
                style={{
                  flex: 1,
                  width: 180,
                  borderRadius: 5,
                  borderColor: 'rgba(62,109,238,0.9)',
                  backgroundColor: 'rgba(62,109,238,0.9)',
                }}>
                <View style={{padding: 15, alignItems: 'center'}}>
                  <View style={{marginBottom: 5}}>
                    <Text
                      style={{
                        fontSize: 15,
                        fontWeight: 'bold',
                        color: 'white',
                      }}>
                      Berita Acara
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row', flex: 1}}>
                    <Text
                      style={{
                        color: 'white',
                        fontWeight: 'bold',
                        fontSize: 30,
                        marginRight: 10,
                      }}>
                      13
                    </Text>
                    <View>
                      <Icon
                        style={{marginTop: 7}}
                        name="file-text"
                        size={23}
                        color={'white'}
                      />
                    </View>
                  </View>
                </View>
              </Card>
              <Card
                style={{
                  flex: 1,
                  width: 180,
                  borderRadius: 5,
                  borderColor: 'rgba(238,62,74,0.9)',
                  backgroundColor: 'rgba(238,62,74,0.9)',
                }}>
                <View style={{padding: 15, alignItems: 'center'}}>
                  <View style={{marginBottom: 5}}>
                    <Text
                      style={{
                        fontSize: 15,
                        fontWeight: 'bold',
                        color: 'white',
                      }}>
                      Bank Usrek
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row', flex: 1}}>
                    <Text
                      style={{
                        color: 'white',
                        fontWeight: 'bold',
                        fontSize: 30,
                        marginRight: 10,
                      }}>
                      13
                    </Text>
                    <View>
                      <Icon
                        style={{marginTop: 7}}
                        name="file-text"
                        size={23}
                        color={'white'}
                      />
                    </View>
                  </View>
                </View>
              </Card>
              <Card
                style={{
                  flex: 1,
                  width: 180,
                  borderRadius: 5,
                  borderColor: 'rgba(57,219,138,0.9)',
                  backgroundColor: 'rgba(57,219,138,0.9)',
                }}>
                <View style={{padding: 15, alignItems: 'center'}}>
                  <View style={{marginBottom: 5}}>
                    <Text
                      style={{
                        fontSize: 15,
                        fontWeight: 'bold',
                        color: 'white',
                      }}>
                      BA Pergantian Aset
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row', flex: 1}}>
                    <Text
                      style={{
                        color: 'white',
                        fontWeight: 'bold',
                        fontSize: 30,
                        marginRight: 10,
                      }}>
                      13
                    </Text>
                    <View>
                      <Icon
                        style={{marginTop: 7}}
                        name="file-text"
                        size={23}
                        color={'white'}
                      />
                    </View>
                  </View>
                </View>
              </Card>
            </View>
            <View style={{flexDirection: 'row', marginHorizontal: 10}}>
              
            </View>
          </View>
        )}
      </View>
    );
  }
}
