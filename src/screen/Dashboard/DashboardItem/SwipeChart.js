/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  Text,
  View,
  Dimensions,
  processColor,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
} from 'react-native';
import {Card} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';
import Swiper from 'react-native-swiper';
import SearchableDropdown from '../../../library/component/SearchableDropdown';
import GlobalConfig from '../../../library/network/GlobalConfig';
import {LineChart} from 'react-native-chart-kit';
export default class ChartKit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,

      visibleValue: false,
      valueChart: '',
      bulan: '',
      slider1ActiveSlide: 1,
      id_gardu_induk: 1,
      data: [],
      labels: '',
      trafo1: '',
      trafo2: '',
      trafo3: '',
      trafo4: '',
      trafo5: '',
      trafo6: '',
    };
  }

  getDataChart() {
    var labels = [];
    var trafo1 = [];
    var trafo2 = [];
    var trafo3 = [];
    var trafo4 = [];
    var trafo5 = [];
    var trafo6 = [];

    for (var i = 0; i < this.state.data.length; i++) {
      labels.push(this.state.data[i].tanggal);
      trafo1.push(this.state.data[i].dataset[0].trafo_1);
      trafo2.push(this.state.data[i].dataset[0].trafo_2);
      trafo3.push(this.state.data[i].dataset[0].trafo_3);
      trafo4.push(this.state.data[i].dataset[0].trafo_4);
      trafo5.push(this.state.data[i].dataset[0].trafo_5);
      trafo6.push(this.state.data[i].dataset[0].trafo_6);
    }

    this.setState(
      {
        labels: labels,
        trafo1: trafo1,
        trafo2: trafo2,
        trafo3: trafo3,
        trafo4: trafo4,
        trafo5: trafo5,
        trafo6: trafo6,
      },
      function() {
        this.setState({
          isLoading: false,
        });
        console.log('dataset', this.state.labels);
        console.log('dataset', this.state.trafo1);
      },
    );
  }

  loadData() {
    var date = new Date();
    var year = date.getFullYear();
    var bulan = date.getMonth();
    this.setState({
      isLoading: true,
    });
    this.setState({isLoading: true});
    var url = GlobalConfig.SERVERHOST + 'kamis_lemon/get_chart_trafo';
    var formData = new FormData();
    formData.append('id_gardu_induk', this.state.id_gardu_induk);
    formData.append('tahun', year);
    formData.append('bulan', this.state.bulan);
    console.log('frmdta', formData);
    fetch(url, {
      method: 'POST',
      body: formData,
    })
      .then(response => response.json())
      .then(response => {
        console.log('data', response);
        if (response.status === 'success') {
          if (response.data === null) {
            this.setState({
              data: response.data,
              isLoading: false,
            });
          } else {
            this.setState(
              {
                data: response.data,
              },
              function() {
                if (JSON.stringify(this.state.data) === '[]') {
                  console.log('metu');
                } else {
                  this.getDataChart();
                }

                console.log('Data all new', this.state.data);
              },
            );
          }
        } else {
          this.setState({
            isLoading: false,
          });
          Alert('Gagal Load Data', [
            {
              text: 'Okay',
            },
          ]);
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        Alert.alert('Error', 'Check Your Internet Connection', [
          {
            text: 'Okay',
          },
        ]);
        console.log(error);
      });
  }

  async componentDidMount() {
    var date = new Date();
    var year = await date.getFullYear();
    var bulan = await date.getMonth();
    await this.setState({
      bulan: bulan + 1,
    });
    this.loadData();
  }

  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 3; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  render() {
    return (
      <View>
        <View style={{marginHorizontal: 20, marginBottom: 15}}>
          <View style={{marginLeft: 5, marginBottom: 2}}>
            <Text>Gardu Induk</Text>
          </View>
          <View>
            <SearchableDropdown
              api="gardu_induk/get_all_data_gardu_induk"
              searchByApi={false}
              displayData="nm_gardu_induk"
              selectedMethod={item =>
                this.setState(
                  {
                    nm_gardu_induk: item.nm_gardu_induk,
                    id_gardu_induk: item.id_gardu_induk,
                  },
                  function() {
                    this.loadData();
                  },
                )
              }
              placeholder="Pilih Gardu Induk..."
            />
          </View>
          <View style={{marginLeft: 5, marginBottom: 2, marginTop: 5}}>
            <Text>Bulan</Text>
          </View>
          <View>
            <SearchableDropdown
              items={Bulan}
              searchByApi={false}
              displayData="NAMA_LIST"
              selectedMethod={item =>
                this.setState(
                  {
                    nm_bulan: item.NAMA_LIST,
                    bulan: item.ID_OPTIONS_LIST,
                  },
                  function() {
                    this.loadData();
                  },
                )
              }
              selected={this.state.nm_bulan}
              placeholder="Pilih Bulan..."
            />
          </View>
        </View>
        {JSON.stringify(this.state.data) === '[]' ? (
          <View style={{alignItems: 'center'}}>
            <Text>Data Chart Kosong</Text>
          </View>
        ) : (
          <View>
            {this.state.isLoading === true ? (
              <View>
                <ActivityIndicator />
              </View>
            ) : (
              <View>
                {this.state.data === null ? (
                  <View>
                    <Text style={{textAlign: 'center'}}>Data Kosong</Text>
                  </View>
                ) : (
                  <Swiper
                    onIndexChanged={() =>
                      this.setState({
                        visibleValue: false,
                      })
                    }
                    height="100%"
                    loadMinimal
                    loadMinimalSize={1}
                    paginationStyle={{
                      bottom: -20,
                    }}>
                    <View>
                      <View style={{marginVertical: 5, marginHorizontal: 25}}>
                        <Text style={{fontWeight: 'bold', color: 'black'}}>
                          Trafo 1
                        </Text>
                      </View>
                      <LineChart
                        bezier
                        data={{
                          labels: this.state.labels,
                          datasets: [
                            {
                              data: this.state.trafo1,
                            },
                          ],
                        }}
                        width={Dimensions.get('window').width - 50} // from react-native
                        height={220}
                        onDataPointClick={({value, getColor}) =>
                          this.setState({
                            visibleValue: true,
                            valueChart: value,
                          })
                        }
                        segments={3}
                        verticalLabelRotation={0}
                        chartConfig={{
                          propsForLabels: {fontSize: 10, fontWeight: 'bold'},
                          backgroundGradientFrom: this.getRandomColor(),
                          color: (opacity = 1) =>
                            `rgba(255, 255, 255, ${opacity})`,
                          decimalPlaces: 0, // optional, defaults to 2dp
                          labelColor: (opacity = 1) => 'white',
                          style: {
                            borderRadius: 10,
                          },
                        }}
                        style={{
                          alignItems: 'center',
                          borderRadius: 10,
                        }}
                      />
                    </View>
                    <View>
                      <View style={{marginVertical: 5, marginHorizontal: 25}}>
                        <Text style={{fontWeight: 'bold', color: 'black'}}>
                          Trafo 2
                        </Text>
                      </View>
                      <LineChart
                        bezier
                        data={{
                          labels: this.state.labels,
                          datasets: [
                            {
                              data: this.state.trafo2,
                            },
                          ],
                        }}
                        width={Dimensions.get('window').width - 50} // from react-native
                        height={220}
                        onDataPointClick={({value, getColor}) =>
                          this.setState({
                            visibleValue: true,
                            valueChart: value,
                          })
                        }
                        segments={3}
                        verticalLabelRotation={0}
                        chartConfig={{
                          propsForLabels: {fontSize: 10, fontWeight: 'bold'},
                          backgroundGradientFrom: this.getRandomColor(),
                          color: (opacity = 1) =>
                            `rgba(255, 255, 255, ${opacity})`,
                          decimalPlaces: 0, // optional, defaults to 2dp
                          labelColor: (opacity = 1) => 'white',
                          style: {
                            borderRadius: 10,
                          },
                        }}
                        style={{
                          alignItems: 'center',
                          borderRadius: 10,
                        }}
                      />
                    </View>
                    <View>
                      <View style={{marginVertical: 5, marginHorizontal: 25}}>
                        <Text style={{fontWeight: 'bold', color: 'black'}}>
                          Trafo 3
                        </Text>
                      </View>
                      <LineChart
                        bezier
                        data={{
                          labels: this.state.labels,
                          datasets: [
                            {
                              data: this.state.trafo3,
                            },
                          ],
                        }}
                        width={Dimensions.get('window').width - 50} // from react-native
                        height={220}
                        onDataPointClick={({value, getColor}) =>
                          this.setState({
                            visibleValue: true,
                            valueChart: value,
                          })
                        }
                        segments={3}
                        verticalLabelRotation={0}
                        chartConfig={{
                          propsForLabels: {fontSize: 10, fontWeight: 'bold'},
                          backgroundGradientFrom: this.getRandomColor(),
                          color: (opacity = 1) =>
                            `rgba(255, 255, 255, ${opacity})`,
                          decimalPlaces: 0, // optional, defaults to 2dp
                          labelColor: (opacity = 1) => 'white',
                          style: {
                            borderRadius: 10,
                          },
                        }}
                        style={{
                          alignItems: 'center',
                          borderRadius: 10,
                        }}
                      />
                    </View>
                    <View>
                      <View style={{marginVertical: 5, marginHorizontal: 25}}>
                        <Text style={{fontWeight: 'bold', color: 'black'}}>
                          Trafo 4
                        </Text>
                      </View>
                      <LineChart
                        bezier
                        data={{
                          labels: this.state.labels,
                          datasets: [
                            {
                              data: this.state.trafo4,
                            },
                          ],
                        }}
                        width={Dimensions.get('window').width - 50} // from react-native
                        height={220}
                        onDataPointClick={({value, getColor}) =>
                          this.setState({
                            visibleValue: true,
                            valueChart: value,
                          })
                        }
                        segments={3}
                        verticalLabelRotation={0}
                        chartConfig={{
                          propsForLabels: {fontSize: 10, fontWeight: 'bold'},
                          backgroundGradientFrom: this.getRandomColor(),
                          color: (opacity = 1) =>
                            `rgba(255, 255, 255, ${opacity})`,
                          decimalPlaces: 0, // optional, defaults to 2dp
                          labelColor: (opacity = 1) => 'white',
                          style: {
                            borderRadius: 10,
                          },
                        }}
                        style={{
                          alignItems: 'center',
                          borderRadius: 10,
                        }}
                      />
                    </View>
                    <View>
                      <View style={{marginVertical: 5, marginHorizontal: 25}}>
                        <Text style={{fontWeight: 'bold', color: 'black'}}>
                          Trafo 5
                        </Text>
                      </View>
                      <LineChart
                        bezier
                        data={{
                          labels: this.state.labels,
                          datasets: [
                            {
                              data: this.state.trafo5,
                            },
                          ],
                        }}
                        width={Dimensions.get('window').width - 50} // from react-native
                        height={220}
                        onDataPointClick={({value, getColor}) =>
                          this.setState({
                            visibleValue: true,
                            valueChart: value,
                          })
                        }
                        segments={3}
                        verticalLabelRotation={0}
                        chartConfig={{
                          propsForLabels: {fontSize: 10, fontWeight: 'bold'},
                          backgroundGradientFrom: this.getRandomColor(),
                          color: (opacity = 1) =>
                            `rgba(255, 255, 255, ${opacity})`,
                          decimalPlaces: 0, // optional, defaults to 2dp
                          labelColor: (opacity = 1) => 'white',
                          style: {
                            borderRadius: 10,
                          },
                        }}
                        style={{
                          alignItems: 'center',
                          borderRadius: 10,
                        }}
                      />
                    </View>
                    <View>
                      <View style={{marginVertical: 5, marginHorizontal: 25}}>
                        <Text style={{fontWeight: 'bold', color: 'black'}}>
                          Trafo 6
                        </Text>
                      </View>
                      <LineChart
                        bezier
                        data={{
                          labels: this.state.labels,
                          datasets: [
                            {
                              data: this.state.trafo6,
                            },
                          ],
                        }}
                        width={Dimensions.get('window').width - 50} // from react-native
                        height={220}
                        onDataPointClick={({value, getColor}) =>
                          this.setState({
                            visibleValue: true,
                            valueChart: value,
                          })
                        }
                        segments={3}
                        verticalLabelRotation={0}
                        chartConfig={{
                          propsForLabels: {fontSize: 10, fontWeight: 'bold'},
                          backgroundGradientFrom: this.getRandomColor(),
                          color: (opacity = 1) =>
                            `rgba(255, 255, 255, ${opacity})`,
                          decimalPlaces: 0, // optional, defaults to 2dp
                          labelColor: (opacity = 1) => 'white',
                          style: {
                            borderRadius: 10,
                          },
                        }}
                        style={{
                          alignItems: 'center',
                          borderRadius: 10,
                        }}
                      />
                    </View>
                  </Swiper>
                )}
                <View>
                  {this.state.visibleValue === true ? (
                    <View
                      style={{
                        width: '30%',
                        marginTop: 25,
                        marginHorizontal: 25,
                        flexDirection: 'row',
                        padding: 10,
                        backgroundColor: 'gray',
                        borderRadius: 5,
                        alignSelf: 'flex-end',
                      }}>
                      <Text style={{fontWeight: 'bold', color: 'white'}}>
                        Value :{' '}
                      </Text>
                      <Text style={{fontWeight: 'bold', color: 'white'}}>
                        {this.state.valueChart}
                      </Text>
                    </View>
                  ) : (
                    <View />
                  )}
                </View>
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}

const Bulan = [
  {NAMA_LIST: 'Januari', ID_OPTIONS_LIST: '1'},
  {NAMA_LIST: 'Februari', ID_OPTIONS_LIST: '2'},
  {NAMA_LIST: 'Maret', ID_OPTIONS_LIST: '3'},
  {NAMA_LIST: 'April', ID_OPTIONS_LIST: '4'},
  {NAMA_LIST: 'Mei', ID_OPTIONS_LIST: '5'},
  {NAMA_LIST: 'Juni', ID_OPTIONS_LIST: '6'},
  {NAMA_LIST: 'Juli', ID_OPTIONS_LIST: '7'},
  {NAMA_LIST: 'Agustus', ID_OPTIONS_LIST: '8'},
  {NAMA_LIST: 'September', ID_OPTIONS_LIST: '9'},
  {NAMA_LIST: 'Oktober', ID_OPTIONS_LIST: '10'},
  {NAMA_LIST: 'November', ID_OPTIONS_LIST: '11'},
  {NAMA_LIST: 'Desember', ID_OPTIONS_LIST: '12'},
];
