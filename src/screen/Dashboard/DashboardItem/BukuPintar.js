import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  Text,
  View,
  Dimensions,
  processColor,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  Alert,
  Linking,
} from 'react-native';
import {Card, Button} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';
import GlobalConfig from '../../../library/network/GlobalConfig';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default class ChartKit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoadingData: false,
      listBuku: [
        {title: 'Buku Pintar Transmisi', upload: '12 maret 2020'},
        {title: 'Buku Pintar Proteksi', upload: '12 maret 2020'},
        {title: 'Buku Pintar Gardu Induk', upload: '12 maret 2020'},
      ],
    };
  }

  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 3; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  loadData() {
    this.setState({
      isLoading: true,
    });
    this.setState({isLoading: true});
    var url = GlobalConfig.SERVERHOST + 'buku/get_all_data_buku';
    fetch(url, {
      method: 'POST',
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.status === 'success') {
          this.setState(
            {
              data: response.data,
            },
            function() {
              console.log('Data all', this.state.data);
              this.setState({
                isLoading: false,
              });
            },
          );
        } else {
          this.setState({
            isLoading: false,
          });
          Alert('Gagal Load Data', [
            {
              text: 'Okay',
            },
          ]);
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        Alert.alert('Error', 'Check Your Internet Connection', [
          {
            text: 'Okay',
          },
        ]);
        console.log(error);
      });
  }

  componentDidMount() {
    this.loadData();
  }

  onOpenBuku(name) {
    var url = GlobalConfig.base_url + 'assets/dokumen-buku/' + name;
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        alert('Browser tidak ditemukan!');
        console.log("Don't know how to open URI: " + url);
      }
    });
  }

  render() {
    return (
      <View style={{marginTop: 0}}>
        {this.state.isLoading === true ? (
          <View style={styleBuku.containerLoading}>
            <ActivityIndicator size="large" />
          </View>
        ) : (
          <View style={{padding: 10, borderRadius: 10}}>
            <View style={styleBuku.titleContain}>
              <Text style={styleBuku.textTitle}>BUKU UPT</Text>
            </View>
            {this.state.data.map((data, index) => (
              <View key={index} style={styleBuku.container}>
                <View style={styleBuku.boxList}>
                  <View style={{flex: 1}}>
                    <Image
                      source={require('../../../res/images/books.png')}
                      style={styleBuku.imageBook}
                    />
                  </View>
                  <View style={styleBuku.titleView}>
                    <Text style={styleBuku.titleBook}>{data.judul_buku}</Text>
                    <Text style={styleBuku.tglBook}>{data.created_at}</Text>
                  </View>
                  <View style={styleBuku.downloadView}>
                    <Button
                      transparent
                      onPress={() => this.onOpenBuku(data.file_buku)}>
                      <Image
                        source={require('../../../res/images/download.png')}
                        style={styleBuku.downloadImg}
                      />
                    </Button>
                  </View>
                </View>
              </View>
            ))}
          </View>
        )}
      </View>
    );
  }
}

const styleBuku = StyleSheet.create({
  titleContain: {
    marginBottom: hp('1%'),
    paddingLeft: wp('2%'),
    borderBottomWidth: 0.5,
    borderColor: colors.gray08,
    paddingBottom: hp('1%'),
  },
  textTitle: {
    fontWeight: 'bold',
    color: colors.gray08,
    fontSize: wp('5%'),
  },
  containerLoading: {
    alignItems: 'center',
    marginTop: hp('2%'),
  },
  container: {
    flex: 1,
    marginVertical: hp('0.5%'),
  },
  boxList: {
    flexDirection: 'row',
    padding: wp('2%'),
    borderRadius: hp('1%'),
    backgroundColor: 'rgba(255,255,255,0.3)',
  },
  imageBook: {
    width: wp('10%'),
    height: hp('5%'),
    resizeMode: 'contain',
  },
  titleView: {
    flex: 5,
    marginLeft: wp('2%'),
    justifyContent: 'center',
  },
  titleBook: {
    color: 'black',
    fontSize: wp('3.2%'),
    marginBottom: hp('0.5%'),
  },
  tglBook: {
    fontSize: wp('2.7%'),
    color: 'black',
  },
  downloadView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginRight: wp('1%'),
  },
  downloadImg: {
    width: wp('8.5%'),
    height: hp('8.5%'),
    resizeMode: 'contain',
  },
});
