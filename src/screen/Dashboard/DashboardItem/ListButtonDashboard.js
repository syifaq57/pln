/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  View,
  Dimensions,
  processColor,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  Linking,
} from 'react-native';
import {Card, Button, Text, Content} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';
import Modal from 'react-native-modal';
import ItemDataMasuk from '../ButtonDashboardItem/itemDataMasuk';
import LinkGI from '../ButtonDashboardItem/LinkGI';
import Kinerja from '../ButtonDashboardItem/Kinerja';
import SwipeChart from './SwipeChart';
import Simoji from '../ButtonDashboardItem/SimojiButton';
import Kegiatan from '../ButtonDashboardItem/Kegiatan';
import Approval from '../ButtonDashboardItem/DataApproval';
const {width} = Dimensions.get('window');
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default class ListButtonDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingData: false,
      isDataMasukVisible: false,
      isSwipeChartVisible: false,
      isLinkgiVisible: false,
      isKinerjaVisible: false,
      isKegiatanVisible: false,
      isSimojiVisible: false,
      isApprovalVisible: false,
    };
  }

  DataMasukModal = () => {
    this.setState({isDataMasukVisible: !this.state.isDataMasukVisible});
  };

  SwipeChartModal = () => {
    this.setState({isSwipeChartVisible: !this.state.isSwipeChartVisible});
  };

  LinkgiModal = () => {
    this.setState({isLinkgiVisible: !this.state.isLinkgiVisible});
  };

  KinerjaModal = () => {
    this.setState({isKinerjaVisible: !this.state.isKinerjaVisible});
  };

  KegiatanModal = () => {
    this.setState({isKegiatanVisible: !this.state.isKegiatanVisible});
  };

  SimojiModal = () => {
    this.setState({isSimojiVisible: !this.state.isSimojiVisible});
  };
  ApprovalModal = () => {
    this.setState({isApprovalVisible: !this.state.isApprovalVisible});
  };

  onOpenMaps() {
    var url =
      'https://www.google.com/maps/d/u/0/viewer?mid=18v9CtkAIFeiKv_bjQHXVRyME3xHO2E69&ll=-7.176971417091967%2C113.12480540000001&z=9';
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        alert('Browser tidak ditemukan!');
        console.log("Don't know how to open URI: " + url);
      }
    });
  }
  navigateToScreen(route) {
    this.KegiatanModal();
    this.props.navigation.navigate(route);
  }

  render() {
    return (
      <View style={buttonStyle.container}>
        <View style={buttonStyle.content}>
          <View style={buttonStyle.viewVert}>
            <View style={buttonStyle.circleView}>
              <TouchableOpacity
                onPress={() => this.DataMasukModal()}
                style={[
                  buttonStyle.circleButton,
                  {backgroundColor: '#B4D7E1'},
                ]}>
                <Image
                  source={require('../../../res/images/brief.png')}
                  style={buttonStyle.imageCircle}
                />
              </TouchableOpacity>
              <Text style={buttonStyle.textCircle}>Data Masuk</Text>
            </View>
            <View style={buttonStyle.circleView}>
              <TouchableOpacity
                onPress={() => this.KegiatanModal()}
                style={[
                  buttonStyle.circleButton,
                  {
                    backgroundColor: '#F3D6DE',
                  },
                ]}>
                <Image
                  source={require('../../../res/images/calendar.png')}
                  style={buttonStyle.imageCircle}
                />
              </TouchableOpacity>
              <Text style={buttonStyle.textCircle}>Kegiatan</Text>
            </View>
          </View>
          <View style={buttonStyle.viewVert}>
            <View style={buttonStyle.circleView}>
              <TouchableOpacity
                onPress={() => this.SwipeChartModal()}
                style={[
                  buttonStyle.circleButton,
                  {
                    backgroundColor: '#F39965',
                  },
                ]}>
                <Image
                  source={require('../../../res/images/bar-chart.png')}
                  style={buttonStyle.imageCircle}
                />
              </TouchableOpacity>
              <Text style={buttonStyle.textCircle}>Kamis Lemon</Text>
            </View>
            <View style={buttonStyle.circleView}>
              <TouchableOpacity
                onPress={() => this.LinkgiModal()}
                style={[
                  buttonStyle.circleButton,
                  {
                    backgroundColor: '#CF524D',
                  },
                ]}>
                <Image
                  source={require('../../../res/images/station.png')}
                  style={buttonStyle.imageCircle}
                />
              </TouchableOpacity>
              <Text style={buttonStyle.textCircle}>Link GI</Text>
            </View>
          </View>
          <View style={buttonStyle.viewVert}>
            <View style={buttonStyle.circleView}>
              <TouchableOpacity
                onPress={() => this.ApprovalModal()}
                style={[
                  buttonStyle.circleButton,
                  {backgroundColor: '#55B6DC'},
                ]}>
                <Image
                  source={require('../../../res/images/approved.png')}
                  style={buttonStyle.imageCircle}
                />
              </TouchableOpacity>
              <Text style={buttonStyle.textCircle}>Data Approval</Text>
            </View>
            <View style={buttonStyle.circleView}>
              <TouchableOpacity
                onPress={() => this.onOpenMaps()}
                style={[
                  buttonStyle.circleButton,
                  {
                    backgroundColor: '#FDECCE',
                  },
                ]}>
                <Image
                  source={require('../../../res/images/map.png')}
                  style={buttonStyle.imageCircle}
                />
              </TouchableOpacity>
              <Text style={buttonStyle.textCircle}>Peta Aset</Text>
            </View>
          </View>
          <View style={buttonStyle.viewVert}>
            <View style={buttonStyle.circleView}>
              <TouchableOpacity
                onPress={() => this.SimojiModal()}
                style={[
                  buttonStyle.circleButton,
                  {
                    backgroundColor: '#FFDB8F',
                  },
                ]}>
                <Image
                  source={require('../../../res/images/multimeter.png')}
                  style={buttonStyle.imageCircle}
                />
              </TouchableOpacity>
              <Text style={buttonStyle.textCircle}>Data Operasional</Text>
            </View>
            <View style={buttonStyle.circleView}>
              <TouchableOpacity
                onPress={() => this.KinerjaModal()}
                style={[
                  buttonStyle.circleButton,
                  {
                    backgroundColor: '#D2FACE',
                  },
                ]}>
                <Image
                  source={require('../../../res/images/result.png')}
                  style={buttonStyle.imageCircle}
                />
              </TouchableOpacity>
              <Text style={buttonStyle.textCircle}>Kinerja</Text>
            </View>
          </View>
        </View>
        {/* DataMasuk */}
        <Modal
          style={{justifyContent: 'flex-start'}}
          animationIn={'slideInDown'}
          isVisible={this.state.isDataMasukVisible}
          onBackdropPress={this.DataMasukModal}
          backdropOpacity={0.2}
          animationOut={'slideOutUp'}
          animationInTiming={1000}>
          <ItemDataMasuk />
        </Modal>
        {/* Simoji */}
        <Modal
          style={{justifyContent: 'flex-start'}}
          animationIn={'slideInDown'}
          isVisible={this.state.isSimojiVisible}
          onBackdropPress={this.SimojiModal}
          backdropOpacity={0.2}
          animationOut={'slideOutUp'}
          animationInTiming={1000}>
          <Simoji />
        </Modal>
        {/* Kamis Lemon */}
        <Modal
          style={{marginHorizontal: 5, justifyContent: 'flex-start'}}
          animationIn={'slideInDown'}
          isVisible={this.state.isSwipeChartVisible}
          onBackdropPress={this.SwipeChartModal}
          backdropOpacity={0.5}
          animationOut={'slideOutUp'}
          animationInTiming={1000}>
          <View
            style={{
              backgroundColor: 'rgba(255,255,255,1)',
              borderRadius: 10,
              paddingBottom: 30,
              marginTop: 50,
            }}>
            <View
              style={{
                alignItems: 'center',
                marginBottom: 5,
                marginTop: 5,
                padding: 5,
              }}>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 20,
                  color: colors.greenpln,
                }}>
                Kamis Lemon Chart
              </Text>
            </View>
            <ScrollView style={{paddingBottom: 20}}>
              <SwipeChart />
            </ScrollView>
          </View>
        </Modal>
        {/* Link GI */}
        <Modal
          style={{justifyContent: 'flex-start'}}
          animationIn={'slideInDown'}
          isVisible={this.state.isLinkgiVisible}
          onBackdropPress={this.LinkgiModal}
          backdropOpacity={0.5}
          animationOut={'slideOutUp'}
          animationInTiming={1000}>
          <LinkGI />
        </Modal>
        {/* Kinerja */}
        <Modal
          style={{justifyContent: 'flex-start'}}
          animationIn={'slideInDown'}
          isVisible={this.state.isKinerjaVisible}
          onBackdropPress={this.KinerjaModal}
          backdropOpacity={0.5}
          animationOut={'slideOutUp'}
          animationInTiming={1000}>
          <Kinerja />
        </Modal>
        {/* Kegiatan */}
        <Modal
          style={{justifyContent: 'flex-start'}}
          animationIn={'slideInDown'}
          isVisible={this.state.isKegiatanVisible}
          onBackdropPress={this.KegiatanModal}
          backdropOpacity={0.5}
          animationOut={'slideOutUp'}
          animationInTiming={1000}>
          <View style={{marginTop: hp('5%')}}>
            <View
              style={{
                backgroundColor: 'rgba(255,255,255,0.9)',
                borderRadius: hp('1%'),
                padding: hp('1.5%'),
              }}>
              <View style={{alignItems: 'center'}}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: wp('5%'),
                    color: colors.greenpln,
                  }}>
                  Jadwal & Kegiatan
                </Text>
              </View>
              <View style={{marginTop: hp('1%')}}>
                <View style={{marginBottom: hp('1%')}}>
                  <Image
                    source={require('../../../res/images/calendar.png')}
                    style={{
                      width: wp('15%'),
                      height: hp('10%'),
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Button
                  onPress={() => this.navigateToScreen('TambahKegiatan')}
                  style={{backgroundColor: colors.greenpln}}>
                  <Text
                    style={{
                      flex: 1,
                      fontSize: wp('4.5%'),
                      textAlign: 'center',
                      fontWeight: 'bold',
                    }}>
                    Tambah Kegiatan
                  </Text>
                </Button>
                <Kegiatan navigation={this.props.navigation} />
              </View>
            </View>
          </View>
        </Modal>
        {/* Data Approval */}
        <Modal
          style={{justifyContent: 'flex-start'}}
          animationIn={'slideInDown'}
          isVisible={this.state.isApprovalVisible}
          onBackdropPress={this.ApprovalModal}
          backdropOpacity={0.5}
          animationOut={'slideOutUp'}
          animationInTiming={1000}>
          <Approval
            navigation={this.props.navigation}
            visible={this.ApprovalModal}
          />
        </Modal>
      </View>
    );
  }
}

const buttonStyle = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: hp('1%'),
    alignItems: 'center',
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: hp('1%'),
  },
  viewVert: {
    marginRight: wp('7.5%'),
  },
  circleView: {
    alignItems: 'center',
    padding: hp('1%'),
    borderRadius: hp('8%'),
    flex: 1,
  },
  circleButton: {
    padding: hp('2%'),
    borderRadius: hp('8%'),
  },
  imageCircle: {
    width: hp('5%'),
    height: hp('5%'),
    resizeMode: 'contain',
  },
  textCircle: {
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
    fontSize: hp('1.4%'),
    marginTop: hp('1%'),
  },
});
