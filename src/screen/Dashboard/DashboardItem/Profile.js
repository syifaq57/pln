/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  View,
  Dimensions,
  processColor,
  TouchableOpacity,
  ActivityIndicator,
  AsyncStorage,
  Image,
} from 'react-native';
import {Card, Button, Text} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const {width} = Dimensions.get('window');

export default class ListButtonDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingData: true,
      DataUser: [],
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('DataUser').then(value =>
      this.setState(
        {
          DataUser: JSON.parse(value),
        },
        function() {
          this.setState({
            isLoadingData: false,
          });
          console.log('user aa', this.state.DataUser);
        },
      ),
    );
  }

  render() {
    return (
      <View style={{flex: 1, marginVertical: hp('2%')}}>
        <Card
          style={{
            paddingVertical: hp('2%'),
            paddingHorizontal: hp('1.5%'),
            borderRadius: wp('3%'),
            backgroundColor: 'rgba(255,255,255,0.4)',
          }}>
          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 1, marginRight: wp('2%')}}>
              <Image
                source={require('../../../res/images/pln.png')}
                style={{
                  width: wp('30%'),
                  height: hp('12%'),
                  resizeMode: 'contain',
                  alignSelf: 'center',
                }}
              />
            </View>
            <View style={{flex: 2, justifyContent: 'center'}}>
              <View
                style={{
                  paddingLeft: wp('2%'),
                  borderBottomWidth: 0.5,
                  borderColor: colors.greenpln,
                }}>
                <Text
                  style={{
                    fontStyle: 'italic',
                    fontSize: hp('1.5%'),
                    color: colors.greenpln,
                  }}>
                  Nama
                </Text>
              </View>
              {this.state.isLoadingData === true ? (
                <View>
                  <ActivityIndicator />
                </View>
              ) : (
                <View style={{paddingLeft: wp('2%'), marginTop: 2}}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontStyle: 'italic',
                      color: colors.greenpln,
                      fontSize: hp('1.6%')
                    }}>
                    {this.state.DataUser.user.nama_lengkap}
                  </Text>
                </View>
              )}
              
              <View
                style={{
                  paddingLeft: wp('2%'),
                  marginTop: hp('1%'),
                  borderBottomWidth: 0.5,
                  borderColor: colors.greenpln,
                }}>
                <Text
                  style={{
                    fontStyle: 'italic',
                    fontSize: hp('1.5%'),
                    color: colors.greenpln,
                  }}>
                  NIP
                </Text>
              </View>
              {this.state.isLoadingData === true ? (
                <View>
                  <ActivityIndicator />
                </View>
              ) : (
                <View style={{paddingLeft: wp('2%'), marginTop: 2}}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontStyle: 'italic',
                      color: colors.greenpln,
                      fontSize: hp('1.6%'),
                    }}>
                    {this.state.DataUser.user.nip}
                  </Text>
                </View>
              )}
            </View>
          </View>
        </Card>
      </View>
    );
  }
}
