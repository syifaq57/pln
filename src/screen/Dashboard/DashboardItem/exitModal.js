/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  View,
  Dimensions,
  processColor,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  FlatList,
  Linking,
  Alert,
} from 'react-native';
import {Card, Button, Text} from 'native-base';
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from '../../../res/colors/index';

const {width} = Dimensions.get('window');
export default class exitModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoadingData: false,
    };
  }
  

  render() {
    return (
      <View style={{marginTop: 20}}>
        <TouchableOpacity
          style={{
            marginBottom: 10,
            backgroundColor: 'rgba(255,255,255,1)',
            borderRadius: 10,
            padding: 15,
          }}>
          <View style={{alignItems: 'center', marginBottom: 0}}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 20,
                color: colors.greenpln,
              }}>
              Exit App
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            marginBottom: 20,
            backgroundColor: 'rgba(255,255,255,1)',
            borderRadius: 10,
            padding: 15,
          }}>
          <View style={{alignItems: 'center', marginBottom: 0}}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 20,
                color: colors.greenpln,
              }}>
              Log Out
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            backgroundColor: 'rgba(255,255,255,0.9)',
            borderRadius: 10,
            padding: 15,
          }}>
          <View style={{alignItems: 'center', marginBottom: 5}}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 20,
                color: colors.greenpln,
              }}>
              Cancel
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
