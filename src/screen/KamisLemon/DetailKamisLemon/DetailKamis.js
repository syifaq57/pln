/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Platform, Dimensions, Alert} from 'react-native';
import {
  Container,
  Header,
  Left,
  Button,
  Content,
  Card,
  Text,
} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../../res/colors/index';
import GlobalConfig from '../../../library/network/GlobalConfig';
import {TabBar, TabView, SceneMap} from 'react-native-tab-view';

export default class DetailKamis extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      dataCoba: 'tes',
      index: 0,
      routes: [
        {key: 'view1', title: 'Lokasi & tanggal'},
        {key: 'view2', title: 'CheckList'},
        {key: 'view3', title: 'Counter Trafo'},
        {key: 'view4', title: 'Cuaca & Kondisi'},
      ],
    };
  }

  loadData() {
    this.setState({
      isLoading: true,
    });
    const {id_kamis_lemon} = this.props.navigation.state.params;
    this.setState({isLoading: true});
    var url =
      GlobalConfig.SERVERHOST + 'kamis_lemon/get_detail_data_kamis_lemon';
    var formData = new FormData();
    formData.append('id_kamis_lemon', id_kamis_lemon);
    console.log('frmdta', formData);
    fetch(url, {
      method: 'POST',
      body: formData,
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.status === 'success') {
          this.setState(
            {
              data: response.data,
            },
            function() {
              console.log('Data all', this.state.data);
              this.setState({
                isLoading: false,
              });
            },
          );
        } else {
          this.setState({
            isLoading: false,
          });
          Alert('Gagal Load Data', [
            {
              text: 'Okay',
            },
          ]);
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        Alert.alert('Error', 'Check Your Internet Connection', [
          {
            text: 'Okay',
          },
        ]);
        console.log(error);
      });
  }
  componentDidMount() {
    this.loadData();
  }

  render() {
    const view1 = () => (
      <Content>
        <View style={{margin: 15}}>
          <Card
            style={{
              borderRadius: 8,
              flex: 1,
              padding: 15,
            }}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text style={{fontSize: 10}}>Tanggal Input</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.tgl_input}
              </Text>
              <Text style={{fontSize: 10}}>Gardu Induk</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.nm_gardu_induk}
              </Text>
              <Text style={{fontSize: 10}}>Nama Pelapor</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.nama_pelapor}
              </Text>
            </View>
          </Card>
        </View>
        <View style={{height: 50}} />
      </Content>
    );
    const view2 = () => (
      <Content>
        <View style={{margin: 15}}>
          <View style={{marginLeft: 10, marginTop: 10}}>
            <Text style={{fontWeight: 'bold'}}>CheckList 1</Text>
          </View>
          <Card
            style={{
              borderRadius: 8,
              flex: 1,
              padding: 15,
            }}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text style={{fontSize: 10}}>Thermovisi Peralatan MTU</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.thermovisi_peralatan_mtu}
              </Text>
              <Text style={{fontSize: 10}}>Kondisi Visual Peralatan Trafo</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.kondisi_visual_peralatan_trafo}
              </Text>
              <Text style={{fontSize: 10}}>Tekanan Gas SF6 PMT</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.tekanan_gas_sf6_pmt}
              </Text>
              <Text style={{fontSize: 10}}>Thermovisi Jalur Kabel Power</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.thermovisi_jalur_kabel_power}
              </Text>
            </View>
          </Card>

          <View style={{marginLeft: 10, marginTop: 10}}>
            <Text style={{fontWeight: 'bold'}}>CheckList 2</Text>
          </View>
          <Card
            style={{
              borderRadius: 8,
              flex: 1,
              padding: 15,
            }}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text style={{fontSize: 10}}>Annountciator Relay</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.announciator_relay}
              </Text>
              <Text style={{fontSize: 10}}>Power Suply Relay</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.power_supply_relay}
              </Text>
              <Text style={{fontSize: 10}}>Markshaling Kiosk</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.markshaling_kiosk}
              </Text>
              <Text style={{fontSize: 10}}>Power Suply ACDC</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.power_supply_ac_dc}
              </Text>
            </View>
          </Card>

          <View style={{marginLeft: 10, marginTop: 10}}>
            <Text style={{fontWeight: 'bold'}}>CheckList 3</Text>
          </View>
          <Card
            style={{
              borderRadius: 8,
              flex: 1,
              padding: 15,
            }}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text style={{fontSize: 10}}>Kondisi Tegakan</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.kondisi_tegakan}
              </Text>
              <Text style={{fontSize: 10}}>Kondisi Tower</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.kondisi_tower}
              </Text>
              <Text style={{fontSize: 10}}>Kondisi Aksesoris Tower</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.kondisi_aksesoris_tower}
              </Text>
              <Text style={{fontSize: 10}}>Kondisi Thermovisi Join</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.kondisi_thermovisi_join}
              </Text>
            </View>
          </Card>
        </View>
        <View style={{height: 50}} />
      </Content>
    );

    const view3 = () => (
      <Content>
        <View style={{margin: 15}}>
          <Card
            style={{
              borderRadius: 8,
              flex: 1,
              padding: 15,
            }}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text style={{fontSize: 10}}>Trafo 1</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.trafo_satu}
              </Text>
              <Text style={{fontSize: 10}}>Trafo 2</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.trafo_dua}
              </Text>
              <Text style={{fontSize: 10}}>Trafo 3</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.trafo_tiga}
              </Text>
              <Text style={{fontSize: 10}}>Trafo 4</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.trafo_empat}
              </Text>
              <Text style={{fontSize: 10}}>Trafo 5</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.trafo_lima}
              </Text>
              <Text style={{fontSize: 10}}>Trafo 6</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.trafo_enam}
              </Text>
            </View>
          </Card>
        </View>
        <View style={{height: 50}} />
      </Content>
    );

    const view4 = () => (
      <Content>
        <View style={{margin: 15}}>
          <Card
            style={{
              borderRadius: 8,
              flex: 1,
              padding: 15,
            }}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text style={{fontSize: 10}}>Pekerjaan Pihak Lain</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.pekerjaan_pihak_lain}
              </Text>
              <Text style={{fontSize: 10}}>Kondisi Cuaca</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.kondisi_cuaca}
              </Text>
              <Text style={{fontSize: 10}}>Kondisi Lingkungan</Text>
              <Text style={{marginBottom: 10}}>
                {this.state.data.kondisi_lingkungan}
              </Text>
            </View>
          </Card>
        </View>
        <View style={{height: 50}} />
      </Content>
    );

    return (
      <Container>
        <Header
          transparent
          style={{
            marginTop: Platform.OS === 'ios' ? 0 : 0,
            borderBottomWidth: 0,
            backgroundColor: colors.greenpln,
          }}>
          <View
            style={{
              flex: 3,
              justifyContent: 'center',
            }}>
            <Button
              onPress={() => this.props.navigation.goBack()}
              transparent
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <Icon
                name="md-arrow-round-back"
                size={23}
                style={{color: 'white'}}
              />
              <Text
                uppercase={false}
                style={{
                  flex: 1,
                  textAlignVertical: 'center',
                  fontWeight: 'bold',
                  fontSize: 20,
                  color: 'white',
                }}>
                Detail Kamis Lemon
              </Text>
            </Button>
          </View>
          <View style={{flex: 1}} />
        </Header>
        <Content>
          <TabView
            navigationState={this.state}
            renderScene={SceneMap({
              view1: view1,
              view2: view2,
              view3: view3,
              view4: view4,
            })}
            onIndexChange={index => this.setState({index})}
            initialLayout={{width: Dimensions.get('window').width}}
            renderTabBar={props => (
              <TabBar
                scrollEnabled={true}
                {...props}
                indicatorStyle={{backgroundColor: 'white'}}
                tabStyle={{alignItems: 'flex-start', width: 'auto'}}
                style={{backgroundColor: colors.greenpln}}
                labelStyle={{fontWeight: 'bold', fontSize: 10}}
              />
            )}
          />
        </Content>
      </Container>
    );
  }
}
