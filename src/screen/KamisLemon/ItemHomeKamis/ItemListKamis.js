/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import {Card, Left, Right, Button} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../../res/colors/index';

export default class ItemListKamis extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DataSource: [
        {
          tanggal: '7 Januari 2019',
          garduInduk: 'GI 150KV PLTU GRESIK',
          nomorBA: '036/BAOL/C03/III/2020',
          jenisPekerjaan: 'M02 - Pemeliharaan Alat Kerja',
        },
        {
          tanggal: '28 Agustus 2019',
          garduInduk: 'GI 150KV CERME',
          nomorBA: '037/BAOL/C03/III/2020',
          jenisPekerjaan: 'C01 - Penggantian Aksesoris MTU',
        },
        {
          tanggal: '6 Agustus 2019',
          garduInduk: 'GI 150KV ALTAPRIMA',
          nomorBA: '038/BAOL/C03/III/2020',
          jenisPekerjaan: 'C09 - Penggantian Material Proteksi',
        },
        {
          tanggal: '7 Januari 2019',
          garduInduk: 'GI 150KV PLTU GRESIK',
          nomorBA: '039/BAOL/C03/III/2020',
          jenisPekerjaan: 'M02 - Pemeliharaan Alat Kerja',
        },
        {
          tanggal: '28 Agustus 2019',
          garduInduk: 'GI 150KV CERME',
          nomorBA: '040/BAOL/C03/III/2020',
          jenisPekerjaan: 'C01 - Penggantian Aksesoris MTU',
        },
        {
          tanggal: '6 Agustus 2019',
          garduInduk: 'GI 150KV ALTAPRIMA',
          nomorBA: '041/BAOL/C03/III/2020',
          jenisPekerjaan: 'C09 - Penggantian Material Proteksi',
        },
      ],
      lks_aktif: [],
      isLoadinglks: true,
    };
  }
  navigateToScreen(route) {
    this.props.navigation.navigate(route, {
      id_kamis_lemon: this.props.id_kamis_lemon,
    });
  }

  componentDidMount() {
    this.setState(
      {
        lks_aktif: this.props.lks_aktif,
      },
      function() {
        this.setState({
          isLoadinglks: false,
        });
      },
    );
  }

  render() {
    return (
      <View>
        <TouchableOpacity
          onPress={() => this.navigateToScreen('DetailKamisLemon')}>
          <View style={{marginLeft: 15, marginRight: 15}}>
            <Card
              style={{
                borderRadius: 8,
                flex: 1,
                padding: 15,
                borderColor: 'white',
                borderWidth: 0,
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginLeft: 0,
                  marginRight: 0,
                }}>
                <View style={{flex: 1}}>
                  <View style={{flexDirection: 'row'}}>
                    <View
                      style={{
                        flex: 2,
                        flexDirection: 'column',
                        marginTop: 5,
                      }}>
                      <Text style={{fontSize: 10}}>Tanggal Input</Text>
                      <Text style={{marginBottom: 10}}>
                        {this.props.tgl_input}
                      </Text>
                    </View>
                    <View
                      style={{alignItems: 'flex-end', marginRight: 5, flex: 1}}>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: colors.green01,
                          marginTop: 5,
                        }}>
                        {this.props.kondisi_akhir}
                      </Text>
                    </View>
                  </View>

                  <View style={{flex: 1, flexDirection: 'column'}}>
                    <Text style={{fontSize: 10}}>Gardu Induk</Text>
                    <Text numberOfLines={2} style={{marginBottom: 10}}>
                      {this.props.nm_gardu_induk}
                    </Text>
                    <Text style={{fontSize: 10}}>Nama Pelapor</Text>
                    <Text numberOfLines={2} style={{marginBottom: 10}}>
                      {this.props.nama_pelapor}
                    </Text>
                    {/* {this.state.isLoadinglks === true ? (
                      <View />
                    ) : (
                      <View>
                        {this.state.lks_aktif.map((data, index) => (
                          <Text style={{marginBottom: 10}}>{data.no_lkso}</Text>
                        ))}
                      </View>
                    )} */}
                  </View>
                </View>

                {/* <Text
                    style={{
                      alignSelf: 'center',
                      color: colors.greenDefault,
                      fontWeight: 'normal',
                    }}>
                    {data.ultg}
                  </Text> */}
              </View>
            </Card>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
