/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  StatusBar,
  View,
  ImageBackground,
  AsyncStorage,
  Platform,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {Text, Container, Header, Left, Right, Button} from 'native-base';
import colors from '../../res/colors';
import FloatingButtonKamis from '../../library/component/FloatingButtonKamis';
import Icon from 'react-native-vector-icons/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient';
import ListKamis from './ItemHomeKamis/ListKamis';
import SearchableDropdown from '../../library/component/SearchableDropdown';
import Modal from 'react-native-modal';
export default class HomeKamisLemon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      allyear: [],
      isFilterVisible: false,
      isLoading: false,
      isLoadingYears: true,
      id_gardu_induk: '',
      bulan: '',
      tahun: '',
      visibleInput: true,
    };
  }

  onResetFilter() {
    var date = new Date();
    var year = date.getFullYear();
    this.setState(
      {
        id_gardu_induk: '',
        nm_gardu_induk: 'ALL GARDU INDUK',
        tahun: '',
        nm_bulan: 'ALL BULAN',
        bulan: '',
      },
      function() {
        this.setState(
          {
            isFilterVisible: false,
            isLoading: true,
          },
          function() {
            this.setState({
              isLoading: false,
            });
          },
        );
      },
    );
  }

  componentDidMount() {
    var date = new Date();
    var year = date.getFullYear();

    this.setState(
      {
        year: year,
        // tahun: year,
        bulan: '',
      },

      function() {
        var allyear = this.state.allyear;
        for (var i = 2019; i <= year; i++) {
          allyear.push({tahun: i});
        }
        this.setState(
          {
            allyear: allyear,
          },
          function() {
            this.setState({
              isLoadingYears: false,
            });
            console.log('year', this.state.allyear);
          },
        );
      },
    );

    // AsyncStorage.getItem('DataUser').then(value =>
    //   this.setState(
    //     {
    //       DataUser: JSON.parse(value),
    //     },
    //     function () {
    //       const jabatan = this.state.DataUser.detail_jabatan[0];
    //       if (jabatan.nm_jenis_pegawai === 'MULTG') {
    //         this.setState({
    //           visibleInput: false,
    //         });
    //       } else {
    //         this.setState({
    //           visibleInput: true,
    //         });
    //       }
    //     },
    //   ),
    // );
  }

  FilterModal = () => {
    this.setState({isFilterVisible: !this.state.isFilterVisible});
  };
  onFilter() {
    this.setState(
      {
        isFilterVisible: false,
        isLoading: true,
      },
      function() {
        this.setState({
          isLoading: false,
        });
      },
    );
  }

  render() {
    return (
      <Container style={{flex: 1, display: 'flex'}}>
        <LinearGradient
          // source={require('../../res/images/BGdashboard.jpg')}
          colors={[colors.greenpln, 'white']}
          style={{
            flex: 1,
          }}>
          <Header
            transparent
            style={{
              marginTop: Platform.OS === 'ios' ? 0 : 0,
              borderBottomWidth: 0,
            }}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'center',
              }}>
              <View style={{flex: 6, justifyContent: 'center'}}>
                <Text
                  style={{
                    flex: 1,
                    marginLeft: 10,
                    textAlignVertical: 'center',
                    fontWeight: 'bold',
                    fontSize: 20,
                    color: 'white',
                  }}>
                  Kamis Lemon
                </Text>
              </View>
              <View style={{flex: 1, marginRight: 5}}>
                <Button
                  onPress={() => this.FilterModal()}
                  style={{flex: 1, justifyContent: 'center'}}
                  transparent>
                  <Icon name="sort" size={30} style={{color: 'white'}} />
                </Button>
                <Modal
                  style={{
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                    marginBottom: 0,
                    marginTop: 0,
                    marginRight: 0,
                  }}
                  animationIn={'slideInRight'}
                  isVisible={this.state.isFilterVisible}
                  onBackdropPress={this.FilterModal}
                  backdropOpacity={0}
                  animationOut={'slideOutRight'}
                  animationInTiming={1000}>
                  <LinearGradient
                    colors={[colors.greenpln, 'white']}
                    style={{
                      flex: 1,
                      padding: 15,
                      width: '70%',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        marginTop: 25,
                        borderBottomWidth: 1,
                        paddingBottom: 5,
                        borderColor: colors.greenpln,
                        justifyContent: 'flex-end',
                      }}>
                      <View style={{flex: 2, justifyContent: 'center'}}>
                        <Text
                          style={{
                            marginLeft: 10,
                            fontSize: 20,
                            fontWeight: 'bold',
                          }}>
                          Filter
                        </Text>
                      </View>

                      <TouchableOpacity
                        onPress={() => this.onResetFilter()}
                        style={{
                          flex: 2,
                          borderRadius: 2,
                          justifyContent: 'center',
                          backgroundColor: colors.greenpln,
                        }}>
                        <Text
                          style={{
                            color: 'white',
                            fontWeight: 'bold',
                            textAlign: 'center',
                          }}>
                          Reset Filter
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <ScrollView>
                      <View style={{marginTop: 10}}>
                        <Text
                          style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 12,
                            marginLeft: 5,
                          }}>
                          Gardu Induk
                        </Text>
                      </View>
                      <View style={{marginTop: 10}}>
                        <SearchableDropdown
                          api="gardu_induk/get_all_data_gardu_induk"
                          searchByApi={false}
                          displayData="nm_gardu_induk"
                          otherList={{
                            nm_gardu_induk: 'ALL GARDU INDUK',
                            id_gardu_induk: '',
                          }}
                          selectedMethod={item =>
                            this.setState({
                              nm_gardu_induk: item.nm_gardu_induk,
                              id_gardu_induk: item.id_gardu_induk,
                            })
                          }
                          selected={this.state.nm_gardu_induk}
                          placeholder="Pilih Gardu Induk..."
                        />
                      </View>
                      <View style={{marginTop: 10}}>
                        <Text
                          style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 12,
                            marginLeft: 5,
                          }}>
                          Bulan
                        </Text>
                      </View>
                      <View style={{marginTop: 10}}>
                        <SearchableDropdown
                          items={Bulan}
                          searchByApi={false}
                          displayData="NAMA_LIST"
                          selectedMethod={item =>
                            this.setState({
                              nm_bulan: item.NAMA_LIST,
                              bulan: item.ID_OPTIONS_LIST,
                            })
                          }
                          selected={this.state.nm_bulan}
                          placeholder="Pilih Bulan..."
                        />
                      </View>
                      <View style={{marginTop: 10}}>
                        <Text
                          style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 12,
                            marginLeft: 5,
                          }}>
                          Tahun
                        </Text>
                      </View>
                      <View style={{marginTop: 10}}>
                        <SearchableDropdown
                          items={this.state.allyear}
                          searchByApi={false}
                          displayData="tahun"
                          selected={this.state.tahun}
                          selectedMethod={item =>
                            this.setState({
                              tahun: item.tahun,
                            })
                          }
                          placeholder="Pilih Tahun..."
                        />
                      </View>
                      <View style={{marginTop: 20}}>
                        <Button
                          onPress={() => this.onFilter()}
                          style={{
                            flex: 1,
                            backgroundColor: colors.green01,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Filter Data
                            </Text>
                          </View>
                        </Button>
                      </View>
                    </ScrollView>
                  </LinearGradient>
                </Modal>
              </View>
            </View>
          </Header>
          {this.state.isLoading === true ? (
            <View style={{alignItems: 'center'}}>
              <ActivityIndicator size="large" />
            </View>
          ) : (
            <View style={{flex: 1, flexDirection: 'column'}}>
              <ListKamis
                navigation={this.props.navigation}
                id_gardu_induk={this.state.id_gardu_induk}
                bulan={this.state.bulan}
                tahun={this.state.tahun}
              />
            </View>
          )}
        </LinearGradient>
        {/* {this.state.visibleInput === true ? (
          <FloatingButtonKamis navigation={this.props.navigation} />
        ) : null} */}
        <FloatingButtonKamis navigation={this.props.navigation} />
      </Container>
    );
  }
}

const Bulan = [
  {NAMA_LIST: 'ALL BULAN', ID_OPTIONS_LIST: ''},
  {NAMA_LIST: 'Januari', ID_OPTIONS_LIST: '1'},
  {NAMA_LIST: 'Februari', ID_OPTIONS_LIST: '2'},
  {NAMA_LIST: 'Maret', ID_OPTIONS_LIST: '3'},
  {NAMA_LIST: 'April', ID_OPTIONS_LIST: '4'},
  {NAMA_LIST: 'Mei', ID_OPTIONS_LIST: '5'},
  {NAMA_LIST: 'Juni', ID_OPTIONS_LIST: '6'},
  {NAMA_LIST: 'Juli', ID_OPTIONS_LIST: '7'},
  {NAMA_LIST: 'Agustus', ID_OPTIONS_LIST: '8'},
  {NAMA_LIST: 'September', ID_OPTIONS_LIST: '9'},
  {NAMA_LIST: 'Oktober', ID_OPTIONS_LIST: '10'},
  {NAMA_LIST: 'November', ID_OPTIONS_LIST: '11'},
  {NAMA_LIST: 'Desember', ID_OPTIONS_LIST: '12'},
];
