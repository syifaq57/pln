/* eslint-disable react-native/no-inline-styles */
import React, {Component, Fragment} from 'react';
import {
  Image,
  StatusBar,
  KeyboardAvoidingView,
  ScrollView,
  View,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Alert,
  BackHandler,
  Platform,
  DeviceEventEmitter,
  PushNotificationIOS,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  Container,
  Header,
  Form,
  Item,
  Label,
  Input,
  Footer,
  Left,
  Right,
  Button,
  Body,
  Title,
  CheckBox,
  Content,
  Card,
  CardItem,
  Text,
  Textarea,
  Switch,
} from 'native-base';
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton,
} from 'react-native-popup-dialog';
import DatePicker from 'react-native-datepicker';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import SearchableDropdown from '../../../library/component/SearchableDropdown';
import OthersDropdown from '../../../library/component/OthersDropdown';
import styles from '../../../res/styles/Form';
import colors from '../../../res/colors/index';
import StepIndicator from 'react-native-step-indicator';
import RadioButton from '../../../library/component/CustomRadioButton';
import GlobalConfig from '../../../library/network/GlobalConfig';
import dashstyles from '../../../res/styles/Dashboard';
import {screensEnabled} from 'react-native-screens';
import ImagePicker from 'react-native-image-crop-picker';
import Modal from 'react-native-modal';

const garduItem = [
  {
    id: 0,
    name: 'GI 150KV SAMBIKEREP',
  },
  {
    id: 1,
    name: 'GI 150KV PLTU GRESIK',
  },
  {
    id: 2,
    name: 'GI 150KV PETROKIMIA',
  },
  {
    id: 3,
    name: 'GI 150KV MANYAR',
  },
  {
    id: 4,
    name: 'GI 150KV CERME',
  },
  {
    id: 5,
    name: 'GI 150KV ALTAPRIMA',
  },
];

var indikatorAnomali;

const ListIndikatorAnomali = [
  {NAMA_LIST: 'Repair Immediately', ID_OPTIONS_LIST: '1'},
  {NAMA_LIST: 'Repair During Next PM', ID_OPTIONS_LIST: '2'},
  {NAMA_LIST: 'Need Attention / Pemantauan', ID_OPTIONS_LIST: '3'},
];

const labels = ['Gardu Induk', 'Peralatan', 'Detail Anomali'];

export default class InputAnomaliKamis extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isLoading1: false,
      isLoading3: false,
      isLoadingBay: false,
      isLoadingMerk: false,
      isLoadingTipe: false,
      isLoadingSeri: false,
      isLoadingalatlain: false,
      isModalVisible: false,
      isModalVisible2: false,

      isOpenImage: false,
      isOpenImage2: false,
      teganganLain: '',
      enabledBay: true,
      nextButton: true,
      disabledMerk: true,
      disabledTipe: true,
      disabledSeri: true,
      ListIndikatorAnomali: [],
      currentPosition: 0,
      id_gardu_induk: '',
      ANOMALI_DATE: '',
      id_bay: '',
      id_peralatan: '',
      ketPeralatan: '',
      id_tegangan: '',
      ketTegangan: '',
      id_phasa: '',
      merk_peralatan: '',
      tipePeralatan: '',
      seriPeralatan: '',
      id_indikator_anomali: '',
      uraianAnomali: '',
      dampakOpresional: '',

      penjelasan_dokumen_satu: '',
      penjelasan_dokumen_dua: '',

      //uji
      cekData: 'NORMAL',
      list_image: [],
    };
  }

  loadIndikatorAnomali() {
    var url =
      GlobalConfig.SERVERHOST +
      'indikator_anomali/get_all_data_indikator_anomali';
    var formData = new FormData();
    fetch(url, {
      method: 'POST',
      body: formData,
    })
      .then(response => response.json())
      .then(response => {
        console.log('ada', response);
        if (response.status === 'success') {
          indikatorAnomali = this.state.ListIndikatorAnomali;
          for (var i = 0; i < response.data.length; i++) {
            indikatorAnomali.push({
              ID_OPTIONS_LIST: response.data[i].id_indikator_anomali,
              NAMA_LIST: response.data[i].nm_indikator_anomali,
            });
          }
          this.setState(
            {
              ListIndikatorAnomali: response.indikatorAnomali,
            },
            function() {
              console.log('respo', this.state.ListIndikatorAnomali);
              this.setState({isLoading3: false});
            },
          );
        }
      })
      .catch(error => {
        this.setState({isLoading3: false});
        Alert.alert('Cannot Load Data', 'Data Indikator Anomali ', [
          {
            text: 'Okay',
          },
        ]);
        console.log(error);
      });
  }

  movePage() {
    if (this.state.currentPosition === 0) {
      if (
        this.state.id_gardu_induk === '' ||
        this.state.ANOMALI_DATE === '' ||
        this.state.id_bay === ''
      ) {
        Alert.alert('Lengkapi Form!', 'Form masih ada yang kosong.', [
          {
            text: 'Okay',
          },
        ]);
      } else {
        this.setState(
          {
            isLoading1: true,
            currentPosition: this.state.currentPosition + 1,
          },
          function() {
            this.setState({isLoading1: false});
          },
        );
      }
    } else if (this.state.currentPosition === 1) {
      if (
        this.state.nm_peralatan === '' ||
        this.state.ketPeralatan === '' ||
        this.state.volume_tegangan === '' ||
        this.state.nm_phasa === '' ||
        this.state.merk_peralatan === '' ||
        this.state.tipe_peralatan === '' ||
        this.state.nomor_seri === ''
      ) {
        Alert.alert('Lengkapi Form!', 'Form masih ada yang kosong.', [
          {
            text: 'Okay',
          },
        ]);
      } else {
        this.setState({
          currentPosition: this.state.currentPosition + 1,
        });
      }
    }
  }

  backPage() {
    this.setState({
      currentPosition: this.state.currentPosition - 1,
    });
  }

  onKirimData() {
    this.setState({
      isLoadingSave: true,
      cekData: 'ANOMALI',
    });
    if (
      this.state.id_indikator_anomali === '' ||
      this.state.uraian_anomali === '' ||
      this.state.dampak_operasional === '' ||
      this.state.penjelasan_dokumen_satu === ''
    ) {
      alert('Form belum lengkap');
      this.setState({
        isLoadingSave: false,
      });
    } else {
      var url = GlobalConfig.SERVERHOST + 'lkso/add_data_lkso';
      var formData = new FormData();
      formData.append('id_pegawai', this.state.DataUser.user.id_pegawai);
      formData.append('tgl_anomali', this.state.ANOMALI_DATE);
      formData.append('id_gardu_induk', this.state.id_gardu_induk);
      formData.append('id_bay', this.state.id_bay);
      formData.append('id_peralatan', this.state.id_peralatan);
      formData.append('keterangan_peralatan', this.state.ketPeralatan);
      formData.append('id_tegangan', this.state.id_tegangan);
      formData.append('id_phasa', this.state.id_phasa);
      formData.append('merk_peralatan', this.state.merk_peralatan);
      formData.append('tipe_peralatan', this.state.tipe_peralatan);
      formData.append('nomor_seri', this.state.nomor_seri);
      formData.append('id_indikator_anomali', this.state.id_indikator_anomali);
      formData.append('uraian_anomali', this.state.uraian_anomali);
      formData.append('dampak_operasional', this.state.dampak_operasional);

      if (this.state.penjelasan_dokumen_satu !== '') {
        formData.append(
          'dokumen_anomali_satu',
          {
            uri: this.state.list_image[0].uri,
            name: this.state.list_image[0].name + '.jpg',
            type: 'image/jpg',
          },
          'file',
        );
        formData.append(
          'penjelasan_dokumen_satu',
          this.state.penjelasan_dokumen_satu,
        );
      }

      if (this.state.penjelasan_dokumen_dua !== '') {
        formData.append(
          'dokumen_anomali_dua',
          {
            uri: this.state.list_image2[0].uri,
            name: this.state.list_image2[0].name + '.jpg',
            type: 'image/jpg',
          },
          'file',
        );
        formData.append(
          'penjelasan_dokumen_dua',
          this.state.penjelasan_dokumen_dua,
        );
      }
      console.log('all FD', formData);
      fetch(url, {
        method: 'POST',

        body: formData,
        redirect: 'follow',
      })
        .then(response => response.json())
        .then(response => {
          console.log('', response);
          if (response.status === 'success') {
            this.setState(
              {
                isLoadingSave: false,
              },
              function() {
                Alert.alert('Data Saved', response.message, [
                  {
                    text: 'Okay',
                  },
                ]);
                this.props.onSimpan(response.data.no_lkso);
              },
            );
          } else {
            this.setState({
              isLoadingSave: false,
            });
            Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [
              {
                text: 'Okay',
              },
            ]);
          }
        })
        .catch(error => {
          this.setState({
            isLoadingSave: false,
          });
          Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [
            {
              text: 'Okay',
            },
          ]);
          console.log(error);
        });
    }
  }

  toggleModal = () => {
    this.setState({isModalVisible: !this.state.isModalVisible});
  };
  toggleModal2 = () => {
    this.setState({isModalVisible2: !this.state.isModalVisible2});
  };

  pickSingle(cropping, mediaType = 'photo') {
    ImagePicker.openPicker({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        console.log('image', image);
        this.setState(
          {
            isModalVisible: false,
            list_image: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
          },
          function() {
            console.log('mlaaku', this.state.list_image);
            this.setState({
              isOpenImage: true,
            });
          },
        );
      })
      .catch(e => {
        console.log(e);
        // Alert.alert(e.message ? e.message : e);
      });
  }

  pickSingleWithCamera(cropping, mediaType = 'photo') {
    ImagePicker.openCamera({
      cropping: cropping,
      compressImageQuality: 0.2,
      includeExif: true,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible: false,
            list_image: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
          },
          function() {
            console.log('mlaaku', this.state.list_image);
            this.setState({
              isOpenImage: true,
            });
          },
        );
      })
      .catch(e => alert(e));
    this.toggleModal;
  }

  pickSingle2(cropping, mediaType = 'photo') {
    ImagePicker.openPicker({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible2: false,
            list_image2: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
          },
          function() {
            console.log('mlaaku', this.state.list_image2);
            this.setState({
              isOpenImage2: true,
            });
          },
        );
      })
      .catch(e => {
        console.log(e);
        // Alert.alert(e.message ? e.message : e);
      });
  }

  pickSingleWithCamera2(cropping, mediaType = 'photo') {
    ImagePicker.openCamera({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(image => {
        this.setState(
          {
            isModalVisible2: false,
            list_image2: [
              {
                name: image.modificationDate,
                exif: image.exif,
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
              },
            ],
          },
          function() {
            console.log('mlaaku', this.state.list_image2);
            this.setState({
              isOpenImage2: true,
            });
          },
        );
      })
      .catch(e => alert(e));
    this.toggleModal;
  }

  componentDidMount() {
    console.log('imag', this.state.list_image);
    AsyncStorage.getItem('DataUser').then(value =>
      this.setState(
        {
          DataUser: JSON.parse(value),
        },
        function() {
          this.setState({
            isLoading: false,
          });
          console.log('user aa', this.state.DataUser.user.id_pegawai);
        },
      ),
    );
    // this.loadIndikatorAnomali();
  }

  render() {
    return (
      <Container>
        <Content
          ref={c => {
            this.scroll = c;
          }}>
          <View style={{marginTop: 20, marginBottom: 10}}>
            <StepIndicator
              customStyles={styles.customStyles}
              currentPosition={this.state.currentPosition}
              labels={labels}
              stepCount={3}
            />
          </View>
          {this.state.currentPosition == 0 ? (
            <CardItem>
              {this.state.isLoading ? (
                <View style={{alignItems: 'center', flex: 1}}>
                  <ActivityIndicator />
                </View>
              ) : (
                <View style={{flex: 1}}>
                  <View>
                    <Text style={[styles.fontLabel]}>Tanggal Anomali</Text>
                  </View>
                  <View>
                    <DatePicker
                      style={{width: '100%', height: 50}}
                      date={this.state.ANOMALI_DATE}
                      mode="date"
                      placeholder="Select Date"
                      format="YYYY-MM-DD"
                      minDate={this.state.minDate}
                      maxDate={this.state.maxDate}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={styles.datePicker}
                      onDateChange={date => {
                        this.setState({ANOMALI_DATE: date});
                      }}
                    />
                  </View>

                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Gardu Induk
                    </Text>
                  </View>
                  <View>
                    <SearchableDropdown
                      api="gardu_induk/get_all_data_gardu_induk"
                      searchByApi={true}
                      bodyApi={false}
                      displayData="nm_gardu_induk"
                      selectedMethod={item =>
                        this.setState(
                          {
                            nm_gardu_induk: item.nm_gardu_induk,
                            id_gardu_induk: item.id_gardu_induk,
                            isLoadingBay: true,
                            enabledBay: false,
                          },
                          function() {
                            this.setState({
                              isLoadingBay: false,
                            });
                          },
                        )
                      }
                      placeholder="Pilih Gardu Induk..."
                      selected={this.state.nm_gardu_induk}
                    />
                  </View>

                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>BAY</Text>
                  </View>
                  <View>
                    {!this.state.isLoadingBay ? (
                      <SearchableDropdown
                        api="bay/bay_gardu_induk"
                        searchByApi={false}
                        bodyApi={[
                          {
                            name: 'id_gardu_induk',
                            value: this.state.id_gardu_induk,
                          },
                        ]}
                        displayData="nm_bay"
                        selectedMethod={item =>
                          this.setState({
                            nm_bay: item.nm_bay,
                            id_bay: item.id_bay,
                            nextButton: false,
                          })
                        }
                        disabled={this.state.enabledBay}
                        placeholder="Pilih BAY..."
                      />
                    ) : (
                      <View style={{alignItems: 'center'}}>
                        <ActivityIndicator />
                      </View>
                    )}
                  </View>
                  <View style={{marginTop: 40}}>
                    <View style={{flexDirection: 'row'}}>
                      <Button
                        disabled={true}
                        style={{
                          flex: 1,
                          marginRight: 5,
                          backgroundColor: colors.grey,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Prev
                          </Text>
                        </View>
                      </Button>
                      <Button
                        onPress={() => this.movePage()}
                        style={{
                          flex: 1,
                          marginLeft: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Next
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </View>
                </View>
              )}
            </CardItem>
          ) : this.state.currentPosition === 1 ? (
            <CardItem>
              {this.state.isLoading1 === true ? (
                <View>
                  <ActivityIndicator />
                </View>
              ) : (
                <View style={{flex: 1}}>
                  <View>
                    <Text style={[{marginTop: 0}, styles.fontLabel]}>
                      Peralatan
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <SearchableDropdown
                      api="peralatan/get_all_data_peralatan"
                      searchByApi={false}
                      displayData="nm_peralatan"
                      otherList={{nm_peralatan: 'Lainnya', id_peralatan: '0'}}
                      selectedMethod={item =>
                        this.setState(
                          {
                            nm_peralatan: item.nm_peralatan,
                            id_peralatan: item.id_peralatan,
                            isLoadingalatlain: true,
                          },
                          function() {
                            this.setState({
                              isLoadingalatlain: false,
                            });
                          },
                        )
                      }
                      placeholder="Pilih Peralatan..."
                      selected={this.state.nm_peralatan}
                    />
                  </View>
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Keterangan Peralatan
                    </Text>
                  </View>
                  <View>
                    <Textarea
                      style={styles.textArea}
                      rowSpan={3}
                      bordered
                      placeholderTextColor={colors.gray02}
                      value={this.state.ketPeralatan}
                      placeholder="Isi Keterangan Peralatan..."
                      onChangeText={text => this.setState({ketPeralatan: text})}
                    />
                  </View>
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Level Tegangan
                    </Text>
                  </View>
                  <View>
                    <SearchableDropdown
                      api="tegangan/get_all_data_tegangan"
                      searchByApi={false}
                      displayData="volume_tegangan"
                      otherList={{volume_tegangan: 'Lainnya', id_tegangan: '0'}}
                      selectedMethod={item =>
                        this.setState({
                          volume_tegangan: item.volume_tegangan,
                          id_tegangan: item.id_tegangan,
                        })
                      }
                      placeholder="Pilih Level Tegangan..."
                    />
                  </View>
                  {this.state.id_tegangan === '0' ? (
                    <View>
                      <View>
                        <Text style={[{marginTop: 10}, styles.fontLabel]}>
                          Keterangan Tegangan Lainnya
                        </Text>
                      </View>
                      <View>
                        <Textarea
                          style={styles.textArea}
                          rowSpan={3}
                          bordered
                          placeholderTextColor={colors.gray02}
                          value={this.state.ketTegangan}
                          placeholder="Isi jika level tegangan yang diinput tidak terdapat pada list pilihan ..."
                          onChangeText={text =>
                            this.setState({ketTegangan: text})
                          }
                        />
                      </View>
                    </View>
                  ) : (
                    <View />
                  )}
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Phasa
                    </Text>
                  </View>
                  <View>
                    <SearchableDropdown
                      api="phasa/get_all_data_phasa"
                      searchByApi={false}
                      data={garduItem}
                      displayData="nm_phasa"
                      selectedMethod={item =>
                        this.setState(
                          {
                            nm_phasa: item.nm_phasa,
                            id_phasa: item.id_phasa,
                            isLoadingMerk: true,
                            disabledMerk: false,
                          },
                          function() {
                            this.setState({
                              isLoadingMerk: false,
                            });
                          },
                        )
                      }
                      placeholder="Pilih Phasa..."
                      selected={this.state.nm_phasa}
                    />
                  </View>
                  {this.state.isLoadingalatlain === true ? (
                    <View style={{alignItems: 'center'}}>
                      <ActivityIndicator />
                    </View>
                  ) : (
                    <View>
                      {this.state.id_peralatan === '0' ? (
                        <View>
                          <View style={{marginTop: 10}}>
                            <Text style={styles.fontLabel}>Merk Peralatan</Text>
                          </View>
                          <View>
                            <TextInput
                              style={styles.fontTextInput}
                              rowSpan={1}
                              editable={true}
                              bordered
                              value={this.state.merk_peralatan}
                              placeholder="Input Merk Peralatan..."
                              onChangeText={text =>
                                this.setState({merk_peralatan: text})
                              }
                            />
                          </View>
                          <View style={{marginTop: 10}}>
                            <Text style={styles.fontLabel}>Tipe Peralatan</Text>
                          </View>
                          <View>
                            <TextInput
                              style={styles.fontTextInput}
                              rowSpan={1}
                              editable={true}
                              bordered
                              value={this.state.tipe_peralatan}
                              placeholder="Input Tipe Peralatan..."
                              onChangeText={text =>
                                this.setState({tipe_peralatan: text})
                              }
                            />
                          </View>
                          <View style={{marginTop: 10}}>
                            <Text style={styles.fontLabel}>
                              Nomor Seri Peralatan
                            </Text>
                          </View>
                          <View>
                            <TextInput
                              style={styles.fontTextInput}
                              rowSpan={1}
                              editable={true}
                              bordered
                              value={this.state.nomor_seri}
                              placeholder="Input Nomor Seri Peralatan..."
                              onChangeText={text =>
                                this.setState({nomor_seri: text})
                              }
                            />
                          </View>
                        </View>
                      ) : (
                        <View>
                          <View>
                            <Text style={[{marginTop: 10}, styles.fontLabel]}>
                              Merk Peralatan
                            </Text>
                          </View>
                          <View>
                            {!this.state.isLoadingMerk ? (
                              <SearchableDropdown
                                api="peralatan_gi/get_all_data_peralatan_gi"
                                searchByApi={false}
                                bodyApi={[
                                  {
                                    name: 'column',
                                    value: 'merk_peralatan',
                                  },
                                  {
                                    name: 'id_gardu_induk',
                                    value: this.state.id_gardu_induk,
                                  },
                                  {
                                    name: 'id_bay',
                                    value: this.state.id_bay,
                                  },
                                  {
                                    name: 'id_phasa',
                                    value: this.state.id_phasa,
                                  },
                                  {
                                    name: 'id_peralatan',
                                    value: this.state.id_peralatan,
                                  },
                                ]}
                                displayData="merk_peralatan"
                                selectedMethod={item =>
                                  this.setState(
                                    {
                                      merk_peralatan: item.merk_peralatan,
                                      isLoadingTipe: true,
                                      disabledTipe: false,
                                    },
                                    function() {
                                      this.setState({
                                        isLoadingTipe: false,
                                      });
                                    },
                                  )
                                }
                                disabled={this.state.disabledMerk}
                                placeholder="Pilih Merk Peralatan..."
                              />
                            ) : (
                              <View style={{alignItems: 'center'}}>
                                <ActivityIndicator />
                              </View>
                            )}
                          </View>

                          <View>
                            <Text style={[{marginTop: 10}, styles.fontLabel]}>
                              Tipe Peralatan
                            </Text>
                          </View>
                          <View>
                            {!this.state.isLoadingTipe ? (
                              <SearchableDropdown
                                api="peralatan_gi/get_all_data_peralatan_gi"
                                searchByApi={false}
                                bodyApi={[
                                  {
                                    name: 'column',
                                    value: 'tipe_peralatan',
                                  },
                                  {
                                    name: 'id_gardu_induk',
                                    value: this.state.id_gardu_induk,
                                  },
                                  {
                                    name: 'id_bay',
                                    value: this.state.id_bay,
                                  },
                                  {
                                    name: 'id_phasa',
                                    value: this.state.id_phasa,
                                  },
                                  {
                                    name: 'id_peralatan',
                                    value: this.state.id_peralatan,
                                  },
                                  {
                                    name: 'merk_peralatan',
                                    value: this.state.merk_peralatan,
                                  },
                                ]}
                                displayData="tipe_peralatan"
                                selectedMethod={item =>
                                  this.setState(
                                    {
                                      tipe_peralatan: item.tipe_peralatan,
                                      disabledSeri: false,
                                      isLoadingSeri: true,
                                    },
                                    function() {
                                      this.setState({
                                        isLoadingSeri: false,
                                      });
                                    },
                                  )
                                }
                                disabled={this.state.disabledTipe}
                                placeholder="Pilih Tipe Peralatan..."
                              />
                            ) : (
                              <View style={{alignItems: 'center'}}>
                                <ActivityIndicator />
                              </View>
                            )}
                          </View>
                          <View>
                            <Text style={[{marginTop: 10}, styles.fontLabel]}>
                              Nomor Seri Peralatan
                            </Text>
                          </View>
                          <View>
                            {!this.state.isLoadingSeri ? (
                              <SearchableDropdown
                                api="peralatan_gi/get_all_data_peralatan_gi"
                                searchByApi={false}
                                bodyApi={[
                                  {
                                    name: 'column',
                                    value: 'nomor_seri',
                                  },
                                  {
                                    name: 'id_gardu_induk',
                                    value: this.state.id_gardu_induk,
                                  },
                                  {
                                    name: 'id_bay',
                                    value: this.state.id_bay,
                                  },
                                  {
                                    name: 'id_phasa',
                                    value: this.state.id_phasa,
                                  },
                                  {
                                    name: 'id_peralatan',
                                    value: this.state.id_peralatan,
                                  },
                                  {
                                    name: 'merk_peralatan',
                                    value: this.state.merk_peralatan,
                                  },
                                  {
                                    name: 'tipe_peralatan',
                                    value: this.state.tipe_peralatan,
                                  },
                                ]}
                                displayData="nomor_seri"
                                selectedMethod={item =>
                                  this.setState({
                                    nomor_seri: item.nomor_seri,
                                    nextButton: false,
                                  })
                                }
                                disabled={this.state.disabledSeri}
                                placeholder="Pilih Tipe Peralatan..."
                              />
                            ) : (
                              <View style={{alignItems: 'center'}}>
                                <ActivityIndicator />
                              </View>
                            )}
                          </View>
                        </View>
                      )}
                    </View>
                  )}

                  <View style={{marginTop: 40}}>
                    <View style={{flexDirection: 'row'}}>
                      <Button
                        onPress={() => this.backPage()}
                        style={{
                          flex: 1,
                          marginRight: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Prev
                          </Text>
                        </View>
                      </Button>
                      <Button
                        onPress={() => this.movePage()}
                        style={{
                          flex: 1,
                          marginLeft: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Next
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </View>
                </View>
              )}
            </CardItem>
          ) : (
            <CardItem>
              {this.state.isLoading3 === true ? (
                <View style={{flex: 1}}>
                  <ActivityIndicator size="large" />
                </View>
              ) : (
                <View style={{flex: 1}}>
                  <View>
                    <Text style={[{marginTop: 0}, styles.fontLabel]}>
                      Indikator Anomali
                    </Text>
                  </View>
                  <View>
                    <RadioButton
                      option={ListIndikatorAnomali}
                      // selected={this.state.namaindikatorAnomali}
                      displayData="NAMA_LIST"
                      onPressMethod={data =>
                        this.setState({
                          id_indikator_anomali: data.ID_OPTIONS_LIST,
                          namaindikatorAnomali: data.NAMA_LIST,
                        })
                      }
                    />
                  </View>
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Uraian Anomali
                    </Text>
                  </View>
                  <View>
                    <Textarea
                      style={styles.textArea}
                      rowSpan={3}
                      bordered
                      placeholderTextColor={colors.gray02}
                      value={this.state.uraian_anomali}
                      placeholder="Isikan uraian anomali (contoh : Hotspot pada klem PMS) ..."
                      onChangeText={text =>
                        this.setState({uraian_anomali: text})
                      }
                    />
                  </View>
                  <View>
                    <Text style={[{marginTop: 10}, styles.fontLabel]}>
                      Dampak Operasional
                    </Text>
                  </View>
                  <View>
                    <Textarea
                      style={styles.textArea}
                      rowSpan={2}
                      bordered
                      placeholderTextColor={colors.gray02}
                      value={this.state.dampak_operasional}
                      placeholder="Tulis Dampak Operasional"
                      onChangeText={text =>
                        this.setState({dampak_operasional: text})
                      }
                    />
                  </View>

                  <View>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <View style={{flex: 1, marginRight: 5}}>
                        <View>
                          <Text style={[{marginTop: 10}, styles.fontLabel]}>
                            Dokumentasi anomali 1
                          </Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            borderColor: colors.greenpln,
                            borderWidth: 1,
                            borderRadius: 5,
                          }}>
                          <View style={{backgroundColor: colors.greenpln}}>
                            <View style={{marginLeft: 10, padding: 5}}>
                              <Text
                                style={{fontWeight: 'bold', color: 'white'}}>
                                Tambah File
                              </Text>
                            </View>
                          </View>
                          <View>
                            <View style={{height: '100%', paddingVertical: 25}}>
                              <Button
                                onPress={this.toggleModal}
                                style={{justifyContent: 'center'}}
                                transparent>
                                {this.state.isOpenImage === true ? (
                                  <Image
                                    source={{uri: this.state.list_image[0].uri}}
                                    style={{
                                      height: 90,
                                      width: 130,
                                      resizeMode: 'contain',
                                    }}
                                  />
                                ) : (
                                  <Image
                                    source={require('../../../res/images/folder.png')}
                                    style={{
                                      height: 90,
                                      width: 130,
                                      resizeMode: 'contain',
                                    }}
                                  />
                                )}
                              </Button>
                            </View>
                          </View>
                        </View>
                        <Textarea
                          style={styles.textArea}
                          rowSpan={1.7}
                          bordered
                          placeholderTextColor={colors.gray02}
                          value={this.state.penjelasan_dokumen_satu}
                          placeholder="Penjelasan Dokumentasi 1"
                          onChangeText={text =>
                            this.setState({penjelasan_dokumen_satu: text})
                          }
                        />
                      </View>

                      <View style={{flex: 1, marginLeft: 5}}>
                        <View>
                          <Text style={[{marginTop: 10}, styles.fontLabel]}>
                            Dokumentasi anomali 2
                          </Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            borderColor: colors.greenpln,
                            borderWidth: 1,
                            borderRadius: 5,
                          }}>
                          <View style={{backgroundColor: colors.greenpln}}>
                            <View style={{marginLeft: 10, padding: 5}}>
                              <Text
                                style={{fontWeight: 'bold', color: 'white'}}>
                                Tambah File
                              </Text>
                            </View>
                          </View>
                          <View>
                            <View style={{height: '100%', paddingVertical: 25}}>
                              <Button
                                onPress={this.toggleModal2}
                                style={{justifyContent: 'center'}}
                                transparent>
                                {this.state.isOpenImage2 === true ? (
                                  <Image
                                    source={{
                                      uri: this.state.list_image2[0].uri,
                                    }}
                                    style={{
                                      height: 90,
                                      width: 130,
                                      resizeMode: 'contain',
                                    }}
                                  />
                                ) : (
                                  <Image
                                    source={require('../../../res/images/folder.png')}
                                    style={{
                                      height: 90,
                                      width: 130,
                                      resizeMode: 'contain',
                                    }}
                                  />
                                )}
                              </Button>
                            </View>
                          </View>
                        </View>
                        <Textarea
                          style={styles.textArea}
                          rowSpan={1.7}
                          bordered
                          placeholderTextColor={colors.gray02}
                          value={this.state.penjelasan_dokumen_dua}
                          placeholder="Penjelasan Dokumentasi 2"
                          onChangeText={text =>
                            this.setState({penjelasan_dokumen_dua: text})
                          }
                        />
                      </View>
                    </View>
                  </View>

                  {/* modal image 1*/}
                  <Modal
                    style={{justifyContent: 'flex-end'}}
                    backdropOpacity={0.3}
                    animationIn={'slideInUp'}
                    animationOut={'slideOutDown'}
                    isVisible={this.state.isModalVisible}
                    onBackdropPress={this.toggleModal}
                    onBackButtonPress={this.toggleModal}>
                    <View style={{marginBottom: 10}}>
                      <View
                        style={{
                          borderRadius: 5,
                          flexDirection: 'row',
                          paddingHorizontal: 30,
                          paddingVertical: 40,
                          backgroundColor: 'white',
                        }}>
                        <View style={{marginRight: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingleWithCamera(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/camera.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Take a Photo
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                        <View style={{marginLeft: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingle(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/galery.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Select from Galery
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                      </View>
                    </View>

                    <View style={{borderRadius: 5, marginBottom: 10}}>
                      <Button
                        onPress={this.toggleModal}
                        style={{backgroundColor: 'white'}}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{
                              color: colors.greenpln,
                              textAlign: 'center',
                            }}>
                            Cancel
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </Modal>

                  {/* modal image 2*/}
                  <Modal
                    style={{justifyContent: 'flex-end'}}
                    backdropOpacity={0.3}
                    animationIn={'slideInUp'}
                    animationOut={'slideOutDown'}
                    isVisible={this.state.isModalVisible2}
                    onBackdropPress={this.toggleModal2}
                    onBackButtonPress={this.toggleModal2}>
                    <View style={{marginBottom: 10}}>
                      <View
                        style={{
                          borderRadius: 5,
                          flexDirection: 'row',
                          paddingHorizontal: 30,
                          paddingVertical: 40,
                          backgroundColor: 'white',
                        }}>
                        <View style={{marginRight: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingleWithCamera2(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/camera.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Take a Photo
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                        <View style={{marginLeft: 10, flex: 1}}>
                          <Button
                            transparent
                            onPress={() => this.pickSingle2(false)}>
                            <View style={{alignItems: 'center'}}>
                              <Image
                                source={require('../../../res/images/galery.png')}
                                style={{
                                  height: 70,
                                  width: 90,
                                  resizeMode: 'contain',
                                }}
                              />
                              <View style={{width: '100%'}}>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: colors.greenpln,
                                  }}>
                                  Select from Galery
                                </Text>
                              </View>
                            </View>
                          </Button>
                        </View>
                      </View>
                    </View>

                    <View style={{borderRadius: 5, marginBottom: 10}}>
                      <Button
                        onPress={this.toggleModal2}
                        style={{backgroundColor: 'white'}}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{
                              color: colors.greenpln,
                              textAlign: 'center',
                            }}>
                            Cancel
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </Modal>

                  <View style={{marginTop: 40}}>
                    <View style={{flexDirection: 'row'}}>
                      <Button
                        onPress={() => this.backPage()}
                        style={{
                          flex: 1,
                          marginRight: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Prev
                          </Text>
                        </View>
                      </Button>
                      {this.state.isLoadingSave === true ? (
                        <View style={{flex: 1, alignItems: 'center'}}>
                          <ActivityIndicator />
                        </View>
                      ) : (
                        <Button
                          onPress={() => this.onKirimData()}
                          style={{
                            flex: 1,
                            marginLeft: 5,
                            backgroundColor: colors.green01,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Kirim
                            </Text>
                          </View>
                        </Button>
                      )}
                    </View>
                  </View>
                </View>
              )}
            </CardItem>
          )}
        </Content>
      </Container>
    );
  }
}
