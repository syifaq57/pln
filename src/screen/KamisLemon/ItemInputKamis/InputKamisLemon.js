/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Image,
  View,
  TextInput,
  Platform,
  AsyncStorage,
  ActivityIndicator,
  Alert,
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Button,
  Content,
  CardItem,
  Text,
  Textarea,
} from 'native-base';
import DatePicker from 'react-native-datepicker';
import Icon from 'react-native-vector-icons/Ionicons';
import SearchableDropdown from '../../../library/component/SearchableDropdown';
import RadioButton from '../../../library/component/CustomRadioButtonHorizontal';
import styles from '../../../res/styles/Form';
import colors from '../../../res/colors/index';
import StepIndicator from 'react-native-step-indicator';
import ImagePicker from 'react-native-image-crop-picker';
import Modal from 'react-native-modal';
import GlobalConfig from '../../../library/network/GlobalConfig';
import TambahLKS from './TambahLksKamis';

export default class IputKamisLemon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading1: true,
      isLoading2: false,
      isLoading3: false,
      isLoading4: false,
      currentPosition: 0,

      loadRadio: false,

      tgl_input: '',
      id_gardu_induk: '',
      nama_pelapor: '',
      //2
      thermovisi_peralatan_mtu: '',
      kondisi_visual_peralatan_trafo: '',
      tekanan_gas_sf6_pmt: '',
      thermovisi_jalur_kabel_power: '',

      announciator_relay: '',
      power_supply_relay: '',
      markshaling_kiosk: '',
      power_supply_ac_dc: '',

      kondisi_tegakan: '',
      kondisi_tower: '',
      kondisi_aksesoris_tower: '',
      kondisi_thermovisi_join: '',
      //3
      trafo_satu: '',
      trafo_dua: '',
      trafo_tiga: '',
      trafo_empat: '',
      trafo_lima: '',
      trafo_enam: '',

      //4
      pekerjaan_pihak_lain: '',
      kondisi_cuaca: '',
      kondisi_lingkungan: '',
      id_lkso_anomali: [],
      listlks: [],

      isLKSVisible: false,
      id_lkso: '',
    };
  }

  onKirimData() {
    this.setState({
      isLoadingSave: true,
    });
    if (
      this.state.pekerjaan_pihak_lain === '' ||
      this.state.kondisi_cuaca === '' ||
      this.state.kondisi_lingkungan === ''
    ) {
      alert('Form belum lengkap');
      this.setState({
        isLoadingSave: false,
      });
    } else {
      var url = GlobalConfig.SERVERHOST + 'kamis_lemon/add_data_kamis_lemon';
      var formData = new FormData();
      formData.append('nip_pegawai', this.state.DataUser.user.nip);
      formData.append('id_gardu_induk', this.state.id_gardu_induk);
      formData.append('tgl_input', this.state.tgl_input);
      formData.append('nama_pelapor', this.state.nama_pelapor);
      formData.append(
        'thermovisi_peralatan_mtu',
        this.state.thermovisi_peralatan_mtu,
      );
      formData.append(
        'kondisi_visual_peralatan_trafo',
        this.state.kondisi_visual_peralatan_trafo,
      );
      formData.append('tekanan_gas_sf6_pmt', this.state.tekanan_gas_sf6_pmt);
      formData.append(
        'thermovisi_jalur_kabel_power',
        this.state.thermovisi_jalur_kabel_power,
      );
      formData.append('trafo_satu', this.state.trafo_satu);
      formData.append('trafo_dua', this.state.trafo_dua);
      formData.append('trafo_tiga', this.state.trafo_tiga);
      formData.append('trafo_empat', this.state.trafo_empat);
      formData.append('trafo_lima', this.state.trafo_lima);
      formData.append('trafo_enam', this.state.trafo_enam);
      formData.append('announciator_relay', this.state.announciator_relay);
      formData.append('power_supply_relay', this.state.power_supply_relay);
      formData.append('markshaling_kiosk', this.state.markshaling_kiosk);
      formData.append('power_supply_ac_dc', this.state.power_supply_ac_dc);
      formData.append('kondisi_tegakan', this.state.kondisi_tegakan);
      formData.append('kondisi_tower', this.state.kondisi_tower);
      for (var i = 0; i < this.state.id_lkso_anomali.length; i++) {
        formData.append('id_lkso_anomali[]', this.state.id_lkso_anomali[i]);
        console.log(this.state.id_lkso_anomali[i]);
      }
      formData.append(
        'kondisi_aksesoris_tower',
        this.state.kondisi_aksesoris_tower,
      );
      formData.append(
        'kondisi_thermovisi_join',
        this.state.kondisi_thermovisi_join,
      );
      formData.append('pekerjaan_pihak_lain', this.state.pekerjaan_pihak_lain);
      formData.append('kondisi_cuaca', this.state.kondisi_cuaca);
      formData.append('kondisi_lingkungan', this.state.kondisi_lingkungan);
      // for (var i=0;i<formData._parts.length;i++)
      if (
        this.state.thermovisi_peralatan_mtu === 'ANOMALI' ||
        this.state.kondisi_visual_peralatan_trafo === 'ANOMALI' ||
        this.state.tekanan_gas_sf6_pmt === 'ANOMALI' ||
        this.state.thermovisi_jalur_kabel_power === 'ANOMALI' ||
        this.state.announciator_relay === 'ANOMALI' ||
        this.state.power_supply_relay === 'ANOMALI' ||
        this.state.markshaling_kiosk === 'ANOMALI' ||
        this.state.power_supply_ac_dc === 'ANOMALI' ||
        this.state.kondisi_tegakan === 'ANOMALI' ||
        this.state.kondisi_tower === 'ANOMALI' ||
        this.state.kondisi_aksesoris_tower === 'ANOMALI' ||
        this.state.kondisi_thermovisi_join === 'ANOMALI'
      ) {
        formData.append('kondisi_akhir', 'ANOMALI');
        console.log('all FD1', formData);
      } else {
        formData.append('kondisi_akhir', 'NORMAL');
        console.log('all FD2', formData);
      }

      fetch(url, {
        method: 'POST',
        body: formData,
      })
        .then(response => response.json())
        .then(response => {
          console.log('piye', response);
          if (response.status === 'success') {
            this.setState(
              {
                isLoadingSave: false,
              },
              function() {
                Alert.alert('Berhasil Menyimpan Data', response.message, [
                  {
                    text: 'Okay',
                  },
                ]);
                AsyncStorage.setItem('SavedKamisLemon', '1');
                this.props.navigation.goBack();
              },
            );
          } else {
            this.setState({
              isLoadingSave: false,
            });
            Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [
              {
                text: 'Okay',
              },
            ]);
          }
        })
        .catch(error => {
          this.setState({
            isLoadingSave: false,
          });
          Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [
            {
              text: 'Okay',
            },
          ]);
          console.log(error);
        });
    }
  }

  componentDidMount() {
    AsyncStorage.getItem('DataUser').then(value =>
      this.setState(
        {
          DataUser: JSON.parse(value),
        },
        function() {
          console.log('user aa', this.state.DataUser.user.id_pegawai);
        },
      ),
    );
    AsyncStorage.getItem('id_lkso').then(value =>
      this.setState(
        {
          id_lkso: value,
        },
        function() {
          console.log('idlkso 2', this.state.id_lkso);
          AsyncStorage.getItem('no_lkso').then(value2 =>
            this.setState(
              {
                no_lkso: value2,
              },
              function() {
                this.setState({
                  isLoading1: false,
                });
              },
            ),
          );
        },
      ),
    );
  }

  movePage() {
    if (this.state.currentPosition === 0) {
      console.log('masuk');
      if (
        this.state.id_gardu_induk === '' ||
        this.state.tgl_input === '' ||
        this.state.nama_pelapor === ''
      ) {
        Alert.alert('Lengkapi Form!', 'Form masih ada yang kosong.', [
          {
            text: 'Okay',
          },
        ]);
      } else {
        this.setState({
          isLoading2: false,
          currentPosition: this.state.currentPosition + 1,
        });
      }
    } else if (this.state.currentPosition === 1) {
      this.setState({
        isLoading3: true,
      });
      // console.log('masuk');
      if (
        this.state.thermovisi_peralatan_mtu === '' ||
        this.state.kondisi_visual_peralatan_trafo === '' ||
        this.state.tekanan_gas_sf6_pmt === '' ||
        this.state.thermovisi_jalur_kabel_power === '' ||
        this.state.announciator_relay === '' ||
        this.state.power_supply_relay === '' ||
        this.state.markshaling_kiosk === '' ||
        this.state.power_supply_ac_dc === '' ||
        this.state.kondisi_tegakan === '' ||
        this.state.kondisi_tower === '' ||
        this.state.kondisi_aksesoris_tower === '' ||
        this.state.kondisi_thermovisi_join === ''
      ) {
        Alert.alert('Lengkapi Form!', 'Form masih ada yang kosong.', [
          {
            text: 'Okay',
          },
        ]);
      } else {
        console.log('gagal');
        this.setState(
          {
            currentPosition: this.state.currentPosition + 1,
          },
          function() {
            this.setState({
              isLoading3: false,
            });
          },
        );
      }
    } else if (this.state.currentPosition === 2) {
      this.setState({
        isLoading4: true,
      });
      this.setState(
        {
          currentPosition: this.state.currentPosition + 1,
        },
        function() {
          this.setState({
            isLoading4: false,
          });
        },
      );
    }
  }

  backPage() {
    this.setState({
      currentPosition: this.state.currentPosition - 1,
    });
  }

  TambahLKS() {
    if (
      this.state.thermovisi_peralatan_mtu === 'ANOMALI' ||
      this.state.kondisi_visual_peralatan_trafo === 'ANOMALI' ||
      this.state.tekanan_gas_sf6_pmt === 'ANOMALI' ||
      this.state.thermovisi_jalur_kabel_power === 'ANOMALI' ||
      this.state.announciator_relay === 'ANOMALI' ||
      this.state.power_supply_relay === 'ANOMALI' ||
      this.state.markshaling_kiosk === 'ANOMALI' ||
      this.state.power_supply_ac_dc === 'ANOMALI' ||
      this.state.kondisi_tegakan === 'ANOMALI' ||
      this.state.kondisi_tower === 'ANOMALI' ||
      this.state.kondisi_aksesoris_tower === 'ANOMALI' ||
      this.state.kondisi_thermovisi_join === 'ANOMALI'
    ) {
      console.log('modal metu');
      this.setState({
        isLKSVisible: true,
      });
    }
  }

  pushLks(id_lkso) {
    var arraylks = this.state.listlks;
    arraylks.push(id_lkso);
    this.setState({
      id_lkso_anomali: arraylks,
    });
    console.log('arraylks', arraylks);
  }

  LKSModal = () => {
    this.setState({isLKSVisible: !this.state.isLKSVisible});
  };

  async loadChecklist(data) {
    this.setState(
      {
        loadRadio: true,
      },
      function() {
        console.log('dataa', data);
        if (this.state.thermovisi_peralatan_mtu !== data) {
          this.setState({thermovisi_peralatan_mtu: ''}, function() {
            this.setState({
              loadRadio: false,
            });
          });
        }
        if (this.state.kondisi_visual_peralatan_trafo !== data) {
          console.log('dat', data);
          this.setState({kondisi_visual_peralatan_trafo: ''}, function() {
            this.setState({
              loadRadio: false,
            });
          });
        }
        if (this.state.tekanan_gas_sf6_pmt !== data) {
          this.setState({tekanan_gas_sf6_pmt: ''}, function() {
            this.setState({
              loadRadio: false,
            });
          });
        }
        if (this.state.thermovisi_jalur_kabel_power !== data) {
          this.setState({thermovisi_jalur_kabel_power: ''}, function() {
            this.setState({
              loadRadio: false,
            });
          });
        }
        if (this.state.announciator_relay !== data) {
          this.setState({announciator_relay: ''}, function() {
            this.setState({
              loadRadio: false,
            });
          });
        }
        if (this.state.power_supply_relay !== data) {
          this.setState({power_supply_relay: ''}, function() {
            this.setState({
              loadRadio: false,
            });
          });
        }
        if (this.state.markshaling_kiosk !== data) {
          this.setState({markshaling_kiosk: ''}, function() {
            this.setState({
              loadRadio: false,
            });
          });
        }
        if (this.state.power_supply_ac_dc !== data) {
          this.setState({power_supply_ac_dc: ''}, function() {
            this.setState({
              loadRadio: false,
            });
          });
        }
        if (this.state.kondisi_tegakan !== data) {
          this.setState({kondisi_tegakan: ''}, function() {
            this.setState({
              loadRadio: false,
            });
          });
        }
        if (this.state.kondisi_tower !== data) {
          this.setState({kondisi_tower: ''}, function() {
            this.setState({
              loadRadio: false,
            });
          });
        }
        if (this.state.kondisi_aksesoris_tower !== data) {
          this.setState({kondisi_aksesoris_tower: ''}, function() {
            this.setState({
              loadRadio: false,
            });
          });
        }
        if (this.state.kondisi_thermovisi_join !== data) {
          this.setState({kondisi_thermovisi_join: ''}, function() {
            this.setState({
              loadRadio: false,
            });
          });
        }
      },
    );
  }

  render() {
    return (
      <Container>
        <Header
          transparent
          style={{
            marginTop: Platform.OS === 'ios' ? 0 : 0,
            borderBottomWidth: 0,
            backgroundColor: colors.greenpln,
          }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
            }}>
            <Button
              onPress={() => this.props.navigation.goBack()}
              transparent
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <Icon
                name="md-arrow-round-back"
                size={23}
                style={{color: 'white'}}
              />
              <Text
                uppercase={false}
                style={{
                  flex: 1,
                  textAlignVertical: 'center',
                  fontWeight: 'bold',
                  fontSize: 20,
                  color: 'white',
                }}>
                Input Kamis Lemon
              </Text>
            </Button>
          </View>
        </Header>
        <Content>
          <View style={{marginTop: 20, marginBottom: 10}}>
            <StepIndicator
              customStyles={styles.customStyles}
              currentPosition={this.state.currentPosition}
              labels={labels}
              stepCount={4}
            />
          </View>
          {this.state.currentPosition === 0 ? (
            <View>
              {this.state.isLoading1 === true ? (
                <View style={{alignItems: 'center'}}>
                  <ActivityIndicator size="large" />
                </View>
              ) : (
                <CardItem>
                  <View style={{flex: 1}}>
                    <View>
                      <Text style={[{marginTop: 10}, styles.fontLabel]}>
                        Gardu Induk
                      </Text>
                    </View>
                    <View>
                      <SearchableDropdown
                        api="gardu_induk/get_all_data_gardu_induk"
                        searchByApi={false}
                        displayData="nm_gardu_induk"
                        selectedMethod={item =>
                          this.setState({
                            nm_gardu_induk: item.nm_gardu_induk,
                            id_gardu_induk: item.id_gardu_induk,
                          })
                        }
                        placeholder="Pilih Gardu Induk..."
                      />
                    </View>

                    <View style={{marginTop: 10}}>
                      <Text style={[styles.fontLabel]}>
                        Tanggal Input Kamis Mingguan
                      </Text>
                    </View>
                    <View>
                      <DatePicker
                        style={{width: '100%', height: 50}}
                        date={this.state.tgl_input}
                        mode="date"
                        placeholder="Select Date"
                        format="YYYY-MM-DD"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={styles.datePicker}
                        onDateChange={date => {
                          this.setState({tgl_input: date}, function() {
                            console.log('date ', this.state.tgl_input);
                          });
                        }}
                      />
                    </View>

                    <View style={{marginTop: 10}}>
                      <Text style={styles.fontLabel}>Nama Pelapor</Text>
                    </View>
                    <View>
                      <TextInput
                        style={styles.fontTextInput}
                        rowSpan={1}
                        editable={true}
                        bordered
                        value={this.state.nama_pelapor}
                        placeholder="Inputkan Nama Pelakasana"
                        onChangeText={text =>
                          this.setState({nama_pelapor: text})
                        }
                      />
                    </View>

                    <View style={{marginTop: 40}}>
                      <View style={{flexDirection: 'row'}}>
                        <Button
                          disabled={true}
                          style={{
                            flex: 1,
                            marginRight: 5,
                            backgroundColor: colors.grey,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Prev
                            </Text>
                          </View>
                        </Button>
                        <Button
                          onPress={() => this.movePage()}
                          style={{
                            flex: 1,
                            marginLeft: 5,
                            backgroundColor: colors.greenpln,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Next
                            </Text>
                          </View>
                        </Button>
                      </View>
                    </View>
                  </View>
                </CardItem>
              )}
            </View>
          ) : this.state.currentPosition === 1 ? (
            <CardItem>
              {this.state.isLoading2 === true ? (
                <View style={{alignItems: 'center'}}>
                  <ActivityIndicator size="large" />
                </View>
              ) : (
                <View style={{flex: 1}}>
                  {this.state.loadRadio === true ? (
                    <View />
                  ) : (
                    <View>
                      <View>
                        <Text
                          style={{
                            marginBottom: 10,
                            marginLeft: 5,
                            fontWeight: 'bold',
                          }}>
                          CHECKLIST 1
                        </Text>
                      </View>
                      <View>
                        <Text style={[{marginTop: 0}, styles.fontLabel]}>
                          THERMOVISI PERALATAN MTU
                        </Text>
                      </View>
                      <RadioButton
                        option={anomali}
                        selected={this.state.thermovisi_peralatan_mtu}
                        displayData="NAMA_LIST"
                        onPressMethod={data =>
                          this.setState(
                            {
                              sortDir: data.ID_OPTIONS_LIST,
                              thermovisi_peralatan_mtu: data.NAMA_LIST,
                            },
                            function() {
                              if (
                                this.state.thermovisi_peralatan_mtu ===
                                'ANOMALI'
                              ) {
                                this.setState({
                                  isLKSVisible: true,
                                });
                              }
                            },
                          )
                        }
                      />
                      <View>
                        <Text style={[{marginTop: 5}, styles.fontLabel]}>
                          KONDISI VISUAL PERALATAN TRAFO
                        </Text>
                      </View>
                      <RadioButton
                        option={anomaliTrafo}
                        selected={this.state.kondisi_visual_peralatan_trafo}
                        displayData="NAMA_LIST"
                        onPressMethod={data =>
                          this.setState(
                            {
                              sortDir: data.ID_OPTIONS_LIST,
                              kondisi_visual_peralatan_trafo: data.NAMA_LIST,
                            },
                            function() {
                              if (
                                this.state.kondisi_visual_peralatan_trafo ===
                                'ANOMALI'
                              ) {
                                this.setState({
                                  isLKSVisible: true,
                                });
                              }
                            },
                          )
                        }
                      />
                      <View>
                        <Text style={[{marginTop: 5}, styles.fontLabel]}>
                          TEKANAN GAS SF6 PMT
                        </Text>
                      </View>
                      <RadioButton
                        option={anomali}
                        selected={this.state.tekanan_gas_sf6_pmt}
                        displayData="NAMA_LIST"
                        onPressMethod={data =>
                          this.setState(
                            {
                              sortDir: data.ID_OPTIONS_LIST,
                              tekanan_gas_sf6_pmt: data.NAMA_LIST,
                            },
                            function() {
                              if (
                                this.state.tekanan_gas_sf6_pmt === 'ANOMALI'
                              ) {
                                this.setState({
                                  isLKSVisible: true,
                                });
                              }
                            },
                          )
                        }
                      />
                      <View>
                        <Text style={[{marginTop: 5}, styles.fontLabel]}>
                          THERMOVISI JALUR KABEL POWER
                        </Text>
                      </View>
                      <RadioButton
                        option={anomaliThermovisi}
                        selected={this.state.thermovisi_jalur_kabel_power}
                        displayData="NAMA_LIST"
                        onPressMethod={data =>
                          this.setState(
                            {
                              sortDir: data.ID_OPTIONS_LIST,
                              thermovisi_jalur_kabel_power: data.NAMA_LIST,
                            },
                            function() {
                              if (
                                this.state.thermovisi_jalur_kabel_power ===
                                'ANOMALI'
                              ) {
                                this.setState({
                                  isLKSVisible: true,
                                });
                              }
                            },
                          )
                        }
                      />

                      <View>
                        <Text
                          style={{
                            marginTop: 15,
                            marginBottom: 10,
                            marginLeft: 5,
                            fontWeight: 'bold',
                          }}>
                          CHECKLIST 2
                        </Text>
                      </View>
                      <View>
                        <Text style={[{marginTop: 0}, styles.fontLabel]}>
                          ANNOUNCIATOR RELAY
                        </Text>
                      </View>
                      <RadioButton
                        option={anomali}
                        selected={this.state.announciator_relay}
                        displayData="NAMA_LIST"
                        onPressMethod={data =>
                          this.setState(
                            {
                              sortDir: data.ID_OPTIONS_LIST,
                              announciator_relay: data.NAMA_LIST,
                            },
                            function() {
                              if (this.state.announciator_relay === 'ANOMALI') {
                                this.setState({
                                  isLKSVisible: true,
                                });
                              }
                            },
                          )
                        }
                      />
                      <View>
                        <Text style={[{marginTop: 5}, styles.fontLabel]}>
                          POWER SUPPLY RELAY
                        </Text>
                      </View>
                      <RadioButton
                        option={anomali}
                        selected={this.state.power_supply_relay}
                        displayData="NAMA_LIST"
                        onPressMethod={data =>
                          this.setState(
                            {
                              sortDir: data.ID_OPTIONS_LIST,
                              power_supply_relay: data.NAMA_LIST,
                            },
                            function() {
                              if (this.state.power_supply_relay === 'ANOMALI') {
                                this.setState({
                                  isLKSVisible: true,
                                });
                              }
                            },
                          )
                        }
                      />
                      <View>
                        <Text style={[{marginTop: 5}, styles.fontLabel]}>
                          MARKSHALING KIOSK
                        </Text>
                      </View>
                      <RadioButton
                        option={anomali}
                        selected={this.state.markshaling_kiosk}
                        displayData="NAMA_LIST"
                        onPressMethod={data =>
                          this.setState(
                            {
                              sortDir: data.ID_OPTIONS_LIST,
                              markshaling_kiosk: data.NAMA_LIST,
                            },
                            function() {
                              if (this.state.markshaling_kiosk === 'ANOMALI') {
                                this.setState({
                                  isLKSVisible: true,
                                });
                              }
                            },
                          )
                        }
                      />
                      <View>
                        <Text style={[{marginTop: 5}, styles.fontLabel]}>
                          POWER SUPPLY AC/DC
                        </Text>
                      </View>
                      <RadioButton
                        option={anomali}
                        selected={this.state.power_supply_ac_dc}
                        displayData="NAMA_LIST"
                        onPressMethod={data =>
                          this.setState(
                            {
                              sortDir: data.ID_OPTIONS_LIST,
                              power_supply_ac_dc: data.NAMA_LIST,
                            },
                            function() {
                              if (this.state.power_supply_ac_dc === 'ANOMALI') {
                                this.setState({
                                  isLKSVisible: true,
                                });
                              }
                            },
                          )
                        }
                      />

                      <View>
                        <Text
                          style={{
                            marginTop: 15,
                            marginBottom: 10,
                            marginLeft: 5,
                            fontWeight: 'bold',
                          }}>
                          CHECKLIST 3
                        </Text>
                      </View>
                      <View>
                        <Text style={[{marginTop: 0}, styles.fontLabel]}>
                          KONDISI TEGAKAN
                        </Text>
                      </View>
                      <RadioButton
                        option={anomali}
                        selected={this.state.kondisi_tegakan}
                        displayData="NAMA_LIST"
                        onPressMethod={data =>
                          this.setState(
                            {
                              sortDir: data.ID_OPTIONS_LIST,
                              kondisi_tegakan: data.NAMA_LIST,
                            },
                            function() {
                              if (this.state.kondisi_tegakan === 'ANOMALI') {
                                this.setState({
                                  isLKSVisible: true,
                                });
                              }
                            },
                          )
                        }
                      />
                      <View>
                        <Text style={[{marginTop: 5}, styles.fontLabel]}>
                          KONDISI TOWER
                        </Text>
                      </View>
                      <RadioButton
                        option={anomali}
                        selected={this.state.kondisi_tower}
                        displayData="NAMA_LIST"
                        onPressMethod={data =>
                          this.setState(
                            {
                              sortDir: data.ID_OPTIONS_LIST,
                              kondisi_tower: data.NAMA_LIST,
                            },
                            function() {
                              if (this.state.kondisi_tower === 'ANOMALI') {
                                this.setState({
                                  isLKSVisible: true,
                                });
                              }
                            },
                          )
                        }
                      />
                      <View>
                        <Text style={[{marginTop: 5}, styles.fontLabel]}>
                          KONDISI AKSESORIS TOWER
                        </Text>
                      </View>
                      <RadioButton
                        option={anomali}
                        selected={this.state.kondisi_aksesoris_tower}
                        displayData="NAMA_LIST"
                        onPressMethod={data =>
                          this.setState(
                            {
                              sortDir: data.ID_OPTIONS_LIST,
                              kondisi_aksesoris_tower: data.NAMA_LIST,
                            },
                            function() {
                              if (
                                this.state.kondisi_aksesoris_tower === 'ANOMALI'
                              ) {
                                this.setState({
                                  isLKSVisible: true,
                                });
                              }
                            },
                          )
                        }
                      />
                      <View>
                        <Text style={[{marginTop: 5}, styles.fontLabel]}>
                          KONDISI THERMOVISI JOIN
                        </Text>
                      </View>
                      <RadioButton
                        option={anomali}
                        selected={this.state.kondisi_thermovisi_join}
                        displayData="NAMA_LIST"
                        onPressMethod={data =>
                          this.setState(
                            {
                              sortDir: data.ID_OPTIONS_LIST,
                              kondisi_thermovisi_join: data.NAMA_LIST,
                            },
                            function() {
                              if (
                                this.state.kondisi_thermovisi_join === 'ANOMALI'
                              ) {
                                this.setState({
                                  isLKSVisible: true,
                                });
                              }
                            },
                          )
                        }
                      />
                    </View>
                  )}

                  <View style={{marginTop: 40}}>
                    <View style={{flexDirection: 'row'}}>
                      <Button
                        onPress={() => this.backPage()}
                        style={{
                          flex: 1,
                          marginRight: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Prev
                          </Text>
                        </View>
                      </Button>
                      <Button
                        onPress={() => this.movePage()}
                        style={{
                          flex: 1,
                          marginLeft: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Next
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </View>
                </View>
              )}
            </CardItem>
          ) : this.state.currentPosition === 2 ? (
            <CardItem>
              {this.state.isLoading3 === true ? (
                <View style={{alignItems: 'center'}}>
                  <ActivityIndicator size="large" />
                </View>
              ) : (
                <View style={{flex: 1}}>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>Trafo 1</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      keyboardType={'numeric'}
                      value={this.state.trafo_satu}
                      placeholder="Counter Trafo 1"
                      onChangeText={text => this.setState({trafo_satu: text})}
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>Trafo 2</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      keyboardType={'numeric'}
                      value={this.state.trafo_dua}
                      placeholder="Counter Trafo 2"
                      onChangeText={text => this.setState({trafo_dua: text})}
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>Trafo 3</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      keyboardType={'numeric'}
                      value={this.state.trafo_tiga}
                      placeholder="Counter Trafo 3"
                      onChangeText={text => this.setState({trafo_tiga: text})}
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>Trafo 4</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      keyboardType={'numeric'}
                      value={this.state.trafo_empat}
                      placeholder="Counter Trafo 4"
                      onChangeText={text => this.setState({trafo_empat: text})}
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>Trafo 5</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      keyboardType={'numeric'}
                      value={this.state.trafo_lima}
                      placeholder="Counter Trafo 5"
                      onChangeText={text => this.setState({trafo_lima: text})}
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>Trafo 6</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      keyboardType={'numeric'}
                      value={this.state.trafo_enam}
                      placeholder="Counter Trafo 6"
                      onChangeText={text => this.setState({trafo_enam: text})}
                    />
                  </View>
                  <View style={{marginTop: 40}}>
                    <View style={{flexDirection: 'row'}}>
                      <Button
                        onPress={() => this.backPage()}
                        style={{
                          flex: 1,
                          marginRight: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Prev
                          </Text>
                        </View>
                      </Button>
                      <Button
                        onPress={() => this.movePage()}
                        style={{
                          flex: 1,
                          marginLeft: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Next
                          </Text>
                        </View>
                      </Button>
                    </View>
                  </View>
                </View>
              )}
            </CardItem>
          ) : (
            <CardItem>
              {this.state.isLoading4 === true ? (
                <View style={{alignItems: 'center'}}>
                  <ActivityIndicator size="large" />
                </View>
              ) : (
                <View style={{flex: 1}}>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>PEKERJAAN PIHAK LAIN</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.pekerjaan_pihak_lain}
                      placeholder="Inpu Pekerjaan Pihak Lain"
                      onChangeText={text =>
                        this.setState({pekerjaan_pihak_lain: text})
                      }
                    />
                    <Text style={{fontSize: 10, marginLeft: 5}}>
                      *JIKA TIDAK ADA MAKA NIHIL, JIKA ADA MOHON DICANTUMKAN
                      PEKERJAANNYA APA DENGAN PIHAK MANA
                    </Text>
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>KONDISI CUACA</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.kondisi_cuaca}
                      placeholder="Input Kondisi Cuaca"
                      onChangeText={text =>
                        this.setState({kondisi_cuaca: text})
                      }
                    />
                  </View>
                  <View style={{marginTop: 10}}>
                    <Text style={styles.fontLabel}>KONDISI LINGKUNGAN</Text>
                  </View>
                  <View>
                    <TextInput
                      style={styles.fontTextInput}
                      rowSpan={1}
                      editable={true}
                      bordered
                      value={this.state.kondisi_lingkungan}
                      placeholder="Input Kondisi Lingkungan"
                      onChangeText={text =>
                        this.setState({kondisi_lingkungan: text})
                      }
                    />
                  </View>

                  <View style={{marginTop: 40}}>
                    <View style={{flexDirection: 'row'}}>
                      <Button
                        onPress={() => this.backPage()}
                        style={{
                          flex: 1,
                          marginRight: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Prev
                          </Text>
                        </View>
                      </Button>
                      {this.state.isLoadingSave === true ? (
                        <View style={{flex: 1, alignItems: 'center'}}>
                          <ActivityIndicator />
                        </View>
                      ) : (
                        <Button
                          onPress={() => this.onKirimData()}
                          style={{
                            flex: 1,
                            marginLeft: 5,
                            backgroundColor: colors.green01,
                          }}>
                          <View style={{flex: 1}}>
                            <Text
                              style={{textAlign: 'center', fontWeight: 'bold'}}>
                              Kirim
                            </Text>
                          </View>
                        </Button>
                      )}
                    </View>
                  </View>
                </View>
              )}
            </CardItem>
          )}
          <Modal
            style={{
              justifyContent: 'flex-start',
              width: '100%',
              marginLeft: 0,
              marginTop: 0,
            }}
            animationIn={'slideInDown'}
            isVisible={this.state.isLKSVisible}
            onBackdropPress={this.LKSModal}
            backdropOpacity={0.5}
            animationOut={'slideOutUp'}
            animationInTiming={1000}>
            <TambahLKS
              navigation={this.props.navigation}
              visible={this.LKSModal}
              cekData={data => this.loadChecklist(data)}
              // eslint-disable-next-line no-undef
              pushLks={obj => this.pushLks(obj)}
            />
          </Modal>
        </Content>
      </Container>
    );
  }
}

const anomali = [
  {NAMA_LIST: 'NORMAL', id_lkso: '0'},
  {NAMA_LIST: 'ANOMALI', id_lkso: '1'},
];
const anomaliTrafo = [
  {NAMA_LIST: 'NORMAL', id_lkso: '0'},
  {NAMA_LIST: 'ANOMALI', id_lkso: '1'},
  {NAMA_LIST: 'TIDAK PUNYA TRAFO', id_lkso: '2'},
];
const anomaliThermovisi = [
  {NAMA_LIST: 'NORMAL', id_lkso: '0'},
  {NAMA_LIST: 'ANOMALI', id_lkso: '1'},
  {NAMA_LIST: 'TIDAK ADA KABEL POWER', id_lkso: '2'},
];

const labels = ['Tgl & Lokasi', 'Checklist', 'Counter Trafo', 'Kondisi'];
