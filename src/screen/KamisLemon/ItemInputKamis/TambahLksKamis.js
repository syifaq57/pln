/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  View,
  Dimensions,
  processColor,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  FlatList,
  Linking,
  Alert,
  Platform,
  AsyncStorage,
} from 'react-native';
import {Card, Button, Text, Header, Content} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../../res/colors/index';
import GlobalConfig from '../../../library/network/GlobalConfig';
import SearchableDropdown from '../../../library/component/SearchableDropdown';
import AnomaliKamis from './InputAnomaliKAmis';

const {width} = Dimensions.get('window');

export default class TambahLksKamis extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoadingData: false,
      isLoadingSave: false,
      jenisLKS: 1,
      cekData: '',
    };
  }

  componentDidMount() {}

  navigateToScreen(route) {
    this.props.navigation.navigate(route);
  }

  onSimpan(lkso) {
    this.props.cekData(this.state.cekData);
    this.setState(
      {
        isLoadingSave: true,
      },
      async function() {
        await this.props.pushLks(lkso);
        await alert('Berhasil Menyimpan');

        this.props.visible();
      },
    );
  }

  onBack() {
    this.setState(
      {
        cekData: 'NORMAL',
      },
      async function() {
        await this.props.cekData(this.state.cekData);
        await this.props.visible();
      },
    );
  }

  render() {
    return (
      <View style={{flex: 1, marginTop: 0, backgroundColor: 'white'}}>
        <Header
          transparent
          style={{
            marginTop: Platform.OS === 'ios' ? 0 : 0,
            borderBottomWidth: 0,
            backgroundColor: colors.greenpln,
          }}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
            }}>
            <Button
              onPress={() => this.onBack()}
              transparent
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <Icon
                name="md-arrow-round-back"
                size={23}
                style={{color: 'white'}}
              />
              <Text
                uppercase={false}
                style={{
                  flex: 1,
                  textAlignVertical: 'center',
                  fontWeight: 'bold',
                  fontSize: 20,
                  color: 'white',
                }}>
                Pengajuan LKS Kamis Lemon
              </Text>
            </Button>
          </View>
          <View style={{flex: 1}} />
        </Header>
        <Content style={{paddingTop: 20}}>
          {this.state.jenisLKS === 1 ? (
            <View style={{marginHorizontal: 20}}>
              <View style={{marginVertical: 10}}>
                <Button
                  style={{backgroundColor: colors.greenpln}}
                  onPress={() =>
                    this.setState({
                      jenisLKS: 2,
                    })
                  }>
                  <Text style={{flex: 5, fontWeight: 'bold'}}>
                    Buat LKS Baru
                  </Text>
                  <Icon
                    name="md-add"
                    size={23}
                    style={{color: 'white', flex: 0.5}}
                  />
                </Button>
              </View>
              <View
                style={{
                  marginVertical: 10,
                }}>
                <View
                  style={{
                    paddingLeft: 10,
                    marginBottom: 10,
                  }}>
                  <Text style={{color: 'black', fontWeight: 'bold'}}>
                    Pilih LKS Exsisting
                  </Text>
                </View>
                <View>
                  <SearchableDropdown
                    api="lkso/get_all_data_lkso"
                    searchByApi={false}
                    displayData="no_lkso"
                    selectedMethod={item =>
                      this.setState(
                        {
                          no_lkso: item.no_lkso,
                        },
                        function() {
                          this.setState({
                            cekData: 'ANOMALI',
                          });
                        },
                      )
                    }
                    placeholder="Pilih LKS..."
                  />
                </View>
                <View style={{marginTop: 40}}>
                  <View style={{flexDirection: 'row'}}>
                    <Button
                      onPress={() => this.onBack()}
                      style={{
                        flex: 1,
                        marginRight: 5,
                        backgroundColor: colors.darkOrange,
                      }}>
                      <View style={{flex: 1}}>
                        <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
                          Cancel
                        </Text>
                      </View>
                    </Button>
                    {this.state.isLoadingSave === true ? (
                      <View style={{flex: 1, alignItems: 'center'}}>
                        <ActivityIndicator />
                      </View>
                    ) : (
                      <Button
                        onPress={() => this.onSimpan(this.state.no_lkso)}
                        style={{
                          flex: 1,
                          marginLeft: 5,
                          backgroundColor: colors.greenpln,
                        }}>
                        <View style={{flex: 1}}>
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            Simpan
                          </Text>
                        </View>
                      </Button>
                    )}
                  </View>
                </View>
              </View>
            </View>
          ) : (
            <View style={{marginHorizontal: 10}}>
              <View style={{marginVertical: 10, marginHorizontal: 10}}>
                <Button
                  style={{backgroundColor: colors.greenpln}}
                  onPress={() =>
                    this.setState({
                      jenisLKS: 1,
                    })
                  }>
                  <Text style={{flex: 5, fontWeight: 'bold'}}>
                    Pilih LKS Existing
                  </Text>
                  <Icon
                    name="md-add"
                    size={23}
                    style={{color: 'white', flex: 0.5}}
                  />
                </Button>
              </View>
              <AnomaliKamis
                onSimpan={no_lkso => this.onSimpan(no_lkso)}
                cekData={data => this.props.cekData(data)}
              />
            </View>
          )}
        </Content>
      </View>
    );
  }
}
